Belote Note project

I started this project to help people who play the card game "Belote".
My project is a utility app where Belote players save their points.
The idea comes in summer in Moldova where I and my friends wanted to play Belote but we didn't have paper and pencil to store the points.
So I decided to create an app to substitute paper and pencil, in this way we can play Belote every time everywhere until there is an Android phone that runs my app.

Belote can be played in different countries: 

•	Quebec: Bœuf
•	Bulgaria: Бридж-белот, Bridge-Belote
•	Greece: Βίδα, Vida; Μπουρλότ, Bourlot
•	Cyprus: Πιλόττα, Pilotta
•	Croatia: Bela
•	Macedonia: Бељот, Beljot
•	Armenia: Բազար բլոտ, Bazaar Belote
•	Saudi Arabia: بلوت, Baloot
•	Russia: Белот, Belot
•	Tunisia: Belote
•	Moldavia: Belote
•	Madagascar: Tsiroanomandidy or Beloty




My app will support many languages:

•	French
•	Italian
•	Romanian
•	Bulgarian
•	Greek
•	Turkish
•	Albanian
•	Serbian
•	Armenian
•	Arabic
•	Russian
•	English (default language)
