package com.ionvaranita.belotenote.shared.generators

import android.content.Context
import android.os.Build
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import com.ionvaranita.belotenote.shared.customview.TextViewStyled

/**
 * Created by ionvaranita on 24/04/18.
 */
class ScorFieldGenerator(private val mContext: Context) : FieldGenerator() {
    fun getTextViewById(id: Int): TextViewStyled {
        val textView = TextViewStyled(mContext)
        textView.id = id
        configureField(textView)
        return textView
    }

    private fun configureField(field: TextViewStyled) {
        field.layoutParams = layoutParams
        field.maxLines = 1
        field.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)
        if (Build.VERSION.SDK_INT > 17) field.textAlignment = View.TEXT_ALIGNMENT_CENTER
        field.gravity = Gravity.CENTER
    }
}