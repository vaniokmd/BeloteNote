package com.ionvaranita.belotenote.shared.utils;

import android.content.Context;

import com.ionvaranita.belotenote.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    private final Context mContext;

    public DateUtils(Context context) {
        this.mContext = context;
    }

    public String getDateFormated(long dataInsertGame) {
        Calendar cYesterday = Calendar.getInstance();
        cYesterday.add(Calendar.DAY_OF_YEAR, -1); // yesterday
        Calendar cDatabase = Calendar.getInstance();
        cDatabase.setTime(new Date(dataInsertGame)); // your date
        boolean isYesterday = cYesterday.get(Calendar.YEAR) == cDatabase.get(Calendar.YEAR)
                && cYesterday.get(Calendar.DAY_OF_YEAR) == cDatabase.get(Calendar.DAY_OF_YEAR);
        Calendar cAlaltaieri = Calendar.getInstance();
        cAlaltaieri.add(Calendar.DAY_OF_YEAR, -2);
        Calendar cToday = Calendar.getInstance();
        boolean isToday = cToday.get(Calendar.YEAR) == cDatabase.get(Calendar.YEAR)
                && cToday.get(Calendar.DAY_OF_YEAR) == cDatabase.get(Calendar.DAY_OF_YEAR);
        String dayName;
        String data;
        if (isToday) {
            dayName = mContext.getString(R.string.today);
        } else if (isYesterday) {
            dayName = mContext.getResources().getString(R.string.yesterday);
        } else {
            dayName = new SimpleDateFormat("E").format(new java.util.Date(dataInsertGame));
        }
        data = new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date(dataInsertGame));
        return dayName + "\n" + data;
    }
}
