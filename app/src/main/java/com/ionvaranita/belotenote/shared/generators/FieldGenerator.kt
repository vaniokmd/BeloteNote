package com.ionvaranita.belotenote.shared.generators

import android.widget.TableRow
import com.ionvaranita.belotenote.shared.utils.DisplayUtils
import com.ionvaranita.belotenote.shared.utils.GlobalLayoutParams

/**
 * Created by ionvaranita on 2019-09-13;
 */
 open class FieldGenerator {
    val layoutParams: TableRow.LayoutParams
        get() {
            val layoutParams = GlobalLayoutParams.layoutParamsCampiInserimento()
            layoutParams.setMargins(DisplayUtils.convertDpAsPixels(2), DisplayUtils.convertDpAsPixels(2), DisplayUtils.convertDpAsPixels(2), DisplayUtils.convertDpAsPixels(2))
            return layoutParams
        }
}