package com.ionvaranita.belotenote.shared.utils

import android.app.Activity
import android.content.Intent
import com.ionvaranita.belotenote.R

class AppUtils {

    companion object {
        val homeItent: Intent
            get() {
                val homeIntent = Intent(Intent.ACTION_MAIN)
                homeIntent.addCategory(Intent.CATEGORY_HOME)
                homeIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                return homeIntent
            }

        fun shareApp(activity: Activity) {
            val sharingIntent = Intent(Intent.ACTION_SEND)
            sharingIntent.type = "text/plain"
            var shareBody = activity.getString(R.string.share_body)
            shareBody = """
            $shareBody
            ${getWebtUrl(activity)}
            """.trimIndent()
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, activity.getString(R.string.share_subject))
            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
            activity.startActivity(Intent.createChooser(sharingIntent, activity.getString(R.string.share_title)))
        }

        fun getWebtUrl(activity: Activity): String {
            return "https://play.google.com/store/apps/details?id=" + activity.applicationContext.packageName
        }


        fun isNumber(value:String): Boolean {
            return try {
                Integer.parseInt(value)
                true
            } catch (e: java.lang.Exception) {
                false
            }
        }
    }

}