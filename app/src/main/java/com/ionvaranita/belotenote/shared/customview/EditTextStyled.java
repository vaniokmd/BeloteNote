package com.ionvaranita.belotenote.shared.customview;

import android.content.Context;
import android.text.InputFilter;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.ionvaranita.belotenote.R;
import com.ionvaranita.belotenote.shared.constants.InputLength;
import com.ionvaranita.belotenote.shared.utils.ResourcesUtils;

public class EditTextStyled extends AppCompatEditText {
    public EditTextStyled(Context context) {
        super(context);
        init(context);
    }

    public EditTextStyled(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public EditTextStyled(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        this.setTypeface(ResourcesUtils.INSTANCE.getComicRelief(context));
        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter.LengthFilter(InputLength.POINTS_INPUT); //Filter to 3 characters
        setFilters(filters);
        setMaxLines(1);
        setHint("");
    }

    public void showError() {
        Animation shake = AnimationUtils.loadAnimation(getContext(), R.anim.shake);
        this.startAnimation(shake);
    }

}
