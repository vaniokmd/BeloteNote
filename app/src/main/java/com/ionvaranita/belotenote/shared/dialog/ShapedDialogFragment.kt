package com.ionvaranita.belotenote.shared.dialog

import androidx.fragment.app.DialogFragment
import com.ionvaranita.belotenote.R

/**
 * Created by ionvaranita on 2020-02-03;
 */
open class ShapedDialogFragment : DialogFragment() {
    override fun onStart() {
        super.onStart()
        dialog?.window?.setBackgroundDrawable(resources.getDrawable(R.drawable.bckg_shaped_white))
        dialog?.setCanceledOnTouchOutside(false)
    }
}