package com.ionvaranita.belotenote.shared.customview;

import android.content.Context;
import android.os.Vibrator;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageButton;
import android.view.View;

public class ImageButtonKeyboardStyled extends AppCompatImageButton {
    public ImageButtonKeyboardStyled(Context context) {
        super(context);
        init();
    }

    public ImageButtonKeyboardStyled(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ImageButtonKeyboardStyled(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Vibrator vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(200);
            }
        });
    }

}
