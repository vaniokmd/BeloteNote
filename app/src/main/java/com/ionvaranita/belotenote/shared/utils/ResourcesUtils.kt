package com.ionvaranita.belotenote.shared.utils

import android.content.Context
import android.graphics.Typeface
import com.ionvaranita.belotenote.R
import com.ionvaranita.belotenote.shared.constants.GameStatus
import com.ionvaranita.belotenote.shared.constants.GamePath

object ResourcesUtils {
    private const val EN = "en"
    private const val IT = "it"
    private const val RO = "ro"
    private const val RU = "ru"
    private const val BG = "bg"
    private const val FR = "fr"
    private const val HR = "hr"

    fun getImageResByStatus(status: Byte): Int {
        return if (status == GameStatus.CONTINUE) {
            R.mipmap.ic_playing_game
        } else if (status == GameStatus.EXTENDED || status == (GameStatus.EXTENDED_MANDATORY)) {
            R.mipmap.ic_extended_game
        } else {
            R.mipmap.ic_stoped_game
        }
    }

    fun getStatusDescription(status: Byte): Int {
        return if (status == GameStatus.CONTINUE) {
            R.string.alert_dialog_playing
        } else if (status == GameStatus.EXTENDED || status == GameStatus.EXTENDED_MANDATORY) {
            R.string.alert_dialog_to_extend
        } else {
            R.string.alert_dialog_finished
        }
    }

    fun getHeaderTablesImg(gamePath: GamePath): Int {
        return when (gamePath) {
            GamePath.GAME_PATH_2_PLAYERS -> R.drawable.ic_2players
            GamePath.GAME_PATH_3_PLAYERS -> R.drawable.ic_3players
            GamePath.GAME_PATH_4_PLAYERS -> R.drawable.ic_4players
            GamePath.GAME_PATH_2_GROUPS -> R.drawable.ic_2groups
        }
    }

    fun getComicRelief(context: Context): Typeface {
        return Typeface.createFromAsset(context.assets, "fonts/ComicRelief.ttf")
    }

    fun getIdByLocale(locale: String?): Int {
        return when (locale) {
            IT -> R.id.rb_italian
            RO -> R.id.rb_romanian
            RU -> R.id.rb_russian
            BG -> R.id.rb_bulgarian
            FR -> R.id.rb_french
            HR -> R.id.rb_croatian
            else -> R.id.rb_english
        }
    }

    fun getLocaleById(idSelected: Int): String {
        return when (idSelected) {
            R.id.rb_italian -> IT
            R.id.rb_romanian -> RO
            R.id.rb_russian -> RU
            R.id.rb_bulgarian -> BG
            R.id.rb_french -> FR
            R.id.rb_croatian -> HR
            else -> EN
        }
    }

    fun getCountryById(idSelected: Int?): String? {
        return when (idSelected) {
            R.id.rb_italian -> IT.toUpperCase()
            R.id.rb_romanian -> RO.toUpperCase()
            R.id.rb_russian -> RU.toUpperCase()
            else -> EN.toUpperCase()
        }
    }

    fun getLanguageByLocale(locale: String?): String {
        return when (locale) {
            IT -> "Italiano"
            RO -> "Românã"
            RU -> "Русский"
            BG -> "Bǎlgarski"
            FR -> "Française"
            HR -> "Hrvatski"
            else -> "English"
        }
    }

    fun getFlagByLocale(locale: String): Int {
        return when (locale) {
            IT -> R.mipmap.ic_it_flag
            RO -> R.mipmap.ic_ro_flag
            RU -> R.mipmap.ic_ru_flag
            BG -> R.mipmap.ic_bg_flag
            FR -> R.mipmap.ic_fr_flag
            HR -> R.mipmap.ic_hr_flag
            else -> R.mipmap.ic_en_flag
        }
    }


}