package com.ionvaranita.belotenote.shared.customview;

import android.content.Context;
import android.os.Vibrator;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatButton;
import android.view.View;

import com.ionvaranita.belotenote.shared.utils.ResourcesUtils;

public class ButtonKeyboardStyled extends AppCompatButton {
    public ButtonKeyboardStyled(Context context) {
        super(context);
        init();
    }

    public ButtonKeyboardStyled(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public ButtonKeyboardStyled(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Vibrator vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(200);
            }
        });
        this.setTypeface(ResourcesUtils.INSTANCE.getComicRelief(this.getContext()));
    }

}
