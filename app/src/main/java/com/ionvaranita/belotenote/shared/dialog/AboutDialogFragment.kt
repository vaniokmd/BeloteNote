package com.ionvaranita.belotenote.shared.dialog

import com.ionvaranita.belotenote.shared.utils.ResourcesUtils.getComicRelief
import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import com.ionvaranita.belotenote.R
import com.ionvaranita.belotenote.shared.customview.TextViewStyled
import android.graphics.Typeface
import android.content.Intent
import android.content.ActivityNotFoundException
import android.app.Activity
import android.net.Uri
import android.view.View
import android.widget.Button
import com.ionvaranita.belotenote.BuildConfig
import com.ionvaranita.belotenote.shared.extensions.getMarketUrl
import com.ionvaranita.belotenote.shared.extensions.watchAppVideo
import kotlinx.android.synthetic.main.dialog_fragment_about.*
import java.util.*

/**
 * Created by ionvaranita on 2019-09-18;
 */
class AboutDialogFragment : ShapedDialogFragment() {
    private val typeface by lazy { getComicRelief(
        Objects.requireNonNull(
            requireActivity()
        )
    )}
    private val DIALOG_FRAGMENT_SUPPORT = "dialog_fragment_support"
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val contentView = inflater.inflate(R.layout.dialog_fragment_about, container, false)
        (contentView.findViewById<View>(R.id.tv_appVersion) as TextViewStyled).text =
            getText(R.string.version).toString() + " " + BuildConfig.VERSION_NAME
        contentView.findViewById<View>(R.id.iv_close).setOnClickListener { dismiss() }
        val supportBt = contentView.findViewById<Button>(R.id.bt_support_about)
        supportBt.setTypeface(
            getComicRelief(
                Objects.requireNonNull(
                    requireActivity()
                )
            ), Typeface.BOLD
        )
        supportBt.setOnClickListener {
                SupportDialogFragment().show(
                    requireActivity().supportFragmentManager,
                    DIALOG_FRAGMENT_SUPPORT
                )

            dismiss()
        }
        val rateBt = contentView.findViewById<Button>(R.id.bt_rate_app_about)
        rateBt.setTypeface(
            typeface, Typeface.BOLD
        )
        rateBt.setOnClickListener {
            goToRatePage()
            dismiss()
        }
        val btHowItWorks = contentView.findViewById<Button>(R.id.bt_how_it_works)
        btHowItWorks.setTypeface(
            typeface,Typeface.BOLD
        )
        btHowItWorks.setOnClickListener {
            requireActivity().watchAppVideo()
            dismiss()
        }
        return contentView
    }


    private fun goToRatePage() {
            val uri = Uri.parse(requireActivity().getMarketUrl())
            val goToMarket = Intent(Intent.ACTION_VIEW, uri)
            goToMarket.addFlags(
                Intent.FLAG_ACTIVITY_NO_HISTORY or
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK
            )
            try {
                startActivity(goToMarket)
            } catch (e: ActivityNotFoundException) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=" + requireActivity().applicationContext.packageName)
                    )
                )
            }
    }

    companion object {
        fun newInstance(): AboutDialogFragment {
            return AboutDialogFragment()
        }
    }
}