package com.ionvaranita.belotenote.shared.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import android.view.animation.AnimationUtils
import androidx.fragment.app.activityViewModels
import com.ionvaranita.belotenote.R
import com.ionvaranita.belotenote.points.viewmodel.PointsViewModel
import kotlinx.android.synthetic.main.dialog_fragment_stop_game.view.*


class StopGameDialogFragment : ShapedDialogFragment() {

    private val viewModelPoints: PointsViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_fragment_stop_game, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.ib_close_delete_points_dialog.setOnClickListener {
            dismiss()
        }
        view.cancel_stop_game.setOnClickListener {
            dismiss()
        }
        view.bt_stop_game.setOnClickListener {
            viewModelPoints.stopGame()
            dismiss()
        }
        runStatusAnimation(view)
    }

    private fun runStatusAnimation(view: View) {
            val animOut: Animation = AnimationUtils.loadAnimation(requireContext(), android.R.anim.fade_out)
            animOut.duration = 1000
            val animIn: Animation = AnimationUtils.loadAnimation(requireContext(), android.R.anim.fade_in)
            animIn.duration = 1000
            animOut.setAnimationListener(object : AnimationListener {
                override fun onAnimationStart(animation: Animation) {}
                override fun onAnimationRepeat(animation: Animation) {}
                override fun onAnimationEnd(animation: Animation) {
                    view.iv_status_game_change.setImageResource(R.mipmap.ic_stoped_game)
                    animIn.setAnimationListener(object : AnimationListener {
                        override fun onAnimationStart(animation: Animation) {}
                        override fun onAnimationRepeat(animation: Animation) {}
                        override fun onAnimationEnd(animation: Animation) {}
                    })
                    view.iv_status_game_change.startAnimation(animIn)
                }
            })
            view.iv_status_game_change.startAnimation(animOut)
    }

    companion object {
        val TAG: String = StopGameDialogFragment::class.java.canonicalName ?: ""
        fun getInstance(): StopGameDialogFragment {
            return StopGameDialogFragment()
        }
    }
}