package com.ionvaranita.belotenote.shared.utils;

import com.ionvaranita.belotenote.shared.constants.GameStatus;

public class StatusGameUtils {
    public static boolean toStart(Byte statusGioco) {
        return statusGioco.equals(GameStatus.TO_START);
    }
    public static boolean isFinished(Byte statusGioco) {
        return statusGioco.equals(GameStatus.FINISHED);
    }

    public static boolean isContinue(Byte statusGioco) {
        return statusGioco.equals(GameStatus.CONTINUE);
    }

    public static boolean isExtended(Byte statusGioco) {
        return statusGioco.equals(GameStatus.EXTENDED);
    }

    public static boolean isExtendedMandatory(Byte statusGioco) {
        return statusGioco.equals(GameStatus.EXTENDED_MANDATORY);
    }

    public static boolean isExtendedOrExtendedMandatory(Byte statusGioco) {
        return statusGioco.equals(GameStatus.EXTENDED) || statusGioco.equals(GameStatus.EXTENDED_MANDATORY);
    }
}
