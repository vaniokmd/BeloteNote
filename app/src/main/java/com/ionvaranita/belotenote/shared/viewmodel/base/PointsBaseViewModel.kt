package com.ionvaranita.belotenote.shared.viewmodel.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ionvaranita.belotenote.R
import com.ionvaranita.belotenote.database.AppDatabase
import com.ionvaranita.belotenote.database.entity.ExtendedGameEntity
import com.ionvaranita.belotenote.shared.constants.GamePath
import com.ionvaranita.belotenote.shared.constants.IdsInputFieldUtils
import com.ionvaranita.belotenote.shared.result.GameResult
import java.util.*

class PointsBaseViewModel(application: Application, private val gamePath: GamePath, private val idGame: Short) : AndroidViewModel(application) {

    private val db by lazy { AppDatabase.getPersistentDatabase(application) }

    private val _observableGameResult: MutableLiveData<GameResult> = MutableLiveData()
    val observableGameResult: LiveData<GameResult> = _observableGameResult

    init {
        fetchGame()
    }

    private fun fetchGame() {
        when (gamePath) {
            GamePath.GAME_PATH_2_PLAYERS -> {
                val game2PEntity = db.game2PDao().getGame(idGame)
                val nameMap: SortedMap<Int, String> = mapOf(IdsInputFieldUtils.ID_GAME_POINTS to getGameString()).toSortedMap()
                nameMap[IdsInputFieldUtils.ID_PERSON_2_1] = game2PEntity.name1
                nameMap[IdsInputFieldUtils.ID_PERSON_2_2] = game2PEntity.name2
                val gameResult = GameResult(game2PEntity.idGame, game2PEntity.winnerPoints, game2PEntity.color, game2PEntity.statusGame, nameMap)
                _observableGameResult.postValue(gameResult)
            }

            GamePath.GAME_PATH_3_PLAYERS -> {
                val game3PEntity = db.game3PDao().getGame(idGame)
                val nameMap: SortedMap<Int, String> = mapOf(IdsInputFieldUtils.ID_GAME_POINTS to getGameString()).toSortedMap()
                nameMap[IdsInputFieldUtils.ID_PERSON_3_1] = game3PEntity.name1
                nameMap[IdsInputFieldUtils.ID_PERSON_3_2] = game3PEntity.name2
                nameMap[IdsInputFieldUtils.ID_PERSON_3_3] = game3PEntity.name3
                val gameResult = GameResult(game3PEntity.idGame, game3PEntity.winnerPoints, game3PEntity.color, game3PEntity.statusGame, nameMap)
                _observableGameResult.postValue(gameResult)

            }

            GamePath.GAME_PATH_4_PLAYERS -> {
                val game4PEntity = db.game4PDao().getGame(idGame)
                val nameMap: SortedMap<Int, String> = mapOf(IdsInputFieldUtils.ID_GAME_POINTS to getGameString()).toSortedMap()
                nameMap[IdsInputFieldUtils.ID_PERSON_4_1] = game4PEntity.name1
                nameMap[IdsInputFieldUtils.ID_PERSON_4_2] = game4PEntity.name2
                nameMap[IdsInputFieldUtils.ID_PERSON_4_3] = game4PEntity.name3
                nameMap[IdsInputFieldUtils.ID_PERSON_4_4] = game4PEntity.name4
                val gameResult = GameResult(game4PEntity.idGame, game4PEntity.winnerPoints, game4PEntity.color, game4PEntity.statusGame, nameMap)
                _observableGameResult.postValue(gameResult)
            }

            GamePath.GAME_PATH_2_GROUPS -> {
                val game2GroupsEntity = db.game2GroupsDao().getGame(idGame)
                val nameMap: SortedMap<Int, String> = mapOf(IdsInputFieldUtils.ID_GAME_POINTS to getGameString()).toSortedMap()
                nameMap[IdsInputFieldUtils.ID_TEAM_1] = game2GroupsEntity.name1
                nameMap[IdsInputFieldUtils.ID_TEAM_2] = game2GroupsEntity.name2
                val gameResult = GameResult(game2GroupsEntity.idGame, game2GroupsEntity.winnerPoints, game2GroupsEntity.color, game2GroupsEntity.statusGame, nameMap)
                _observableGameResult.postValue(gameResult)
            }

        }

    }

    private fun getGameString(): String {
        return getApplication<Application>().resources.getString(R.string.game)
    }

    fun getExtendedGame(): ExtendedGameEntity {
        return db.extendedGameDao().getExtendedGame(gamePath.description, idGame)
    }

}