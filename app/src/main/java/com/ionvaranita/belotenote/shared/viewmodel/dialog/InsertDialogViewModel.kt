package com.ionvaranita.belotenote.shared.viewmodel.dialog

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ionvaranita.belotenote.database.entity.ExtendedGameEntity
import com.ionvaranita.belotenote.shared.constants.GamePath
import com.ionvaranita.belotenote.shared.constants.GameStatus
import com.ionvaranita.belotenote.shared.viewmodel.base.BaseViewModel
import java.util.*

class InsertDialogViewModel(application: Application, private val gamePath: GamePath, private val idGame: Short) : BaseViewModel(application) {
    fun getExtendedGame(): ExtendedGameEntity {
        return db.extendedGameDao().getExtendedGame(gamePath.description, idGame)
    }

    private val _observableInsertMatch = MutableLiveData<Short>()
    val observableInsertMatch: LiveData<Short> = _observableInsertMatch

    private val _observableExtendMatch = MutableLiveData<Short>()
    val observableExtendMatch: LiveData<Short> = _observableExtendMatch

    private  val _observableFinishMatch = MutableLiveData<Int>()
    val observableFinishMatch: LiveData<Int> = _observableFinishMatch

    fun insertMatch(winningPoints: Short) {
        when (gamePath) {
            GamePath.GAME_PATH_2_PLAYERS -> {
                insertMatch2P(winningPoints)
            }
            GamePath.GAME_PATH_3_PLAYERS -> {
                insertMatch3P(winningPoints)
            }
            GamePath.GAME_PATH_4_PLAYERS -> {
                insertMatch4P(winningPoints)
            }
            GamePath.GAME_PATH_2_GROUPS -> {
                insertMatch2Groups(winningPoints)
            }
        }
        _observableInsertMatch.postValue(winningPoints)
    }

    fun extendGame(winningPoints: Short) {
        val isUpdated = when (gamePath) {
            GamePath.GAME_PATH_2_PLAYERS -> {
                extendGame2P(winningPoints)
            }
            GamePath.GAME_PATH_3_PLAYERS -> {
                extendGame3P(winningPoints)
            }
            GamePath.GAME_PATH_4_PLAYERS -> {
                extendGame4P(winningPoints)
            }
            GamePath.GAME_PATH_2_GROUPS -> {
                extendGame2Groups(winningPoints)
            }
        }

        if (isUpdated) {
            _observableExtendMatch.postValue(winningPoints)
        }
    }

    fun finishGame() {
        val idWinner = getExtendedGame().idWinner
        val isUpdated = when (gamePath) {
            GamePath.GAME_PATH_2_PLAYERS -> {
                finishGame2P(idWinner)
            }
            GamePath.GAME_PATH_3_PLAYERS -> {
                finishGame3P(idWinner)
            }
            GamePath.GAME_PATH_4_PLAYERS -> {
                finishGame4P(idWinner)
            }
            GamePath.GAME_PATH_2_GROUPS -> {
                finishGame2Groups(idWinner)
            }
        }
        if (isUpdated) {
            _observableFinishMatch.postValue(idWinner)
        }
    }

    private fun insertMatch2P(winningPoints: Short) {
        clearGame2P(idGame)
        extendGame2P(winningPoints)
    }

    private fun insertMatch3P(winnerPoints: Short) {
        clearGame3P(idGame)
        extendGame3P(winnerPoints)
    }

    private fun insertMatch4P(winnerPoints: Short) {
        clearGame4P(idGame)
        extendGame4P(winnerPoints)
    }

    private fun extendGame2P(winningPoints: Short): Boolean {
        return db.game2PDao().update(idGame, GameStatus.CONTINUE, winningPoints, Date().time) == 1
    }

    private fun extendGame3P(winnerPoints: Short): Boolean {
        return db.game3PDao().update(idGame, GameStatus.CONTINUE, winnerPoints, Date().time) == 1
    }

    private fun extendGame4P(winnerPoints: Short): Boolean {
        return db.game4PDao().update(idGame, GameStatus.CONTINUE, winnerPoints, Date().time) == 1
    }

    private fun extendGame2Groups(winningPoints: Short): Boolean {
        return db.game2GroupsDao().update(idGame, GameStatus.CONTINUE, winningPoints, Date().time) == 1
    }

    private fun insertMatch2Groups(winnerPoints: Short) {
        clearGame2Groups(idGame)
        extendGame2Groups(winnerPoints)
    }


    private fun finishGame2P(idWinner: Int): Boolean {
        val isUpdatedStatus = updateStatus2P(idGame, GameStatus.FINISHED)
        val isUpdatedScor = updateScor2P(idGame, idWinner)
        return isUpdatedStatus && isUpdatedScor
    }

    private fun finishGame3P(idWinner: Int): Boolean {
        val isUpdatedStatus = updateStatus3P(idGame, GameStatus.FINISHED)
        val isUpdatedScor = updateScor3P(idGame, idWinner)
        return isUpdatedStatus && isUpdatedScor
    }

    private fun finishGame4P(idWinner: Int): Boolean {
        val isUpdatedStatus = updateStatus4P(idGame, GameStatus.FINISHED)
        val isUpdatedScor = updateScor4P(idGame, idWinner)
        return isUpdatedStatus && isUpdatedScor
    }

    private fun finishGame2Groups(idWinner: Int): Boolean {
        val isUpdatedStatus = updateStatus2Groups(idGame, GameStatus.FINISHED)
        val isUpdatedScor = updateScor2Groups(idGame, idWinner)
        return isUpdatedStatus && isUpdatedScor
    }


}