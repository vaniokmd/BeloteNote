package com.ionvaranita.belotenote.shared.constants

import java.io.Serializable

/**
 * Created by ionvaranita on 19/04/18.
 */
object Paths {

    const val GAME_NAME_PARAM = "game_name_param"
    const val WINNING_POINTS_PARAM = "winning_points_param"
    const val GAME_PATH = "PATH"
}

enum class GamePath(val description: String) : Serializable{
    GAME_PATH_2_PLAYERS("action_2_param"),
    GAME_PATH_3_PLAYERS("action_3_param"),
    GAME_PATH_4_PLAYERS("action_4_param"),
    GAME_PATH_2_GROUPS("action_2_groups_param")
}
