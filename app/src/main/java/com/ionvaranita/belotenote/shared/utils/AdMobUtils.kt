package com.ionvaranita.belotenote.shared.utils

import android.app.Activity
import android.util.DisplayMetrics
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.ads.AdSize
import android.view.Display




object AdMobUtils {
    fun getAdSize(adviewContainer: View, activity: Activity): AdSize {
        val display = activity.windowManager.defaultDisplay
        val outMetrics = DisplayMetrics()
        display.getMetrics(outMetrics)

        val density = outMetrics.density

        var adWidthPixels = adviewContainer.width.toFloat()
        if (adWidthPixels == 0f) {
            adWidthPixels = outMetrics.widthPixels.toFloat()
        }

        var adHeightPixels = adviewContainer.height.toFloat()
        if (adHeightPixels == 0f) {
            adHeightPixels = outMetrics.heightPixels.toFloat()
        }

        val adWidth = (adWidthPixels / density).toInt()
        val adHeight = (adHeightPixels / density).toInt()
        return AdSize.getInlineAdaptiveBannerAdSize(adWidth, adHeight)
    }

     fun getAdSize(activity: Activity): AdSize {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        val display: Display = activity.getWindowManager().getDefaultDisplay()
        val outMetrics = DisplayMetrics()
        display.getMetrics(outMetrics)
        val widthPixels = outMetrics.widthPixels.toFloat()
        val density = outMetrics.density
        val adWidth = (widthPixels / density).toInt()
        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(activity, adWidth)
    }
}