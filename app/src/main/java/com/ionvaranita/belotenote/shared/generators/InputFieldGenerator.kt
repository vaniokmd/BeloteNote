package com.ionvaranita.belotenote.shared.generators

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
import android.text.Editable
import android.text.InputFilter
import android.text.InputFilter.AllCaps
import android.text.InputFilter.LengthFilter
import android.text.InputType
import android.text.TextWatcher
import android.view.Gravity
import android.view.View
import android.view.View.OnFocusChangeListener
import android.widget.TableRow
import androidx.core.content.ContextCompat
import com.ionvaranita.belotenote.R
import com.ionvaranita.belotenote.shared.constants.*
import com.ionvaranita.belotenote.shared.customview.EditTextStyled
import com.ionvaranita.belotenote.shared.extensions.intOrEmpty
import com.ionvaranita.belotenote.shared.keyboard.MyKeyboardFragment

/**
 * Created by ionvaranita on 19/04/18.
 */
class InputFieldGenerator(private val mAction: GamePath) : FieldGenerator() {
    @SuppressLint("ClickableViewAccessibility")
    fun popolatePointsFields(tableRow: TableRow, myKeyboardFragment: MyKeyboardFragment) {
        val listIdFields: List<Int> = when (mAction){
            GamePath.GAME_PATH_2_PLAYERS -> {
                IdsInputFieldUtils.getIdsFields2P()
            }
            GamePath.GAME_PATH_2_GROUPS -> {
                IdsInputFieldUtils.getIdsFields2Groups()
            }
            GamePath.GAME_PATH_3_PLAYERS -> {
                IdsInputFieldUtils.getIdsFields3P()
            }
            GamePath.GAME_PATH_4_PLAYERS -> {
                IdsInputFieldUtils.getIdsFields4P()
            }
        }
        for (i in listIdFields.indices) {
            val field = EditTextStyled(tableRow.context)
            val idCampo = listIdFields[i]
            field.id = idCampo
            configurePointInputField(field)
            field.inputType = InputType.TYPE_NUMBER_FLAG_SIGNED
            field.setTextIsSelectable(true)
            field.setTextIsSelectable(false)
            field.isFocusableInTouchMode = true
            field.isFocusable = true
            field.isClickable = true
            field.onFocusChangeListener = OnFocusChangeListener { v: View?, hasFocus: Boolean ->
                if (hasFocus) {
                    if (idCampo == IdsInputFieldUtils.ID_GAME_POINTS) {
                        myKeyboardFragment.setGameButtonsVisibility(false)
                    }
                    field.background.setColorFilter(Color.parseColor("#C0C0C0"), PorterDuff.Mode.DARKEN)
                } else {
                    if (idCampo == IdsInputFieldUtils.ID_GAME_POINTS) {
                        myKeyboardFragment.setGameButtonsVisibility(true)
                    }
                    field.background.setColorFilter(Color.WHITE, PorterDuff.Mode.DARKEN)
                }
                myKeyboardFragment.setField(field)
            }
            field.setOnTouchListener { v, event ->
                val inType = field.inputType // backup the input type
                field.inputType = InputType.TYPE_NULL // disable soft input
                field.onTouchEvent(event) // call native handler
                field.inputType = inType // restore input type
                true // consume touch even
                // if( v!=null)((InputMethodManager)v.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
            tableRow.addView(field)
        }
    }

    fun popolateNameFields(tableRow: TableRow) {
        val hintsMap: Map<Int, String> = when (mAction) {
            GamePath.GAME_PATH_2_PLAYERS -> {
                IdsInputFieldUtils.getHints2P(tableRow.context)
            }
            GamePath.GAME_PATH_3_PLAYERS -> {
                IdsInputFieldUtils.getHints3P(tableRow.context)
            }
            GamePath.GAME_PATH_4_PLAYERS -> {
                IdsInputFieldUtils.getHints4P(tableRow.context)
            }
            GamePath.GAME_PATH_2_GROUPS -> {
                IdsInputFieldUtils.getHints2Groups(tableRow.context)
            }
        }
        val iterator = hintsMap.keys.iterator()
        while (iterator.hasNext()) {
            val idField = iterator.next()
            val field = EditTextStyled(tableRow.context)
            field.id = idField
            field.hint = hintsMap[idField]!!.toUpperCase()
            configureNameInputField(field)
            tableRow.addView(field)
        }
    }

    private fun configurePointInputField(field: EditTextStyled) {
        field.layoutParams = layoutParams
        val filters = arrayOfNulls<InputFilter>(1)
        filters[0] = LengthFilter(InputLength.POINTS_INPUT.toInt())
        field.filters = filters
        field.maxLines = 1
        val bacgroundCampo = ContextCompat.getDrawable(field.context, R.drawable.bckg_shaped_white_bordered)
        bacgroundCampo!!.setColorFilter(Color.WHITE, PorterDuff.Mode.DARKEN)
        field.background = bacgroundCampo
        if (Build.VERSION.SDK_INT > 17) field.textAlignment = View.TEXT_ALIGNMENT_CENTER
        field.gravity = Gravity.CENTER
        field.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                val isBolt = s.toString() == Constants.STRING_BOLT
                val number = s.toString().intOrEmpty()
                if (!isBolt && s.length != number.length) {
                    s.clear()
                    s.insert(0, number)
                }
            }
        })
    }

    private fun configureNameInputField(field: EditTextStyled) {
        field.layoutParams = layoutParams
        val filters = arrayOfNulls<InputFilter>(2)
        filters[0] = LengthFilter(InputLength.NAME_INPUT.toInt())
        filters[1] = AllCaps()
        field.inputType = InputType.TYPE_CLASS_TEXT
        field.filters = filters
        field.setSingleLine()
        if (Build.VERSION.SDK_INT > 17) field.textAlignment = View.TEXT_ALIGNMENT_CENTER
        field.gravity = Gravity.CENTER
    }

}