package com.ionvaranita.belotenote.shared.constants;

public class GameStatus {
    public static final Byte TO_START = 0;
    public static final Byte FINISHED = 1;
    public static final Byte CONTINUE = 2;
    public static final Byte EXTENDED = 3;
    public static final Byte EXTENDED_MANDATORY = 4;
}
