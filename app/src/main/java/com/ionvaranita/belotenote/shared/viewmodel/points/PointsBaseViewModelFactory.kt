package com.ionvaranita.belotenote.shared.viewmodel.points

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ionvaranita.belotenote.shared.constants.GamePath
import com.ionvaranita.belotenote.shared.viewmodel.dialog.InsertDialogViewModel
import com.ionvaranita.belotenote.shared.viewmodel.base.PointsBaseViewModel

class PointsBaseViewModelFactory(private val application: Application, private val gamePath: GamePath,private val idGame: Short): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.canonicalName == InsertDialogViewModel::class.java.canonicalName) {
            return InsertDialogViewModel(application, gamePath, idGame) as T
        }
        return PointsBaseViewModel(application, gamePath, idGame) as T

    }
}