package com.ionvaranita.belotenote.shared.viewmodel.table

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.ionvaranita.belotenote.database.entity.WinnerPointsEntity
import com.ionvaranita.belotenote.database.entity.groups2.Game2GroupsEntity
import com.ionvaranita.belotenote.database.entity.groups2.Scor2GroupsEntity
import com.ionvaranita.belotenote.database.entity.players2.Game2PEntity
import com.ionvaranita.belotenote.database.entity.players2.Scor2PEntity
import com.ionvaranita.belotenote.database.entity.players3.Game3PEntity
import com.ionvaranita.belotenote.database.entity.players3.Scor3PEntity
import com.ionvaranita.belotenote.database.entity.players4.Game4PEntity
import com.ionvaranita.belotenote.database.entity.players4.Scor4PEntity
import com.ionvaranita.belotenote.shared.constants.GamePath
import com.ionvaranita.belotenote.main.datamodel.TableModelConverter
import com.ionvaranita.belotenote.main.datamodel.TableDataModel
import com.ionvaranita.belotenote.shared.viewmodel.base.BaseViewModel

class TableViewModel(application: Application, private val gamePath: GamePath) : BaseViewModel(application) {
    private val dataModelConverter by lazy { TableModelConverter(application) }

    val observableTables = MutableLiveData<ArrayList<TableDataModel>>()

    fun fetchTables() {
        when (gamePath) {
            GamePath.GAME_PATH_2_PLAYERS -> {
                val entities = db.game2PDao().games
                val tableDoList = dataModelConverter.convertTable2P(entities)
                observableTables.postValue(tableDoList)
            }
            GamePath.GAME_PATH_3_PLAYERS -> {
                val entities = db.game3PDao().games
                val tableDoList = dataModelConverter.convertTable3P(entities)
                observableTables.postValue(tableDoList)
            }
            GamePath.GAME_PATH_4_PLAYERS -> {
                val entities = db.game4PDao().games
                val tableDoList = dataModelConverter.convertTable4P(entities)
                observableTables.postValue(tableDoList)
            }
            GamePath.GAME_PATH_2_GROUPS -> {
                val entities = db.game2GroupsDao().games
                val tableDoList = dataModelConverter.convertTable2Groups(entities)
                observableTables.postValue(tableDoList)
            }
        }
    }

    fun insertTable2P(game2PEntity: Game2PEntity): Short {
        val idGameInserted = db.game2PDao().insert(game2PEntity).toShort()
        val scor2PEntity = Scor2PEntity()
        scor2PEntity.idGame = idGameInserted
        db.scor2PDao().insert(scor2PEntity)
        val winnerPointsEntity = WinnerPointsEntity()
        winnerPointsEntity.puntiVincenti = game2PEntity.winnerPoints
        db.winnerPointsDao().insert(winnerPointsEntity)
        return idGameInserted
    }

    fun insertTable3P(game3PEntity: Game3PEntity): Short {
        val idGameInserted = db.game3PDao().insert(game3PEntity).toShort()
        val scor3PEntity = Scor3PEntity()
        scor3PEntity.idGame = idGameInserted
        db.scor3PDao().insert(scor3PEntity)
        val winnerPointsEntity = WinnerPointsEntity()
        winnerPointsEntity.puntiVincenti = game3PEntity.winnerPoints
        db.winnerPointsDao().insert(winnerPointsEntity)
        return idGameInserted
    }

    fun insertTable4P(game4PEntity: Game4PEntity): Short {
        val idGame = db.game4PDao().insert(game4PEntity).toShort()
        val scor4PEntity = Scor4PEntity()
        scor4PEntity.idGame = idGame
        db.scor4PDao().insert(scor4PEntity)
        val winnerPointsEntity = WinnerPointsEntity()
        winnerPointsEntity.puntiVincenti = game4PEntity.winnerPoints
        db.winnerPointsDao().insert(winnerPointsEntity)
        return idGame
    }

    fun insertTable2Groups(game2GroupsEntity: Game2GroupsEntity): Short {
        val idGameInserted = db.game2GroupsDao().insert(game2GroupsEntity).toShort()
        val scor2GroupsEntity = Scor2GroupsEntity()
        scor2GroupsEntity.idGame = idGameInserted
        db.scor2GroupsDao().insert(scor2GroupsEntity)
        val winnerPointsEntity = WinnerPointsEntity()
        winnerPointsEntity.puntiVincenti = game2GroupsEntity.winnerPoints
        db.winnerPointsDao().insert(winnerPointsEntity)
        return idGameInserted
    }

    fun deleteTable(idGame: Short) {
        when(gamePath) {
            GamePath.GAME_PATH_2_PLAYERS -> {
                deleteTable2P(idGame)
            }
            GamePath.GAME_PATH_3_PLAYERS -> {
                deleteTable3P(idGame)
            }
            GamePath.GAME_PATH_4_PLAYERS -> {
                deleteTable4P(idGame)
            }
            GamePath.GAME_PATH_2_GROUPS -> {
                deleteTable2Groups(idGame)
            }
        }
    }

    private fun deleteTable2P(idGame: Short) {
        db.game2PDao().delete(idGame)
        db.scor2PDao().delete(idGame)
        db.extendedGameDao().delete(GamePath.GAME_PATH_2_PLAYERS.description, idGame)
        clearGame2P(idGame)
    }


    private fun deleteTable3P(idGame: Short) {
        db.game3PDao().delete(idGame)
        db.scor3PDao().delete(idGame)
        db.extendedGameDao().delete(GamePath.GAME_PATH_3_PLAYERS.description, idGame)
        clearGame3P(idGame)
    }


    private fun deleteTable4P(idGame: Short) {
        db.game4PDao().delete(idGame)
        db.scor4PDao().delete(idGame)
        db.extendedGameDao().delete(GamePath.GAME_PATH_4_PLAYERS.description, idGame)
        clearGame4P(idGame)
    }


    private fun deleteTable2Groups(idGame: Short) {
        db.game2GroupsDao().delete(idGame)
        db.scor2GroupsDao().delete(idGame)
        db.extendedGameDao().delete(GamePath.GAME_PATH_2_GROUPS.description, idGame)
        clearGame2Groups(idGame)
    }
}