package com.ionvaranita.belotenote.shared.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ionvaranita.belotenote.R
import com.ionvaranita.belotenote.shared.constants.GamePath
import com.ionvaranita.belotenote.shared.constants.Paths.GAME_NAME_PARAM
import com.ionvaranita.belotenote.shared.constants.Paths.GAME_PATH
import com.ionvaranita.belotenote.shared.constants.Paths.WINNING_POINTS_PARAM
import com.ionvaranita.belotenote.shared.dialog.InsertDialogFragment.Companion.STATUS_GAME_PARAM
import com.ionvaranita.belotenote.shared.utils.ResourcesUtils
import com.ionvaranita.belotenote.shared.utils.StatusGameUtils
import kotlinx.android.synthetic.main.dialog_fragment_info_game.view.*
import kotlin.properties.Delegates

/**
 * Created by ionvaranita on 2019-09-25;
 */
class InfoGameDialogFramgent : ShapedDialogFragment() {
    private lateinit var gamePath: GamePath
    private lateinit var gameName: String
    private var winnerPoints by Delegates.notNull<Short>()
    private var statusGame by Delegates.notNull<Byte>()

    override fun onCreate(savedInstanceState: Bundle?) {
        arguments?.let { bundle ->
            gamePath = bundle.getSerializable(GAME_PATH) as GamePath
            gameName = bundle.getString(GAME_NAME_PARAM, "")
            winnerPoints = bundle.getShort(WINNING_POINTS_PARAM)
            statusGame = bundle.getByte(STATUS_GAME_PARAM)
        }
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.dialog_fragment_info_game, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.iv_info_game_path.setImageResource(ResourcesUtils.getHeaderTablesImg(gamePath))
        view.tv_game_name_description.text = gameName
        view.tv_winner_points_description.text = winnerPoints.toString()
        view.iv_status_alert_dialog.setImageResource(ResourcesUtils.getImageResByStatus(statusGame))
        view.tv_status_description.setText(ResourcesUtils.getStatusDescription(statusGame))
        view.iv_close_infoGame.setOnClickListener { dismiss() }
        if (!StatusGameUtils.isFinished(statusGame)) {
            view.bt_stop_game_info.setOnClickListener {goToStopGame()}
            view.bt_stop_game_info.visibility = View.VISIBLE
        }

    }

    private fun goToStopGame() {
        this.dismiss()
        activity?.run {
            StopGameDialogFragment.getInstance().show(supportFragmentManager, StopGameDialogFragment.TAG)
        }
    }

    companion object {
        fun getInstance(gamePath: GamePath, gameName: String, winningPoints: Short, status: Byte): InfoGameDialogFramgent {
            val infoGameDialogFramgent = InfoGameDialogFramgent()
            val args = Bundle()
            args.putSerializable(GAME_PATH, gamePath)
            args.putString(GAME_NAME_PARAM, gameName)
            args.putShort(WINNING_POINTS_PARAM, winningPoints)
            args.putByte(STATUS_GAME_PARAM, status)
            infoGameDialogFramgent.arguments = args
            return infoGameDialogFramgent
        }
    }
}