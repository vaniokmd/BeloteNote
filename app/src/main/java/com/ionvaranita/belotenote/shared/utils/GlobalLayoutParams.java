package com.ionvaranita.belotenote.shared.utils;

import android.widget.TableRow;

/**
 * Created by ionvaranita on 19/04/18.
 */

public class GlobalLayoutParams {
    public static TableRow.LayoutParams layoutParamsCampiInserimento(){
        return new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT,1f);
    }
}
