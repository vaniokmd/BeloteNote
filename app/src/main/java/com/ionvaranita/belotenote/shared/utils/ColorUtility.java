package com.ionvaranita.belotenote.shared.utils;

import android.graphics.Color;

import java.util.Random;

public class ColorUtility {
    private final  Random mRandom = new Random(System.currentTimeMillis());

    public int generateRandomColor() {
        final int baseColor = Color.WHITE;

        final int baseRed = Color.red(baseColor);
        final int baseGreen = Color.green(baseColor);
        final int baseBlue = Color.blue(baseColor);

        final int red = (baseRed + mRandom.nextInt(256)) / 2;
        final int green = (baseGreen + mRandom.nextInt(256)) / 2;
        final int blue = (baseBlue + mRandom.nextInt(256)) / 2;

        return Color.rgb(red, green, blue);
    }
}
