package com.ionvaranita.belotenote.shared.dialog;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.ionvaranita.belotenote.R;
import com.ionvaranita.belotenote.shared.dialog.ShapedDialogFragment;

public class SupportDialogFragment extends ShapedDialogFragment {
    private EditText messageEdt;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.dialog_fragment_support, container, false);
        contentView.findViewById(R.id.iv_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        messageEdt = contentView.findViewById(R.id.et_messageEdit);
        contentView.findViewById(R.id.wrap_send_email).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail(messageEdt.getText().toString());
            }
        });

        return contentView;
    }

    private void sendEmail(final String body) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"vantech.developer@gmail.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.support) + " " + getString(R.string.app_name));
        i.putExtra(Intent.EXTRA_TEXT, body);
        try {
            startActivity(Intent.createChooser(i, getString(R.string.send_email)));
            dismiss();
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }
}
