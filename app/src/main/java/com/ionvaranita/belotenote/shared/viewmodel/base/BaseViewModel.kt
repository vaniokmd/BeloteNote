package com.ionvaranita.belotenote.shared.viewmodel.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.ionvaranita.belotenote.database.AppDatabase
import com.ionvaranita.belotenote.shared.constants.GamePath
import com.ionvaranita.belotenote.shared.constants.IdsInputFieldUtils
import java.util.*

open class BaseViewModel(application: Application) : AndroidViewModel(application) {

    protected val db: AppDatabase by lazy { AppDatabase.getPersistentDatabase(getApplication()) }
    protected fun clearGame2P(idGame: Short) {
        db.points2PDao().delete(idGame)
        db.boltDao().delete(GamePath.GAME_PATH_2_PLAYERS.description, idGame)
        db.boltManagerDao().delete(GamePath.GAME_PATH_2_PLAYERS.description, idGame)
    }

    protected fun clearGame3P(idGame: Short) {
        db.points3PDao().deletePoints(idGame)
        db.boltDao().delete(GamePath.GAME_PATH_3_PLAYERS.description, idGame)
        db.boltManagerDao().delete(GamePath.GAME_PATH_3_PLAYERS.description, idGame)
    }

    protected fun clearGame4P(idGame: Short) {
        db.points4PDao().deletePoints(idGame)
        db.boltDao().delete(GamePath.GAME_PATH_4_PLAYERS.description, idGame)
        db.boltManagerDao().delete(GamePath.GAME_PATH_4_PLAYERS.description, idGame)
    }

    protected fun clearGame2Groups(idGame: Short) {
        db.points2GroupsDao().delete(idGame)
        db.boltDao().delete(GamePath.GAME_PATH_2_GROUPS.description, idGame)
        db.boltManagerDao().delete(GamePath.GAME_PATH_2_GROUPS.description, idGame)
    }

    protected fun updateStatus2P(idGame: Short, statusGame: Byte): Boolean {
        return db.game2PDao().updateStatus(idGame, statusGame, Date().time) == 1
    }

    protected fun updateStatus3P(idGame: Short, statusGame: Byte): Boolean {
       return db.game3PDao().updateStatus(idGame, statusGame, Date().time) == 1
    }

    protected fun updateStatus4P(idGame: Short, statusGame: Byte): Boolean {
        return db.game4PDao().updateStatus(idGame, statusGame, Date().time) == 1
    }

    protected fun updateStatus2Groups(idGame: Short,statusGame: Byte): Boolean {
        return db.game2GroupsDao().updateStatus(idGame, statusGame, Date().time) == 1
    }

    protected fun updateScor2P(idGame: Short, idWinner: Int): Boolean {
        val scor2PEntity = db.scor2PDao().getScore(idGame)
        if (idWinner == IdsInputFieldUtils.ID_PERSON_2_1) {
            scor2PEntity.scorMe = (scor2PEntity.scorMe + 1).toShort()
        } else if (idWinner == IdsInputFieldUtils.ID_PERSON_2_2) {
            scor2PEntity.scorYouS = (scor2PEntity.scorYouS + 1).toShort()
        }
        return db.scor2PDao().update(scor2PEntity) == 1
    }

    protected fun updateScor3P(idGame: Short, idWinner: Int): Boolean {
        val scor3PEntity = db.scor3PDao().getScore(idGame)
        when (idWinner) {
            IdsInputFieldUtils.ID_PERSON_3_1 -> {
                scor3PEntity.scorP1 = scor3PEntity.scorP1.plus(1).toShort()
            }
            IdsInputFieldUtils.ID_PERSON_3_2 -> {
                scor3PEntity.scorP2 = scor3PEntity.scorP2.plus(1).toShort()
            }
            IdsInputFieldUtils.ID_PERSON_3_3 -> {
                scor3PEntity.scorP3 = scor3PEntity.scorP3.plus(1).toShort()
            }
        }
        return db.scor3PDao().update(scor3PEntity) == 1
    }

    protected fun updateScor4P(idGame: Short, idWinner: Int): Boolean {
        val scor4PEntity = db.scor4PDao().getScore(idGame)
        when (idWinner) {
            IdsInputFieldUtils.ID_PERSON_4_1 -> {
                scor4PEntity.scorP1 = (scor4PEntity.scorP1 + 1).toShort()
            }
            IdsInputFieldUtils.ID_PERSON_4_2 -> {
                scor4PEntity.scorP2 = (scor4PEntity.scorP2 + 1).toShort()
            }
            IdsInputFieldUtils.ID_PERSON_4_3 -> {
                scor4PEntity.scorP3 = (scor4PEntity.scorP3 + 1).toShort()
            }
            IdsInputFieldUtils.ID_PERSON_4_4 -> {
                scor4PEntity.scorP4 = (scor4PEntity.scorP4 + 1).toShort()
            }
        }
        return db.scor4PDao().update(scor4PEntity) == 1
    }

    protected fun updateScor2Groups(idGame: Short, idWinner: Int): Boolean {
        val scor2GroupsEntity = db.scor2GroupsDao().getScore(idGame)
        if (idWinner == IdsInputFieldUtils.ID_TEAM_1) {
            scor2GroupsEntity.scorWe = scor2GroupsEntity.scorWe.plus(1).toShort()
        } else if (idWinner == IdsInputFieldUtils.ID_TEAM_2) {
            scor2GroupsEntity.scorYouP = scor2GroupsEntity.scorYouP.plus(1).toShort()
        }
        return db.scor2GroupsDao().update(scor2GroupsEntity) == 1
    }

}