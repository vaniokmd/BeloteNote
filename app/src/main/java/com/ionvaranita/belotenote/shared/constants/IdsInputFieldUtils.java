package com.ionvaranita.belotenote.shared.constants;

import android.content.Context;

import com.ionvaranita.belotenote.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ionvaranita on 23/04/18.
 */

public class IdsInputFieldUtils {

    public static final Integer ID_TEAM_1 =411;
    public static final Integer ID_TEAM_2 = 412;

    public static final Integer ID_PERSON_2_1 =11;
    public static final Integer ID_PERSON_2_2 = 12;

    public static final Integer ID_PERSON_3_1 = 31;
    public static final Integer ID_PERSON_3_2 = 32;
    public static final Integer ID_PERSON_3_3 = 33;

    public static final Integer ID_PERSON_4_1 = 401;
    public static final Integer ID_PERSON_4_2 = 402;
    public static final Integer ID_PERSON_4_3 = 403;
    public static final Integer ID_PERSON_4_4 = 404;

    public static final Integer ID_GAME_POINTS = 17;

    public static List<Integer> getIdsFields2P(){
        List<Integer> listaIds = getIdsNames2P();
        listaIds.add(0,ID_GAME_POINTS);
        return listaIds;
    }

    public static List<Integer> getIdsFields3P(){
        List<Integer> listaIds = getIdsNames3P();
        listaIds.add(0,ID_GAME_POINTS);
        return listaIds;
    }

    public static List<Integer> getIdsFields4P(){
        List<Integer> listaIds = getIdsNames4P();
        listaIds.add(0,ID_GAME_POINTS);
        return listaIds;
    }

    public static List<Integer> getIdsFields2Groups(){
        List<Integer> listaIds = getIdsNames2Groups();
        listaIds.add(0,ID_GAME_POINTS);

        return listaIds;
    }

    public static List<Integer> getIdsNames2P(){
        List<Integer> listaIds = new ArrayList<>();
        listaIds.add(ID_PERSON_2_1);
        listaIds.add(ID_PERSON_2_2);
        return listaIds;
    }

    public static Map<Integer,String> getHints2P(Context context) {
        Map<Integer,String> stringMap  = new LinkedHashMap<>();
        stringMap.put(ID_PERSON_2_1,context.getString(R.string.me));
        stringMap.put(ID_PERSON_2_2,context.getString(R.string.you_s));
        return stringMap;
    }

    public static List<Integer> getIdsNames3P(){
        List<Integer> listaIds = new ArrayList<>();
        listaIds.add(ID_PERSON_3_1);
        listaIds.add(ID_PERSON_3_2);
        listaIds.add(ID_PERSON_3_3);
        return listaIds;
    }

    public static Map<Integer,String> getHints3P(Context context) {
        Map<Integer,String> stringMap  = new LinkedHashMap<>();
        stringMap.put(ID_PERSON_3_1,context.getString(R.string.p) + 1);
        stringMap.put(ID_PERSON_3_2,context.getString(R.string.p) + 2);
        stringMap.put(ID_PERSON_3_3,context.getString(R.string.p) + 3);
        return stringMap;
    }

    public static List<Integer> getIdsNames4P(){
        List<Integer> listaIds = new ArrayList<>();
        listaIds.add(ID_PERSON_4_1);
        listaIds.add(ID_PERSON_4_2);
        listaIds.add(ID_PERSON_4_3);
        listaIds.add(ID_PERSON_4_4);
        return listaIds;
    }

    public static Map<Integer,String> getHints4P(Context context) {
        Map<Integer,String> stringMap  = new LinkedHashMap<>();
        stringMap.put(ID_PERSON_4_1,context.getString(R.string.p) + 1);
        stringMap.put(ID_PERSON_4_2,context.getString(R.string.p) + 2);
        stringMap.put(ID_PERSON_4_3,context.getString(R.string.p) + 3);
        stringMap.put(ID_PERSON_4_4,context.getString(R.string.p) + 4);
        return stringMap;
    }

    public static List<Integer> getIdsNames2Groups(){
        List<Integer> listaIds = new ArrayList<>();
        listaIds.add(ID_TEAM_1);
        listaIds.add(ID_TEAM_2);
        return listaIds;
    }

    public static Map<Integer,String> getHints2Groups(Context context) {
        Map<Integer,String> stringMap  = new LinkedHashMap<>();
        stringMap.put(ID_TEAM_1,context.getString(R.string.we));
        stringMap.put(ID_TEAM_2,context.getString(R.string.you_p));
        return stringMap;
    }

}
