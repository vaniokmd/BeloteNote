package com.ionvaranita.belotenote.shared.dialog

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.ionvaranita.belotenote.R
import com.ionvaranita.belotenote.database.AppDatabase
import com.ionvaranita.belotenote.database.entity.groups2.Game2GroupsEntity
import com.ionvaranita.belotenote.database.entity.players2.Game2PEntity
import com.ionvaranita.belotenote.database.entity.players3.Game3PEntity
import com.ionvaranita.belotenote.database.entity.players4.Game4PEntity
import com.ionvaranita.belotenote.points.activities.Points2PActivity
import com.ionvaranita.belotenote.points.activities.Points2GroupsActivity
import com.ionvaranita.belotenote.points.activities.Points3PActivity
import com.ionvaranita.belotenote.points.activities.Points4PActivity
import com.ionvaranita.belotenote.shared.adapter.AdapterSpinner
import com.ionvaranita.belotenote.shared.constants.Constants
import com.ionvaranita.belotenote.shared.constants.GamePath
import com.ionvaranita.belotenote.shared.constants.GameStatus
import com.ionvaranita.belotenote.shared.constants.IdsInputFieldUtils
import com.ionvaranita.belotenote.shared.constants.Paths.GAME_PATH
import com.ionvaranita.belotenote.shared.customview.EditTextStyled
import com.ionvaranita.belotenote.shared.generators.InputFieldGenerator
import com.ionvaranita.belotenote.shared.utils.ResourcesUtils.getComicRelief
import com.ionvaranita.belotenote.shared.utils.StatusGameUtils
import com.ionvaranita.belotenote.shared.viewmodel.dialog.InsertDialogViewModel
import com.ionvaranita.belotenote.shared.viewmodel.points.PointsBaseViewModelFactory
import com.ionvaranita.belotenote.shared.viewmodel.table.TableViewModel
import com.ionvaranita.belotenote.shared.viewmodel.factory.TableViewModelFactory
import kotlinx.android.synthetic.main.dialog_fragment_insert.view.*
import java.util.*

/**
 * Created by ionvaranita on 12/04/18.
 */
class InsertDialogFragment : ShapedDialogFragment() {
    private lateinit var gamePath: GamePath
    private var mStatus: Byte? = null
    private var mDb: AppDatabase? = null
    private lateinit var viewModel: InsertDialogViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        arguments?.let {
            gamePath = it.getSerializable(GAME_PATH) as GamePath
            mStatus = it.getByte(STATUS_GAME_PARAM)
            val idGame = it.getShort(ID_GAME_PARAM)
            viewModel = ViewModelProvider(requireActivity(), PointsBaseViewModelFactory(requireActivity().application, gamePath, idGame)).get(InsertDialogViewModel::class.java)
        }

        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_fragment_insert, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.bt_ok.setTypeface(getComicRelief(view.context), Typeface.BOLD)
        view.bt_cancel.setTypeface(getComicRelief(view.context), Typeface.BOLD)
        view.findViewById<View>(R.id.ib_close_insert_dialog).setOnClickListener { dismiss() }
        if (StatusGameUtils.toStart(mStatus)) {
            showInputNames(view)
            showWinningPoints()
            popolateInputNames(view)
        }
        if (StatusGameUtils.isFinished(mStatus)) {
            showWinningPoints()
        }
        if (gamePath == GamePath.GAME_PATH_2_GROUPS) {
            view.tr_name_input_fields_header.setText(R.string.dialog_fragment_team_names)
        }
        init(view)
    }

    private fun showInputNames(view: View) {
        view.tr_name_input_fields.visibility = View.VISIBLE
        view.tr_name_input_fields_header.visibility = View.VISIBLE
    }

    private fun showWinningPoints() {
        view?.findViewById<View>(R.id.wrap_spinner_winner_points)?.visibility = View.VISIBLE
        view?.findViewById<View>(R.id.sw_winnerPoints)?.visibility = View.VISIBLE
    }

    private fun popolateInputNames(view: View) {
        val inputFieldGenerator = InputFieldGenerator(gamePath)
        inputFieldGenerator.popolateNameFields(view.tr_name_input_fields!!)
        view.tr_name_input_fields!!.getChildAt(0).requestFocus()
    }

    private fun init(view: View) {
        view.bet_input_winner_points.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                val value = s.toString()
                if (value.length == 1 && value == "0") {
                    s.clear()
                }
            }
        })
        mDb = AppDatabase.getPersistentDatabase(view.context)
        popolateWinnerPointsRV(view)
        setButtons(view)
    }

    private fun setButtons(view: View) {
        if (StatusGameUtils.toStart(mStatus)) {
            manageNewGame(view)
        } else if (StatusGameUtils.isFinished(mStatus)) {
            manageNewMatch(view)
        } else if (StatusGameUtils.isExtended(mStatus) || StatusGameUtils.isExtendedMandatory(mStatus)) {
            view.sw_winnerPoints.visibility = View.GONE
            view.sw_winnerPoints.isChecked = true
            view.sw_winnerPoints.isEnabled = false
            view.tv_messagge_text1.visibility = View.VISIBLE
            view.tv_messagge_text2.visibility = View.VISIBLE
            if (StatusGameUtils.isExtended(mStatus)) {
                manageExtended(view)
            } else {
                view.bt_cancel.visibility = View.GONE
                view.tv_messagge_text1.setText(R.string.dialog_fragment_mandatory_extend_match_info1)
                view.tv_messagge_text2.setText(R.string.dialog_fragment_mandatory_extend_match_info2)
            }
            val maxPoints = viewModel.getExtendedGame().maxPoints
            view.bet_input_winner_points.hint = getString(R.string.dialog_fragment_greater_than, maxPoints.toString())
            view.bt_ok.setOnClickListener {
                val winnerPointsStr = winnerPoints
                if (winnerPointsStr.isNotEmpty()) {
                    val winningPoints = winnerPointsStr.toShort()
                    if (winningPoints > maxPoints) {
                        viewModel.extendGame(winningPoints)
                        dismiss()
                    } else {
                        view.bet_input_winner_points.text!!.clear()
                        view.bet_input_winner_points.showError()
                    }
                }
            }
            view.bt_ok.setText(R.string.dialog_fragment_extend_match)
        }
    }

    @SuppressLint("StringFormatInvalid")
    private fun manageExtended(view: View) {
        view.bt_cancel.visibility = View.VISIBLE
        val winnerName = arguments?.getString(WINNER_NAME_PARAM) ?: ""
        view.tv_messagge_text1.setText(R.string.dialog_fragment_extend_match_info)
        view.tv_messagge_text2.setText(R.string.dialog_fragment_extend_match_q)
        view.bt_cancel.text = getString(R.string.dialog_fragment_win, winnerName)
        view.bt_cancel.setOnClickListener {
            viewModel.finishGame()
            dismiss()
        }
    }

    private fun manageNewMatch(view: View) {
        view.bt_cancel.visibility = View.GONE
        view.bt_ok.setText(R.string.dialog_fragment_insert_match)
        view.bt_ok.setOnClickListener {
            val winningPointsStr = winnerPoints
            if (winningPointsStr.isNotEmpty()) {
                activity?.run {
                    val winningPoints = winningPointsStr.toShort()
                    viewModel.insertMatch(winningPoints)
                    dismiss()
                }

            }
        }
    }

    private fun manageNewGame(view: View) {
        view.bt_cancel.visibility = View.GONE
        view.bt_ok.setText(R.string.dialog_fragment_insert_table)
        view.bt_ok.setOnClickListener {
            val winerPointsStr = winnerPoints
            if (winerPointsStr.isNotEmpty()) {
                activity?.run {
                    val stringMap: MutableMap<Int, String> = LinkedHashMap()
                    stringMap[IdsInputFieldUtils.ID_GAME_POINTS] = getString(R.string.game)
                    for (i in 0 until view.tr_name_input_fields!!.childCount) {
                        val field = view.tr_name_input_fields!!.getChildAt(i) as EditTextStyled
                        val value = field.text.toString().trim { it <= ' ' }
                        if (value.isNotEmpty()) {
                            stringMap[field.id] = value
                        } else {
                            stringMap[field.id] = field.hint.toString()
                        }
                    }
                    stringMap[IdsInputFieldUtils.ID_GAME_POINTS] = resources.getString(R.string.game)
                    val winnerPoints = winerPointsStr.toShort()

                    activity?.run {
                        val viewModelTable = ViewModelProvider(this, TableViewModelFactory(this.application, gamePath)).get(TableViewModel::class.java)
                        when (gamePath) {
                            GamePath.GAME_PATH_2_GROUPS -> {
                                val game2GroupsEntity = Game2GroupsEntity()
                                game2GroupsEntity.winnerPoints = winnerPoints
                                game2GroupsEntity.name1 = stringMap[IdsInputFieldUtils.ID_TEAM_1]!!
                                game2GroupsEntity.name2 = stringMap[IdsInputFieldUtils.ID_TEAM_2]!!
                                val idGame2Groups = viewModelTable.insertTable2Groups(game2GroupsEntity)
                                goToPoints(idGame2Groups)

                            }
                            GamePath.GAME_PATH_2_PLAYERS -> {
                                val game2PEntity = Game2PEntity()
                                game2PEntity.winnerPoints = winnerPoints
                                game2PEntity.name1 = stringMap[IdsInputFieldUtils.ID_PERSON_2_1]!!
                                game2PEntity.name2 = stringMap[IdsInputFieldUtils.ID_PERSON_2_2]!!
                                val idGame2Groups = viewModelTable.insertTable2P(game2PEntity)
                                goToPoints(idGame2Groups)
                            }
                            GamePath.GAME_PATH_3_PLAYERS -> {
                                val game3PEntity = Game3PEntity()
                                game3PEntity.name1 = stringMap[IdsInputFieldUtils.ID_PERSON_3_1]!!
                                game3PEntity.name2 = stringMap[IdsInputFieldUtils.ID_PERSON_3_2]!!
                                game3PEntity.name3 = stringMap[IdsInputFieldUtils.ID_PERSON_3_3]!!
                                game3PEntity.winnerPoints = winnerPoints
                                val idGame3P = viewModelTable.insertTable3P(game3PEntity)
                                goToPoints(idGame3P)
                            }
                            GamePath.GAME_PATH_4_PLAYERS -> {
                                val game4PEntity = Game4PEntity()
                                game4PEntity.name1 = stringMap[IdsInputFieldUtils.ID_PERSON_4_1]!!
                                game4PEntity.name2 = stringMap[IdsInputFieldUtils.ID_PERSON_4_2]!!
                                game4PEntity.name3 = stringMap[IdsInputFieldUtils.ID_PERSON_4_3]!!
                                game4PEntity.name4 = stringMap[IdsInputFieldUtils.ID_PERSON_4_4]!!
                                game4PEntity.winnerPoints = winnerPoints
                                val idGame4P = viewModelTable.insertTable4P(game4PEntity)
                                goToPoints(idGame4P)
                            }
                        }
                    }

                    dismiss()
                }

            }
        }
    }

    private fun popolateWinnerPointsRV(rootView: View) {
        val listaPuntiVincenti = mDb!!.winnerPointsDao().winnerPoints
        rootView.sw_winnerPoints.setOnCheckedChangeListener { buttonView, isChecked ->
            val selectedView = rootView.sp_winnerPoints.selectedView as TextView?
            if (isChecked) {
                rootView.bet_input_winner_points.visibility = View.VISIBLE
                rootView.bet_input_winner_points.isEnabled = true
                rootView.sp_winnerPoints.isEnabled = false
                selectedView?.setTextColor(Color.GRAY)
                rootView.textview_winner_points.setTextColor(Color.GRAY)
                rootView.bet_input_winner_points.requestFocus()
            } else {
                rootView.bet_input_winner_points.visibility = View.GONE
                rootView.sp_winnerPoints.isEnabled = true
                rootView.textview_winner_points.isEnabled = true
                rootView.textview_winner_points.setTextColor(Color.BLACK)
                selectedView?.setTextColor(Color.BLACK)
            }
        }
        if (listaPuntiVincenti.isEmpty()) {
            rootView.sw_winnerPoints.isChecked = true
            rootView.sw_winnerPoints.isEnabled = false
        } else {
            val mAdapterSpinner = AdapterSpinner(rootView.context, R.layout.item_spinner_winner_point, listaPuntiVincenti)
            rootView.sp_winnerPoints.adapter = mAdapterSpinner
            val switchButton = rootView.sw_winnerPoints
            rootView.sp_winnerPoints.onItemSelectedListener = object : OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    switchButton.isChecked = false
                }

                override fun onNothingSelected(adapter: AdapterView<*>?) {}
            }
        }
    }

    private val winnerPoints: String
        private get() {
            view?.let { view ->
                if (view.sp_winnerPoints.isEnabled && view.sp_winnerPoints.selectedItem != null) {
                    return view.sp_winnerPoints.selectedItem.toString()
                }
                val inputUtente = view.bet_input_winner_points.text.toString()
                if (inputUtente.isEmpty()) {
                    view.bet_input_winner_points.showError()
                }
                return inputUtente

            }
            return ""
        }

    private fun goToPoints(idGame: Short) {
        val intent: Intent = when (gamePath) {
            GamePath.GAME_PATH_2_PLAYERS -> {
                Intent(this.context, Points2PActivity::class.java)
            }
            GamePath.GAME_PATH_3_PLAYERS -> {
                Intent(this.context, Points3PActivity::class.java)
            }
            GamePath.GAME_PATH_4_PLAYERS -> {
                Intent(this.context, Points4PActivity::class.java)
            }
            GamePath.GAME_PATH_2_GROUPS -> {
                Intent(this.context, Points2GroupsActivity::class.java)
            }
        }
        intent.putExtra(Constants.ID_GAME, idGame)
        this.startActivity(intent)
    }

    companion object {

        fun getFragmentNewGame(gamePath: GamePath): InsertDialogFragment {
            val f = InsertDialogFragment()
            val args = Bundle()
            args.putSerializable(GAME_PATH, gamePath)
            args.putSerializable(GAME_PATH, gamePath)
            args.putByte(STATUS_GAME_PARAM, GameStatus.TO_START)
            f.arguments = args
            return f
        }

        fun getDialogNewMatch(gamePath: GamePath, idGame: Short): InsertDialogFragment {
            val f = InsertDialogFragment()
            val args = Bundle()
            args.putSerializable(GAME_PATH, gamePath)
            args.putByte(STATUS_GAME_PARAM, GameStatus.FINISHED)
            args.putShort(ID_GAME_PARAM, idGame)
            f.arguments = args
            return f
        }

        fun getDialogExtend(gamePath: GamePath, idGame: Short, winner: String): InsertDialogFragment {
            val f = InsertDialogFragment()
            val args = Bundle()
            args.putSerializable(GAME_PATH, gamePath)
            args.putByte(STATUS_GAME_PARAM, GameStatus.EXTENDED)
            args.putShort(ID_GAME_PARAM, idGame)
            args.putString(WINNER_NAME_PARAM, winner)
            f.arguments = args
            return f
        }

        fun getFragmentExtendMandatory(gamePath: GamePath, idGame: Short): InsertDialogFragment {
            val f = InsertDialogFragment()
            val args = Bundle()
            args.putSerializable(GAME_PATH, gamePath)
            args.putByte(STATUS_GAME_PARAM, GameStatus.EXTENDED_MANDATORY)
            args.putShort(ID_GAME_PARAM, idGame)
            f.arguments = args
            return f
        }

        val STATUS_GAME_PARAM = "STATUS_GAME_PARAM"
        private val ID_GAME_PARAM = "ID_GAME_PARAM"
        private val WINNER_NAME_PARAM = "WINNER_PARAM"
        val TAG = InsertDialogFragment::class.java.canonicalName
    }
}