package com.ionvaranita.belotenote.shared.result;

import java.util.List;

public class WinnerResult {
    private List<Integer> winners;
    private Byte statusGame;
    private Short maxPoints;
    private Integer maxPersona;

    public Integer getMaxPersona() {
        return maxPersona;
    }

    public void setMaxPersona(Integer maxPersona) {
        this.maxPersona = maxPersona;
    }

    public Short getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(Short maxPoints) {
        this.maxPoints = maxPoints;
    }

    public List<Integer> getWinners() {
        return winners;
    }

    public void setWinners(List<Integer> winners) {
        this.winners = winners;
    }

    public Byte getStatusGame() {
        return statusGame;
    }

    public void setStatusGame(Byte statusGame) {
        this.statusGame = statusGame;
    }

}
