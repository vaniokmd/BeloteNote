package com.ionvaranita.belotenote.shared.constants;

/**
 * Created by ionvaranita on 15/04/18.
 */

public class Constants {
    public static final String STRING_BOLT = "B";
    public static final String STRING_MINUS_10 = "-10";
    public static final String ID_GAME = "info_game";

    public static final String ID_BUTTON_PRESSED_ARG = "button_pressed";
    public static final Integer NONE = -1;

    public static final String FRAGMENT_HOME = "fragment_home";

    public static final String DIALOG_FRAGMENT_ABOUT = "dialog_fragment_about";

    public static final String DIALOG_FRAGMENT_INFO_GAME = "dialog_fragment_info_game";
}
