package com.ionvaranita.belotenote.shared.viewmodel.factory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ionvaranita.belotenote.shared.constants.GamePath
import com.ionvaranita.belotenote.shared.viewmodel.table.TableViewModel

class TableViewModelFactory(private val application: Application, private val gamePath: GamePath): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TableViewModel(application, gamePath) as T
    }
}