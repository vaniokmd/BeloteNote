package com.ionvaranita.belotenote.shared.extensions

fun String.intOrEmpty(): String {
    return when(val v = toIntOrNull()) {
        null -> ""
        else -> v.toString()
    }
}
