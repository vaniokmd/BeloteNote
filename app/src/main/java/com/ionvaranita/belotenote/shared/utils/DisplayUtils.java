package com.ionvaranita.belotenote.shared.utils;

import android.content.res.Resources;

public class DisplayUtils {
    public static int convertDpAsPixels(int sizeInDp){
        float scale = Resources.getSystem().getDisplayMetrics().density;
        return (int) (sizeInDp*scale + 0.5f);
    }
}
