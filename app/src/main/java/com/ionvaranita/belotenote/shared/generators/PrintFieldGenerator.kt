package com.ionvaranita.belotenote.shared.generators

import android.util.TypedValue
import android.view.Gravity
import android.widget.TableRow
import com.ionvaranita.belotenote.shared.constants.IdsInputFieldUtils
import com.ionvaranita.belotenote.shared.constants.GamePath
import com.ionvaranita.belotenote.shared.customview.TextViewStyled

/**
 * Created by ionvaranita on 23/04/18.
 */
class PrintFieldGenerator(private val gamePath: GamePath) : FieldGenerator() {
    fun popolatePrintPointFields(row: TableRow, personsName: Map<Int, String>) {
        var idsPointFields: List<Int> = when (gamePath) {
            GamePath.GAME_PATH_2_PLAYERS -> IdsInputFieldUtils.getIdsNames2P()
            GamePath.GAME_PATH_3_PLAYERS -> IdsInputFieldUtils.getIdsNames3P()
            GamePath.GAME_PATH_4_PLAYERS -> IdsInputFieldUtils.getIdsNames4P()
            GamePath.GAME_PATH_2_GROUPS -> IdsInputFieldUtils.getIdsNames2Groups()
        }
        val fieldGame = TextViewStyled(row.context)
        fieldGame.id = IdsInputFieldUtils.ID_GAME_POINTS
        fieldGame.text = personsName[IdsInputFieldUtils.ID_GAME_POINTS]
        configureField(fieldGame)
        row.addView(fieldGame)
        for (i in idsPointFields.indices) {
            val field = TextViewStyled(row.context)
            field.id = idsPointFields[i]
            field.text = personsName[field.id]
            configureField(field)
            row.addView(field)
        }
    }

    private fun configureField(field: TextViewStyled) {
        field.layoutParams = layoutParams
        field.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
        field.maxLines = 1
        field.gravity = Gravity.CENTER
    }

}