package com.ionvaranita.belotenote.shared.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ionvaranita.belotenote.R;

import java.util.List;
public class AdapterSpinner extends ArrayAdapter<Short> {

    private final List<Short> winnningPoints;

    public AdapterSpinner(Context context, int textViewResourceId,
                          List<Short> winnningPoints) {
        super(context, textViewResourceId, winnningPoints);
        this.winnningPoints = winnningPoints;
        this.setDropDownViewResource(R.layout.item_spinner_winner_point);
    }

    @Override
    public int getCount(){
        return winnningPoints.size();
    }

    @Override
    public Short getItem(int position){
        return winnningPoints.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(winnningPoints.get(position).toString());
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(winnningPoints.get(position).toString());
        return label;
    }
}

