package com.ionvaranita.belotenote.shared.dialog

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.ionvaranita.belotenote.R
import com.ionvaranita.belotenote.shared.customview.TextViewStyled


/**
 * Created by ionvaranita on 2020-01-31;
 */
class WinnerDialogFragment : ShapedDialogFragment() {
    private var winnerName: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        winnerName = arguments?.getString(WINNER_NAME)
    }

    override fun onCreateView(layoutInflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.dialog_fragment_winner, container, false)
    }

    interface PointsActivityInteraction {
        fun onOkWinnerDialogClick()
        fun onShareScoreClick()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        view.findViewById<TextViewStyled>(R.id.tv_winner_name).text = winnerName
        view.findViewById<Button>(R.id.bt_ok_winner).setOnClickListener {
            dismiss()
        }
        view.findViewById<Button>(R.id.bt_share_score).setOnClickListener {
            (context as PointsActivityInteraction?)!!.onShareScoreClick()
            dismiss()

        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        (context as PointsActivityInteraction?)!!.onOkWinnerDialogClick()
    }

    companion object {
        private const val WINNER_NAME = "winner_name"
        const val WINNER_DIALOG_TAG = "winner_dialog_tag"
        fun getInstance(winnerName: String?): WinnerDialogFragment {
            val winnerDialogFragment = WinnerDialogFragment()
            val args = Bundle()
            args.putString(WINNER_NAME, winnerName)
            winnerDialogFragment.arguments = args
            return winnerDialogFragment
        }
    }
}