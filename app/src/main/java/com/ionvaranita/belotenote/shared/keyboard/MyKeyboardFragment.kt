package com.ionvaranita.belotenote.shared.keyboard

import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.os.Vibrator
import android.text.InputType
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import com.ionvaranita.belotenote.R
import com.ionvaranita.belotenote.shared.constants.Constants
import com.ionvaranita.belotenote.shared.customview.EditTextStyled
import com.ionvaranita.belotenote.shared.utils.ResourcesUtils.getComicRelief
import java.util.*

class MyKeyboardFragment : Fragment(), View.OnClickListener {
    var onEnterClick:() -> Unit = {}
    private var mField: EditTextStyled? = null
    private var myListener: MyKeyboardListener? = null
    private val mSetMinus10: MutableSet<Int> = HashSet()
    private var mIdBolt: Int? = null
    private var mVibe: Vibrator? = null
    private lateinit var mButtonBolt: Button
    private lateinit var mButtonMeno10: Button
    private val keyValues = SparseArray<String>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.my_keyboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        init(view)
    }

    private fun init(view: View) {
        // keyboard keys (buttons)
        mVibe = view.context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        val mButton1 = view.findViewById<Button>(R.id.button_1)
        val mButton2 = view.findViewById<Button>(R.id.button_2)
        val mButton3 = view.findViewById<Button>(R.id.button_3)
        val mButton4 = view.findViewById<Button>(R.id.button_4)
        val mButton5 = view.findViewById<Button>(R.id.button_5)
        val mButton6 = view.findViewById<Button>(R.id.button_6)
        val mButton7 = view.findViewById<Button>(R.id.button_7)
        val mButton8 = view.findViewById<Button>(R.id.button_8)
        val mButton9 = view.findViewById<Button>(R.id.button_9)
        val mButton0 = view.findViewById<Button>(R.id.button_0)
        mButtonBolt = view.findViewById(R.id.button_bolt)
        mButtonMeno10 = view.findViewById(R.id.button_meno10)
        val mButtonDelete = view.findViewById<ImageButton>(R.id.button_delete)
        val mButtonEnter = view.findViewById<ImageButton>(R.id.button_enter)
        mButton1.setOnClickListener(this)
        mButton2.setOnClickListener(this)
        mButton3.setOnClickListener(this)
        mButton4.setOnClickListener(this)
        mButton5.setOnClickListener(this)
        mButton6.setOnClickListener(this)
        mButton7.setOnClickListener(this)
        mButton8.setOnClickListener(this)
        mButton9.setOnClickListener(this)
        mButton0.setOnClickListener(this)
        mButtonBolt.setOnClickListener(this)
        mButtonMeno10.setOnClickListener(this)
        mButtonEnter.setOnClickListener(this)
        mButtonDelete.setOnClickListener(this)
        val typeface = getComicRelief(this.context!!)
        mButton1.setTypeface(typeface, Typeface.BOLD)
        mButton2.setTypeface(typeface, Typeface.BOLD)
        mButton3.setTypeface(typeface, Typeface.BOLD)
        mButton4.setTypeface(typeface, Typeface.BOLD)
        mButton5.setTypeface(typeface, Typeface.BOLD)
        mButton6.setTypeface(typeface, Typeface.BOLD)
        mButton7.setTypeface(typeface, Typeface.BOLD)
        mButton8.setTypeface(typeface, Typeface.BOLD)
        mButton9.setTypeface(typeface, Typeface.BOLD)
        mButton0.setTypeface(typeface, Typeface.BOLD)
        mButtonBolt.setTypeface(typeface, Typeface.BOLD)

        // map buttons IDs to input strings
        keyValues.put(R.id.button_1, "1")
        keyValues.put(R.id.button_2, "2")
        keyValues.put(R.id.button_3, "3")
        keyValues.put(R.id.button_4, "4")
        keyValues.put(R.id.button_5, "5")
        keyValues.put(R.id.button_6, "6")
        keyValues.put(R.id.button_7, "7")
        keyValues.put(R.id.button_8, "8")
        keyValues.put(R.id.button_9, "9")
        keyValues.put(R.id.button_0, "0")
        keyValues.put(R.id.button_bolt, Constants.STRING_BOLT)
        keyValues.put(R.id.button_meno10, Constants.STRING_MINUS_10)
    }

    override fun onClick(v: View) {
        if (mField == null) return
        mVibe!!.vibrate(30)
        val isMeno10 = mSetMinus10.contains(mField!!.id)
        val isBolt = mIdBolt != null && mIdBolt == mField!!.id
        val value = keyValues[v.id]
        if (v.id == R.id.button_delete) {
            if (isMeno10 || isBolt) {
                mSetMinus10.remove(mField!!.id)
                mIdBolt = null
                clearText()
            } else {
                val fieldValue = mField!!.text.toString()
                if (fieldValue.isEmpty() || fieldValue.length == 1) {
                    mField!!.setText("")
                } else {
                    mField!!.setText(fieldValue.subSequence(0, fieldValue.length - 1))
                }
            }
        } else if (v.id == R.id.button_bolt) {
            if (!isBolt) {
                clearText()
                myListener!!.setInputType(mField!!.id, InputType.TYPE_CLASS_TEXT)
                mField!!.setText(value)
                myListener!!.setInputType(mField!!.id, InputType.TYPE_NUMBER_FLAG_SIGNED)
                myListener!!.clearFiled(mIdBolt)
                mIdBolt = mField!!.id
                mSetMinus10.remove(mField!!.id)
            }
        } else if (v.id == R.id.button_meno10) {
            if (!isMeno10) {
                clearText()
                mField!!.setText(value)
                mSetMinus10.add(mField!!.id)
                if (mIdBolt != null && mIdBolt == mField!!.id) {
                    mIdBolt = null
                }
            }
        } else if (v.id == R.id.button_enter) {
            onEnterClick()
        } else {
            if (isMeno10 || isBolt) {
                clearText()
                if (isBolt) {
                    mIdBolt = null
                } else {
                    mSetMinus10.remove(mField!!.id)
                }
            }
            val fieldValue = mField!!.text.toString()
            mField!!.setText(fieldValue + value)
        }
        (context as PointsActivityInteraction?)?.calcolateRemainingField(mField!!.id)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        myListener = if (context is MyKeyboardListener) {
            context
        } else {
            throw RuntimeException(context.toString()
                    + " must implement PointsActivityInteraction")
        }
    }

    private fun clearText() {
        mField!!.text!!.clear()
    }

    fun setField(field: EditTextStyled?) {
        mField = field
    }

    fun clearBoltMeno10() {
        mIdBolt = null
        mSetMinus10.clear()
    }

    fun setGameButtonsVisibility(isVisible: Boolean) {
        if (isVisible) {
            mButtonBolt.visibility = View.VISIBLE
            mButtonMeno10.visibility = View.VISIBLE
        } else {
            mButtonBolt.visibility = View.GONE
            mButtonMeno10.visibility = View.GONE
        }
    }

    interface MyKeyboardListener {
        fun clearFiled(idField: Int?)
        fun setInputType(idField: Int, inputType: Int)
    }

    interface MyKeyboardEnterClickListener {
        fun onEnterClick()
    }

    interface PointsActivityInteraction {
        fun calcolateRemainingField(idField: Int?)
    }
}