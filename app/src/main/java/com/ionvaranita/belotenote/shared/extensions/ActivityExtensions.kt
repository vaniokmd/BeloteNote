package com.ionvaranita.belotenote.shared.extensions

import android.app.Activity
import android.content.ActivityNotFoundException

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity

const val APP_VIDEO_ID = "whsLX6-O7VI"

fun FragmentActivity.watchAppVideo() {
    val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$APP_VIDEO_ID"))
    val webIntent = Intent(
        Intent.ACTION_VIEW,
        Uri.parse("http://www.youtube.com/watch?v=$APP_VIDEO_ID")
    )
    try {
        startActivity(appIntent)
    } catch (ex: ActivityNotFoundException) {
        startActivity(webIntent)
    }
}

fun FragmentActivity.getMarketUrl(): String {
    return "market://details?id=" +applicationContext.packageName
}