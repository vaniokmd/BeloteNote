package com.ionvaranita.belotenote.shared.result

import java.util.*

data class GameResult(
        val idGame: Short,
        val winnerPoints: Short,
        val color: Int,
        val statusGame: Byte,
        val nameMap: SortedMap<Int, String>
)