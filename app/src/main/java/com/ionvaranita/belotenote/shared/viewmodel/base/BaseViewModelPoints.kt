package com.ionvaranita.belotenote.shared.viewmodel.base

import android.app.Application
import com.ionvaranita.belotenote.database.entity.BoltEntity
import com.ionvaranita.belotenote.database.entity.BoltManagerEntity
import com.ionvaranita.belotenote.shared.constants.GameStatus
import com.ionvaranita.belotenote.shared.constants.IdsInputFieldUtils
import com.ionvaranita.belotenote.shared.result.WinnerResult
import java.util.ArrayList

open class BaseViewModelPoints(application: Application): BaseViewModel(application) {
    protected fun insertBolt(boltEntity: BoltEntity) {
        var nrBolt = boltEntity.nrBolt
        if (nrBolt == null || nrBolt.toInt() == 2) {
            nrBolt = 1.toByte()
        } else {
            ++nrBolt
        }
        boltEntity.nrBolt = nrBolt
        db.boltDao().insert(boltEntity)
        if (nrBolt.toInt() == 2) {
            val boltManagerEntity = BoltManagerEntity(boltEntity.path)
            boltManagerEntity.idGame = boltEntity.idGame
            boltManagerEntity.idPerson = boltEntity.idPersonBolt
            boltManagerEntity.minus10 = true
            db.boltManagerDao().update(boltManagerEntity)
        }
    }

    protected fun getWinner(pointMap: Map<Int, Short?>, winnerPoints: Short): WinnerResult {
        val playerPointMap = pointMap.toMutableMap()
        playerPointMap.remove(IdsInputFieldUtils.ID_GAME_POINTS)
        val infoWinner = WinnerResult()
        val winners: MutableList<Int> = ArrayList()
        val statusGame: Byte
        val idPersone = playerPointMap.keys
        var maxPointsPerson: Short? = null
        var maxIdPersona: Int? = null
        var sameValue = false
        for (idPerson in idPersone) {
            val points = playerPointMap[idPerson]
            if (points != null && points >= winnerPoints) {
                winners.add(idPerson)
                if (maxPointsPerson == null) {
                    maxPointsPerson = points
                    maxIdPersona = idPerson
                    continue
                }
                if (points > maxPointsPerson) {
                    maxPointsPerson = points
                    maxIdPersona = idPerson
                } else if (points == maxPointsPerson) {
                    sameValue = true
                }
            }
        }
        statusGame = if (winners.isEmpty()) {
            GameStatus.CONTINUE
        } else if (winners.size == 1) {
            GameStatus.FINISHED
        } else {
            if (sameValue) {
                GameStatus.EXTENDED_MANDATORY
            } else {
                GameStatus.EXTENDED
            }
        }
        infoWinner.maxPersona = maxIdPersona
        infoWinner.winners = winners
        infoWinner.statusGame = statusGame
        infoWinner.maxPoints = maxPointsPerson
        return infoWinner
    }
}