package com.ionvaranita.belotenote.shared.viewmodel.factory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ionvaranita.belotenote.main.viewmodel.MainViewModel
import com.ionvaranita.belotenote.points.viewmodel.PointsViewModel
import com.ionvaranita.belotenote.shared.constants.GamePath
import com.ionvaranita.belotenote.shared.viewmodel.base.BaseViewModel

class PointsViewModelFactory(private val application: Application, private val idGame: Short, private val gamePath: GamePath) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        when (modelClass.canonicalName) {

            PointsViewModel::class.java.canonicalName -> {
                return PointsViewModel(application,idGame, gamePath) as T
            }
        }
        return BaseViewModel(application) as T
    }
}

class MainViewModelFactory(private val application: Application) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        when (modelClass.canonicalName) {

            MainViewModel::class.java.canonicalName -> {
                return MainViewModel(application) as T
            }
        }
        return MainViewModel(application) as T
    }
}