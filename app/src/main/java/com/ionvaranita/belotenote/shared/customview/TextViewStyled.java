package com.ionvaranita.belotenote.shared.customview;

import android.content.Context;
import android.graphics.Typeface;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.ionvaranita.belotenote.shared.utils.ResourcesUtils;

/**
 * Created by ionvaranita on 2019-10-10;
 */
public class TextViewStyled extends TextView {


    public TextViewStyled(Context context) {
        super(context);
        setPelmeshkaTypeFace();
    }

    public TextViewStyled(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setPelmeshkaTypeFace();

    }

    public TextViewStyled(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setPelmeshkaTypeFace();
    }

    private void setPelmeshkaTypeFace() {
        setTypeface(ResourcesUtils.INSTANCE.getComicRelief(this.getContext()), Typeface.BOLD);
    }
}
