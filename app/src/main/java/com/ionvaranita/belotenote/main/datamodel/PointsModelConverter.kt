package com.ionvaranita.belotenote.main.datamodel

import android.content.Context
import com.ionvaranita.belotenote.R
import com.ionvaranita.belotenote.database.entity.BoltEntity
import com.ionvaranita.belotenote.database.entity.groups2.Points2GroupsEntity
import com.ionvaranita.belotenote.database.entity.groups2.Scor2GroupsEntity
import com.ionvaranita.belotenote.database.entity.players2.Points2PEntity
import com.ionvaranita.belotenote.database.entity.players2.Scor2PEntity
import com.ionvaranita.belotenote.database.entity.players3.Points3PEntity
import com.ionvaranita.belotenote.database.entity.players3.Scor3PEntity
import com.ionvaranita.belotenote.database.entity.players4.Points4PEntity
import com.ionvaranita.belotenote.database.entity.players4.Scor4PEntity
import com.ionvaranita.belotenote.shared.constants.Constants
import com.ionvaranita.belotenote.shared.constants.IdsInputFieldUtils

class PointsModelConverter(private val context: Context) {

    private fun convertPoints2P(points2PEntity: Points2PEntity, boltEntity: BoltEntity): HashMap<Int, String> {
        val pointsMap = convertPoints2P(points2PEntity)
        pointsMap[boltEntity.idPersonBolt] = context.getString(R.string.bolt, boltEntity.nrBolt.toString())
        return pointsMap
    }

    private fun convertPoints2P(points2PEntity: Points2PEntity): HashMap<Int, String> {
        val pointsMap = HashMap<Int, String>()
        pointsMap[IdsInputFieldUtils.ID_GAME_POINTS] = points2PEntity.pointsGame.toString()
        pointsMap[IdsInputFieldUtils.ID_PERSON_2_1] = points2PEntity.pointsMe.toString()
        pointsMap[IdsInputFieldUtils.ID_PERSON_2_2] = points2PEntity.pointsYouS.toString()
        return pointsMap
    }

    fun convertPoints2P(points2PEntities: List<Points2PEntity>, boltEntities: List<BoltEntity>): ArrayList<HashMap<Int, String>> {
        val integerBoltEntityMap: MutableMap<Int, BoltEntity> = java.util.HashMap()
        val pointsMap: ArrayList<HashMap<Int, String>> = java.util.ArrayList()
        for (boltEntity in boltEntities) {
            integerBoltEntityMap[boltEntity.idRow] = boltEntity
        }
        for (points2PEntity in points2PEntities) {
            val idRow = points2PEntity.id
            if (integerBoltEntityMap.containsKey(idRow)) {
                pointsMap.add(convertPoints2P(points2PEntity, integerBoltEntityMap[idRow]!!))
                integerBoltEntityMap.remove(idRow)
            } else {
                pointsMap.add(convertPoints2P(points2PEntity))
            }
        }
        return pointsMap
    }

    fun convertPoints3P(points3PEntities: List<Points3PEntity>, boltEntities: List<BoltEntity>): ArrayList<HashMap<Int, String>> {
        val integerBoltEntityMap: MutableMap<Int, BoltEntity> = java.util.HashMap()
        val pointsMap: ArrayList<HashMap<Int, String>> = java.util.ArrayList()
        for (boltEntity in boltEntities) {
            integerBoltEntityMap[boltEntity.idRow] = boltEntity
        }
        for (points3PEntity in points3PEntities) {
            val idRow = points3PEntity.id
            if (integerBoltEntityMap.containsKey(idRow)) {
                pointsMap.add(convertPoints3P(points3PEntity, integerBoltEntityMap[idRow]!!))
                integerBoltEntityMap.remove(idRow)
            } else {
                pointsMap.add(convertPoints3P(points3PEntity))
            }
        }
        return pointsMap
    }
    private fun convertPoints3P(points3PEntity: Points3PEntity): HashMap<Int, String> {
        val pointsMap = HashMap<Int, String>()
        pointsMap[IdsInputFieldUtils.ID_GAME_POINTS] = points3PEntity.pointsGame.toString()
        pointsMap[IdsInputFieldUtils.ID_PERSON_3_1] = points3PEntity.pointsP1.toString()
        pointsMap[IdsInputFieldUtils.ID_PERSON_3_2] = points3PEntity.pointsP2.toString()
        pointsMap[IdsInputFieldUtils.ID_PERSON_3_3] = points3PEntity.pointsP3.toString()
        return pointsMap
    }

    private fun convertPoints3P(points3PEntity: Points3PEntity, boltEntity: BoltEntity): HashMap<Int, String> {
        val pointsMap =convertPoints3P(points3PEntity)
            pointsMap[boltEntity.idPersonBolt] = Constants.STRING_BOLT
        return pointsMap
    }

    fun convertPoints4P(points4PEntities: List<Points4PEntity>, boltEntities: List<BoltEntity>): ArrayList<HashMap<Int, String>> {
        val integerBoltEntityMap: MutableMap<Int, BoltEntity> = java.util.HashMap()
        val pointsMap: ArrayList<HashMap<Int, String>> = java.util.ArrayList()
        for (boltEntity in boltEntities) {
            integerBoltEntityMap[boltEntity.idRow] = boltEntity
        }
        for (points4PEntity in points4PEntities) {
            val idRow = points4PEntity.id
            if (integerBoltEntityMap.containsKey(idRow)) {
                pointsMap.add(convertPoints4P(points4PEntity, integerBoltEntityMap[idRow]!!))
                integerBoltEntityMap.remove(idRow)
            } else {
                pointsMap.add(convertPoints4P(points4PEntity))
            }
        }
        return pointsMap
    }
    private fun convertPoints4P(points4PEntity: Points4PEntity): HashMap<Int, String> {
        val pointsMap = HashMap<Int, String>()
        pointsMap[IdsInputFieldUtils.ID_GAME_POINTS] = points4PEntity.pointsGame.toString()
        pointsMap[IdsInputFieldUtils.ID_PERSON_4_1] = points4PEntity.pointsP1.toString()
        pointsMap[IdsInputFieldUtils.ID_PERSON_4_2] = points4PEntity.pointsP2.toString()
        pointsMap[IdsInputFieldUtils.ID_PERSON_4_3] = points4PEntity.pointsP3.toString()
        pointsMap[IdsInputFieldUtils.ID_PERSON_4_4] = points4PEntity.pointsP4.toString()
        return pointsMap
    }

    private fun convertPoints4P(points4PEntity: Points4PEntity, boltEntity: BoltEntity): HashMap<Int, String> {
        val pointsMap =convertPoints4P(points4PEntity)
        pointsMap[boltEntity.idPersonBolt] = Constants.STRING_BOLT
        return pointsMap
    }

    private fun convertPoints2Groups(points2GroupsEntity: Points2GroupsEntity, boltEntity: BoltEntity): HashMap<Int, String> {
        val pointsMap = convertPoints2Groups(points2GroupsEntity)
        pointsMap[boltEntity.idPersonBolt] = context.getString(R.string.bolt, boltEntity.nrBolt.toString())
        return pointsMap
    }

    private fun convertPoints2Groups(points2GroupsEntity: Points2GroupsEntity): HashMap<Int, String> {
        val pointsMap = HashMap<Int, String>()
        pointsMap[IdsInputFieldUtils.ID_GAME_POINTS] = points2GroupsEntity.pointsGame.toString()
        pointsMap[IdsInputFieldUtils.ID_TEAM_1] = points2GroupsEntity.pointsWe.toString()
        pointsMap[IdsInputFieldUtils.ID_TEAM_2] = points2GroupsEntity.pointsYouP.toString()
        return pointsMap
    }

    fun convertPoints2Groups(points2GroupsEntities: List<Points2GroupsEntity>, boltEntities: List<BoltEntity>): ArrayList<HashMap<Int, String>> {
        val integerBoltEntityMap: MutableMap<Int, BoltEntity> = java.util.HashMap()
        val pointsMap: ArrayList<HashMap<Int, String>> = java.util.ArrayList()
        for (boltEntity in boltEntities) {
            integerBoltEntityMap[boltEntity.idRow] = boltEntity
        }
        for (points2GroupsEntity in points2GroupsEntities) {
            val idRow = points2GroupsEntity.id
            if (integerBoltEntityMap.containsKey(idRow)) {
                pointsMap.add(convertPoints2Groups(points2GroupsEntity, integerBoltEntityMap[idRow]!!))
                integerBoltEntityMap.remove(idRow)
            } else {
                pointsMap.add(convertPoints2Groups(points2GroupsEntity))
            }
        }
        return pointsMap
    }

    fun convertScor2P(scor2PEntity: Scor2PEntity): LinkedHashMap<Int, String> {
        val scorMap = LinkedHashMap<Int, String>()
        scorMap[IdsInputFieldUtils.ID_PERSON_2_1] = scor2PEntity.scorMe.toString()
        scorMap[IdsInputFieldUtils.ID_PERSON_2_2] = scor2PEntity.scorYouS.toString()
        return scorMap
    }

    fun convertScor3P(scor3PEntity: Scor3PEntity): LinkedHashMap<Int, String> {
        val scorMap = LinkedHashMap<Int, String>()
        scorMap[IdsInputFieldUtils.ID_PERSON_3_1] = scor3PEntity.scorP1.toString()
        scorMap[IdsInputFieldUtils.ID_PERSON_3_2] = scor3PEntity.scorP2.toString()
        scorMap[IdsInputFieldUtils.ID_PERSON_3_3] = scor3PEntity.scorP3.toString()
        return scorMap
    }

    fun convertScor4P(scor4PEntity: Scor4PEntity): LinkedHashMap<Int, String> {
        val scorMap = LinkedHashMap<Int, String>()
        scorMap[IdsInputFieldUtils.ID_PERSON_4_1] = scor4PEntity.scorP1.toString()
        scorMap[IdsInputFieldUtils.ID_PERSON_4_2] = scor4PEntity.scorP2.toString()
        scorMap[IdsInputFieldUtils.ID_PERSON_4_3] = scor4PEntity.scorP3.toString()
        scorMap[IdsInputFieldUtils.ID_PERSON_4_4] = scor4PEntity.scorP4.toString()
        return scorMap
    }

    fun convertScor2Groups(scor2GroupsEntity: Scor2GroupsEntity): LinkedHashMap<Int, String> {
        val scorMap = LinkedHashMap<Int, String>()
        scorMap[IdsInputFieldUtils.ID_TEAM_1] = scor2GroupsEntity.scorWe.toString()
        scorMap[IdsInputFieldUtils.ID_TEAM_2] = scor2GroupsEntity.scorYouP.toString()
        return scorMap
    }
}