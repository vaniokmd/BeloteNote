package com.ionvaranita.belotenote.main.datamodel

import android.content.Context
import com.ionvaranita.belotenote.R
import com.ionvaranita.belotenote.database.entity.groups2.Game2GroupsEntity
import com.ionvaranita.belotenote.database.entity.players2.Game2PEntity
import com.ionvaranita.belotenote.database.entity.players3.Game3PEntity
import com.ionvaranita.belotenote.database.entity.players4.Game4PEntity
import com.ionvaranita.belotenote.shared.utils.DateUtils

class TableModelConverter(private val contex: Context) {
    private val dateUtils by lazy { DateUtils(contex) }

    private fun convert(game2PEntity: Game2PEntity): TableDataModel {
        return getGameDo(game2PEntity.idGame, game2PEntity.statusGame, game2PEntity.dateGame,game2PEntity.color)
    }

    private fun convert(game3PEntity: Game3PEntity): TableDataModel {
        return getGameDo(game3PEntity.idGame, game3PEntity.statusGame, game3PEntity.dateGame, game3PEntity.color)
    }

    private fun convert(game4PEntity: Game4PEntity): TableDataModel {
        return getGameDo(game4PEntity.idGame, game4PEntity.statusGame, game4PEntity.dateGame, game4PEntity.color)
    }

    private fun convert(game2GroupsEntity: Game2GroupsEntity): TableDataModel {
        return getGameDo(game2GroupsEntity.idGame, game2GroupsEntity.statusGame, game2GroupsEntity.dateGame, game2GroupsEntity.color)
    }

    private fun getGameDo(idGame: Short, statusGame: Byte, dateGame: Long, color: Int): TableDataModel {
        val tableName = contex.getString(R.string.table, idGame.toString())
        val statusGame = statusGame
        val dateGame = dateUtils.getDateFormated(dateGame)
        return TableDataModel(idGame, tableName, statusGame, dateGame, color)
    }

    fun convertTable2P(game2PEntities: List<Game2PEntity>): ArrayList<TableDataModel> {
        val result = ArrayList<TableDataModel>()
        for (game2PEntity in game2PEntities) {
            result.add(convert(game2PEntity))
        }
        return result
    }

    fun convertTable3P(game3PEntities: List<Game3PEntity>): ArrayList<TableDataModel> {
        val result = ArrayList<TableDataModel>()
        for (game3PEntity in game3PEntities) {
            result.add(convert(game3PEntity))
        }
        return result
    }

    fun convertTable4P(game4PEntities: List<Game4PEntity>): ArrayList<TableDataModel> {
        val result = ArrayList<TableDataModel>()
        for (game4PEntity in game4PEntities) {
            result.add(convert(game4PEntity))
        }
        return result
    }

    fun convertTable2Groups(game2GroupsEntities: List<Game2GroupsEntity>): ArrayList<TableDataModel> {
        val result = ArrayList<TableDataModel>()
        for (game2GroupsEntity in game2GroupsEntities) {
            result.add(convert(game2GroupsEntity))
        }
        return result
    }

}