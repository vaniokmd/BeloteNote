package com.ionvaranita.belotenote.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.ionvaranita.belotenote.R
import com.ionvaranita.belotenote.main.viewmodel.MainViewModel
import com.ionvaranita.belotenote.shared.customview.TextViewStyled
import com.ionvaranita.belotenote.shared.livedata.EventObserver
import com.ionvaranita.belotenote.shared.utils.ResourcesUtils
import com.ionvaranita.belotenote.shared.viewmodel.factory.MainViewModelFactory


class HomeFragment : Fragment() {


    private lateinit var mainViewModel: MainViewModel
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.startAnimation(AnimationUtils.loadAnimation(context, R.anim.bottom_to_top))
        view.findViewById<LinearLayout>(R.id.wrap_languageSelect).setOnClickListener {
            LanguageDialogFragment.getInstance().show(parentFragmentManager, LanguageDialogFragment.TAG)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.run {
            mainViewModel = ViewModelProvider(this, MainViewModelFactory(application)).get(MainViewModel::class.java)
            mainViewModel.languageChangeEvent.observe(this, EventObserver{
                finish()
                startActivity(intent)
            })

        }
        mainViewModel.language.observe(viewLifecycleOwner, Observer {
            view?.findViewById<ImageView>(R.id.iv_nationalFlag)?.setImageResource(ResourcesUtils.getFlagByLocale(it))
            view?.findViewById<TextViewStyled>(R.id.tv_languageName)?.text = ResourcesUtils.getLanguageByLocale(it)
        })
    }

    companion object {
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }
}