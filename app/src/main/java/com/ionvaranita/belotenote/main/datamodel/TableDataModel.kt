package com.ionvaranita.belotenote.main.datamodel

data class TableDataModel(
        val idGame: Short,
        val tableName: String,
        val statusGame: Byte,
        val dateGame: String,
        val color: Int
)