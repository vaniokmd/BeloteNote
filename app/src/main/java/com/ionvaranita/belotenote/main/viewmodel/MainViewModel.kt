package com.ionvaranita.belotenote.main.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ionvaranita.belotenote.App
import com.ionvaranita.belotenote.shared.SharePreferencesManager
import com.ionvaranita.belotenote.shared.livedata.Event
import com.yariksoffice.lingver.Lingver

class MainViewModel(application: Application) : AndroidViewModel(application) {
    private val _languageChangeEvent = MutableLiveData<Event<Unit>>()
    val languageChangeEvent: LiveData<Event<Unit>> = _languageChangeEvent

    private val _language = MutableLiveData<String>()
    val language: LiveData<String> = _language

    fun changeLanguage(language: String) {
        if (language != App.languageSelected) {
            App.languageSelected = language
            Lingver.getInstance().setLocale(getApplication<App>().applicationContext, App.languageSelected)
            _languageChangeEvent.value = Event(Unit)
        }
    }


    init {
        _language.value = App.languageSelected
    }
}