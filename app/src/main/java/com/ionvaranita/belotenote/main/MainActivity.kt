package com.ionvaranita.belotenote.main

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.ContextMenu
import android.view.ContextMenu.ContextMenuInfo
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.google.android.gms.ads.MobileAds
import com.ionvaranita.belotenote.R
import com.ionvaranita.belotenote.shared.constants.Constants
import com.ionvaranita.belotenote.shared.dialog.AboutDialogFragment
import com.ionvaranita.belotenote.main.tables.TablesFragment
import com.ionvaranita.belotenote.shared.utils.AppUtils
import com.ionvaranita.belotenote.shared.utils.DeviceUtils

class MainActivity : AppCompatActivity() {
    private var mIdButtonPressed = Constants.NONE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        if (!DeviceUtils.isTablet(this)) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT
        }
        setContentView(R.layout.activity_main)
        MobileAds.initialize(this)
        setHomeFragment()
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setCustomView(R.layout.action_bar_custom)
    }

    public override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(Constants.ID_BUTTON_PRESSED_ARG, mIdButtonPressed!!)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        mIdButtonPressed = savedInstanceState.getInt(Constants.ID_BUTTON_PRESSED_ARG)
        super.onRestoreInstanceState(savedInstanceState)
    }

    private fun setHomeFragment() {
        if (supportFragmentManager.fragments.size == 0) {
            val homeFragment = HomeFragment.newInstance()
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.add(
                R.id.frame_layout_fragment_main,
                homeFragment,
                Constants.FRAGMENT_HOME
            )
            fragmentTransaction.commit()
        }
    }

    fun onButtonClick(view: View) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        if (DeviceUtils.isTablet(this)) {
            if (mIdButtonPressed == null || mIdButtonPressed != null && mIdButtonPressed != view.id) {
                mIdButtonPressed = view.id
                val fragmentTransaction = supportFragmentManager.beginTransaction()
                val tablesFragment = TablesFragment.newInstance(view.id)
                fragmentTransaction.add(R.id.frame_layout_fragment_tables, tablesFragment, TablesFragment.TAG)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()
            }
        } else if (!DeviceUtils.isTablet(this)) {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            val tablesFragment = TablesFragment.newInstance(view.id)
            fragmentTransaction.replace(R.id.frame_layout_fragment_main, tablesFragment, TablesFragment.TAG)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.app_menu, menu)
        return true
    }

    override fun onCreateContextMenu(
        menu: ContextMenu, v: View,
        menuInfo: ContextMenuInfo
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val inflater = menuInflater
        inflater.inflate(R.menu.app_menu, menu)
    }

    override fun onBackPressed() {
        if (DeviceUtils.isTablet(this)) {
            startActivity(AppUtils.homeItent)
        } else {
            val homeFragment =
                supportFragmentManager.findFragmentByTag(Constants.FRAGMENT_HOME) as HomeFragment?
            if (!homeFragment!!.isVisible) {
                supportActionBar!!.setDisplayHomeAsUpEnabled(false)
                super.onBackPressed()
            } else {
                startActivity(AppUtils.homeItent)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.about -> {
                val fragmentManager = supportFragmentManager
                AboutDialogFragment.newInstance()
                    .show(fragmentManager, Constants.DIALOG_FRAGMENT_ABOUT)
                true
            }
            R.id.share -> {
                AppUtils.shareApp(this)
                true
            }
            else -> {
                onBackPressed()
                true
            }
        }
    }
}