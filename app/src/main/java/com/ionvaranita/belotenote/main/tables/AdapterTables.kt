package com.ionvaranita.belotenote.main.tables

import android.content.Intent
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ionvaranita.belotenote.R
import com.ionvaranita.belotenote.points.activities.Points2PActivity
import com.ionvaranita.belotenote.points.activities.Points2GroupsActivity
import com.ionvaranita.belotenote.points.activities.Points3PActivity
import com.ionvaranita.belotenote.points.activities.Points4PActivity
import com.ionvaranita.belotenote.shared.constants.Constants
import com.ionvaranita.belotenote.shared.constants.GamePath
import com.ionvaranita.belotenote.main.datamodel.TableDataModel
import com.ionvaranita.belotenote.shared.utils.ResourcesUtils.getImageResByStatus
import kotlinx.android.synthetic.main.item_table.view.*
import java.util.*

/**
 * Created by ionvaranita on 2019-08-29;
 */
class AdapterTables(private val gamePath: GamePath, val items: ArrayList<TableDataModel>) : RecyclerView.Adapter<AdapterTables.ViewHolder>() {
    fun delete(position: Int): Short {
        val idGameToDelete: Short = items.removeAt(position).idGame
        notifyItemRemoved(position)
        return idGameToDelete
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_table, parent, false)
        return ViewHolder(view, gamePath)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(items[position])

    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder internal constructor(itemView: View, private val gamePath: GamePath) : RecyclerView.ViewHolder(itemView) {

        fun bind(tableDataModel: TableDataModel) {
            itemView.wrap_item_game.background.setColorFilter(tableDataModel.color, PorterDuff.Mode.DARKEN)
            itemView.tv_nameGame.text = itemView.context.getString(R.string.table, tableDataModel.idGame.toString())
            itemView.tv_dateGame.text = tableDataModel.dateGame
            itemView.iv_statusGame.setImageResource(getImageResByStatus(tableDataModel.statusGame))
            itemView.wrap_item_game.setOnClickListener {
                val activityClass = when (gamePath) {
                    GamePath.GAME_PATH_2_PLAYERS -> {
                        Points2PActivity::class.java
                    }
                    GamePath.GAME_PATH_3_PLAYERS -> {
                        Points3PActivity::class.java
                    }
                    GamePath.GAME_PATH_4_PLAYERS -> {
                        Points4PActivity::class.java
                    }
                    GamePath.GAME_PATH_2_GROUPS -> {
                        Points2GroupsActivity::class.java
                    }
                }
                val intent = Intent(itemView.context, activityClass)
                intent.putExtra(Constants.ID_GAME, tableDataModel.idGame)
                itemView.context.startActivity(intent)
            }
        }

    }

}