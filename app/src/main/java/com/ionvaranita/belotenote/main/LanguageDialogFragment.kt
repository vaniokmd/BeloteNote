package com.ionvaranita.belotenote.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RadioGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.ionvaranita.belotenote.R
import com.ionvaranita.belotenote.main.viewmodel.MainViewModel
import com.ionvaranita.belotenote.shared.dialog.ShapedDialogFragment
import com.ionvaranita.belotenote.shared.utils.ResourcesUtils
import com.ionvaranita.belotenote.shared.utils.ResourcesUtils.getLocaleById
import com.ionvaranita.belotenote.shared.viewmodel.factory.MainViewModelFactory


class LanguageDialogFragment : ShapedDialogFragment() {
    private lateinit var mainViewModel: MainViewModel
    private lateinit var radioGroup: RadioGroup
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.lang_select_frag, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.run {
            mainViewModel = ViewModelProvider(this, MainViewModelFactory(application)).get(MainViewModel::class.java)
        }
        mainViewModel.language.observe(viewLifecycleOwner, Observer {
            radioGroup.check(ResourcesUtils.getIdByLocale(it))
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        radioGroup = view.findViewById(R.id.rg_languages)
        view.findViewById<Button>(R.id.bt_ok_selectLanguage).setOnClickListener {
            val idSelected: Int = radioGroup.checkedRadioButtonId
            val language = getLocaleById(idSelected)
            mainViewModel.changeLanguage(language)
            dismiss()
        }
    }

    override fun onStart() {
        super.onStart()
        dialog?.setCanceledOnTouchOutside(true)
    }

    companion object {
        fun getInstance(): LanguageDialogFragment {
            return LanguageDialogFragment()
        }

        val TAG = LanguageDialogFragment::class.java.canonicalName
    }
}