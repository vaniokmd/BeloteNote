package com.ionvaranita.belotenote.main.tables

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.material.snackbar.Snackbar
import com.ionvaranita.belotenote.R
import com.ionvaranita.belotenote.shared.constants.GamePath
import com.ionvaranita.belotenote.shared.constants.Paths
import com.ionvaranita.belotenote.shared.constants.Paths.GAME_PATH
import com.ionvaranita.belotenote.main.datamodel.TableDataModel
import com.ionvaranita.belotenote.shared.dialog.InsertDialogFragment
import com.ionvaranita.belotenote.shared.utils.AdMobUtils.getAdSize
import com.ionvaranita.belotenote.shared.utils.ResourcesUtils.getHeaderTablesImg
import com.ionvaranita.belotenote.shared.viewmodel.table.TableViewModel
import com.ionvaranita.belotenote.shared.viewmodel.factory.TableViewModelFactory
import kotlinx.android.synthetic.main.fragment_tables.*
import kotlinx.android.synthetic.main.fragment_tables.view.*
import kotlinx.coroutines.launch
import java.util.logging.Logger

class TablesFragment : Fragment() {
    private lateinit var adapterTables: AdapterTables
    private lateinit var viewModel: TableViewModel
    private lateinit var gamePath: GamePath
    private lateinit var adView: AdView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            gamePath = it.getSerializable(GAME_PATH) as GamePath
        }
        activity?.run {
            viewModel = ViewModelProvider(this, TableViewModelFactory(application, gamePath)).get(TableViewModel::class.java)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tables, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.iv_menu_table_header.setImageResource(getHeaderTablesImg(gamePath))
        view.tablesRecyclerView.layoutManager = LinearLayoutManager(context)

        viewModel = ViewModelProvider(this, TableViewModelFactory(requireActivity().application, gamePath)).get(TableViewModel::class.java)

        val path = arguments?.getSerializable(Paths.GAME_PATH) as GamePath

       // viewModel.fetchTables(path)

        viewModel.observableTables.observe(viewLifecycleOwner, Observer {
            manageTable(it)
            enableSwipeToDeleteAndUndo(path)
        })


        view.fb_insertTable.setOnClickListener {
            onInsertGame(path)
        }
        initBanner()
    }

    private fun initBanner() {
        adView = AdView(requireContext())
        adView.adUnitId = getString(R.string.banner_ad_unit_id)
        adView.adSize = getAdSize(requireActivity())
        adview_container.addView(adView)
    }

    private fun loadBanner() {
        Log.d(TAG,"loadBanner called")
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    override fun onResume() {
        viewModel.fetchTables()
        super.onResume()
    }


    private fun manageTable(tableDataModels: ArrayList<TableDataModel>) {
        adapterTables = AdapterTables(gamePath, tableDataModels)
        view?.tablesRecyclerView?.adapter = adapterTables
    }

    private var mSnackbar: Snackbar? = null
    private fun enableSwipeToDeleteAndUndo(gamePath: GamePath) {
        val swipeToDeleteCallback: SwipeToDeleteCallback = object : SwipeToDeleteCallback(this.context) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, i: Int) {
                mSnackbar = Snackbar
                        .make(view!!, R.string.snackbar_table_deleted, 2000)
                val position = viewHolder.adapterPosition
                val idGameToDelete = adapterTables.delete(position)
                viewModel.deleteTable(idGameToDelete)
                mSnackbar!!.setActionTextColor(Color.YELLOW)
                mSnackbar!!.show()
                view?.tablesRecyclerView?.scrollToPosition(position)
            }
        }
        val itemTouchhelper = ItemTouchHelper(swipeToDeleteCallback)
        itemTouchhelper.attachToRecyclerView(view?.tablesRecyclerView)
    }

    private fun onInsertGame(gamePath: GamePath) {
        fragmentManager?.let { it ->
            val insertDialogFragment = InsertDialogFragment.getFragmentNewGame(gamePath)
            insertDialogFragment.show(it.beginTransaction(), InsertDialogFragment.TAG)
        }

    }

    override fun onActivityCreated(bundleInstance: Bundle?) {
        super.onActivityCreated(bundleInstance)
        view?.startAnimation(AnimationUtils.loadAnimation(context, R.anim.bottom_to_top))
    }

    override fun onStart() {
        super.onStart()
        loadBanner()
    }


    override fun onDetach() {
        super.onDetach()
        mSnackbar?.dismiss()
    }

    companion object {
        const val TAG = "TablesFragment"
        fun newInstance(idButton: Int?): TablesFragment {
            val fragment = TablesFragment()
            val args = Bundle()
            val gamePath: GamePath = when (idButton) {
                R.id.ib_2_players -> GamePath.GAME_PATH_2_PLAYERS
                R.id.ib_3_players -> GamePath.GAME_PATH_3_PLAYERS
                R.id.ib_4_players -> GamePath.GAME_PATH_4_PLAYERS
                R.id.ib_2_groups -> GamePath.GAME_PATH_2_GROUPS
                else -> GamePath.GAME_PATH_2_GROUPS
            }
            args.putSerializable(GAME_PATH, gamePath)
            fragment.arguments = args
            return fragment
        }
    }
}