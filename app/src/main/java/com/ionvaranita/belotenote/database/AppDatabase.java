package com.ionvaranita.belotenote.database;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import android.content.Context;

import com.ionvaranita.belotenote.database.dao.BoltDao;
import com.ionvaranita.belotenote.database.dao.BoltManagerDao;
import com.ionvaranita.belotenote.database.dao.groups2.Game2GroupsDao;
import com.ionvaranita.belotenote.database.dao.WinnerPointsDao;
import com.ionvaranita.belotenote.database.dao.groups2.Points2GroupsDao;
import com.ionvaranita.belotenote.database.dao.groups2.Scor2GroupsDao;
import com.ionvaranita.belotenote.database.dao.ExtendedGameDao;
import com.ionvaranita.belotenote.database.dao.players2.Game2PDao;
import com.ionvaranita.belotenote.database.dao.players2.Points2PDao;
import com.ionvaranita.belotenote.database.dao.players2.Scor2PDao;
import com.ionvaranita.belotenote.database.dao.players3.Game3PDao;
import com.ionvaranita.belotenote.database.dao.players3.Points3PDao;
import com.ionvaranita.belotenote.database.dao.players3.Scor3PDao;
import com.ionvaranita.belotenote.database.dao.players4.Game4PDao;
import com.ionvaranita.belotenote.database.dao.players4.Points4PDao;
import com.ionvaranita.belotenote.database.dao.players4.Scor4PDao;
import com.ionvaranita.belotenote.database.entity.WinnerPointsEntity;
import com.ionvaranita.belotenote.database.entity.groups2.Game2GroupsEntity;
import com.ionvaranita.belotenote.database.entity.groups2.Points2GroupsEntity;
import com.ionvaranita.belotenote.database.entity.groups2.Scor2GroupsEntity;
import com.ionvaranita.belotenote.database.entity.BoltEntity;
import com.ionvaranita.belotenote.database.entity.BoltManagerEntity;
import com.ionvaranita.belotenote.database.entity.ExtendedGameEntity;
import com.ionvaranita.belotenote.database.entity.players2.Game2PEntity;
import com.ionvaranita.belotenote.database.entity.players2.Points2PEntity;
import com.ionvaranita.belotenote.database.entity.players2.Scor2PEntity;
import com.ionvaranita.belotenote.database.entity.players3.Game3PEntity;
import com.ionvaranita.belotenote.database.entity.players3.Points3PEntity;
import com.ionvaranita.belotenote.database.entity.players3.Scor3PEntity;
import com.ionvaranita.belotenote.database.entity.players4.Game4PEntity;
import com.ionvaranita.belotenote.database.entity.players4.Points4PEntity;
import com.ionvaranita.belotenote.database.entity.players4.Scor4PEntity;

/**
 * Created by ionvaranita on 20/11/17.
 */
@Database(entities = {Game2PEntity.class, Points2PEntity.class, Scor2PEntity.class, Game3PEntity.class, Points3PEntity.class, Scor3PEntity.class
        , Game4PEntity.class, Points4PEntity.class, Scor4PEntity.class, Game2GroupsEntity.class, Points2GroupsEntity.class, Scor2GroupsEntity.class, ExtendedGameEntity.class, BoltEntity.class, BoltManagerEntity.class, WinnerPointsEntity.class}, version = 92, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private static final String DATABASE_NAME = "BeloteNoteDatabase";
    private static AppDatabase PERSISTENT_DATABASE;

    //PLAYERS 2
    public abstract Game2PDao game2PDao();

    public abstract Points2PDao points2PDao();

    public abstract Scor2PDao scor2PDao();

    //PLAYERS 3
    public abstract Game3PDao game3PDao();

    public abstract Points3PDao points3PDao();

    public abstract Scor3PDao scor3PDao();

    //GROUPS 2
    public abstract Points2GroupsDao points2GroupsDao();

    public abstract Scor2GroupsDao scor2GroupsDao();

    public abstract Game2GroupsDao game2GroupsDao();

    //PLAYERS 4
    public abstract Game4PDao game4PDao();

    public abstract Points4PDao points4PDao();

    public abstract Scor4PDao scor4PDao();

    //GLOBAL
    public abstract WinnerPointsDao winnerPointsDao();

    public abstract BoltDao boltDao();

    public abstract BoltManagerDao boltManagerDao();

    public abstract ExtendedGameDao extendedGameDao();


    public static AppDatabase getPersistentDatabase(Context context) {
        if (PERSISTENT_DATABASE == null) {
            try {
                PERSISTENT_DATABASE = Room.databaseBuilder(context, AppDatabase.class, DATABASE_NAME).fallbackToDestructiveMigration().allowMainThreadQueries().build();

                WinnerPointsEntity winnerPointsEntity1 = new WinnerPointsEntity();
                winnerPointsEntity1.setPuntiVincenti((short) 101);
                PERSISTENT_DATABASE.winnerPointsDao().insert(winnerPointsEntity1);

                WinnerPointsEntity winnerPointsEntity = new WinnerPointsEntity();
                winnerPointsEntity.setPuntiVincenti((short) 51);
                PERSISTENT_DATABASE.winnerPointsDao().insert(winnerPointsEntity);
            } catch (Exception e) {
                Room.databaseBuilder(context, AppDatabase.class, DATABASE_NAME).fallbackToDestructiveMigration();
            }
        }
        return PERSISTENT_DATABASE;
    }
}