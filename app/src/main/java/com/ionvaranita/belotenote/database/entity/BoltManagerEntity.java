package com.ionvaranita.belotenote.database.entity;

import androidx.room.Entity;
import androidx.room.Index;
import androidx.annotation.NonNull;

/**
 * Created by ionvaranita on 2019-09-02;
 */
@Entity(indices = {@Index(value = {"idPerson","idGame"}, unique = true)}, primaryKeys = {"idPerson","idGame"})

public class BoltManagerEntity {
    @NonNull
    private final String path;

    public BoltManagerEntity(@NonNull String path) {
        this.path = path;
    }

    @NonNull
    private Integer idPerson;
    @NonNull
    private
    Short idGame;
    @NonNull
    private Boolean isMinus10;

    @NonNull
    public String getPath() {
        return path;
    }

    public Integer getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Integer idPerson) {
        this.idPerson = idPerson;
    }

    public Short getIdGame() {
        return idGame;
    }

    public void setIdGame(Short idGame) {
        this.idGame = idGame;
    }

    @NonNull
    public Boolean getMinus10() {
        return isMinus10;
    }

    public void setMinus10(@NonNull Boolean meno10) {
        isMinus10 = meno10;
    }
}
