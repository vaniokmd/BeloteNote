package com.ionvaranita.belotenote.database.entity.players4;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import com.ionvaranita.belotenote.shared.constants.GameStatus;
import com.ionvaranita.belotenote.shared.utils.ColorUtility;

/**
 * Created by ionvaranita on 2019-09-11;
 */
@Entity
public class Game4PEntity {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private Short idGame;
    @NonNull
    private long dateGame;
    @NonNull
    private int color;
    @NonNull
    private Byte statusGame;
    @NonNull
    private Short winnerPoints;
    private String name1;
    @NonNull
    private String name2;
    @NonNull
    private String name3;
    @NonNull
    private String name4;

    public Game4PEntity() {
        dateGame = new java.util.Date().getTime();
        ColorUtility colorUtility = new ColorUtility();
        color = colorUtility.generateRandomColor();
        statusGame = GameStatus.CONTINUE;
    }

    @NonNull
    public Short getIdGame() {
        return idGame;
    }

    public void setIdGame(@NonNull Short idGame) {
        this.idGame = idGame;
    }

    @NonNull
    public long getDateGame() {
        return dateGame;
    }

    public void setDateGame(@NonNull long dateGame) {
        this.dateGame = dateGame;
    }

    @NonNull
    public Byte getStatusGame() {
        return statusGame;
    }

    public void setStatusGame(@NonNull Byte statusGame) {
        this.statusGame = statusGame;
    }

    @NonNull
    public Short getWinnerPoints() {
        return winnerPoints;
    }

    public void setWinnerPoints(@NonNull Short winnerPoints) {
        this.winnerPoints = winnerPoints;
    }

    @NonNull
    public int getColor() {
        return color;
    }

    public void setColor(@NonNull int color) {
        this.color = color;
    }


    @NonNull
    public String getName1() {
        return name1;
    }

    public void setName1(@NonNull String name1) {
        this.name1 = name1;
    }

    @NonNull
    public String getName2() {
        return name2;
    }

    public void setName2(@NonNull String name2) {
        this.name2 = name2;
    }

    @NonNull
    public String getName3() {
        return name3;
    }

    public void setName3(@NonNull String name3) {
        this.name3 = name3;
    }

    @NonNull
    public String getName4() {
        return name4;
    }

    public void setName4(@NonNull String name4) {
        this.name4 = name4;
    }
}
