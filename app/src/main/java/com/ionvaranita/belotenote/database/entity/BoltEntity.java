package com.ionvaranita.belotenote.database.entity;

import androidx.room.Entity;
import androidx.room.Index;
import androidx.annotation.NonNull;

/**
 * Created by ionvaranita on 2019-09-02;
 */
@Entity(indices = {@Index(value = {"path", "idGame", "idRow"}, unique = true)}, primaryKeys = {"path", "idGame", "idRow"})
public class BoltEntity {
    @NonNull
    private final
    String path;
    @NonNull
    private Integer idRow;
    @NonNull
    private Short idGame;
    @NonNull
    private Integer idPersonBolt;
    @NonNull
    private Byte nrBolt;

    public BoltEntity(@NonNull String path) {
        this.path = path;
    }

    public Short getIdGame() {
        return idGame;
    }

    @NonNull
    public String getPath() {
        return path;
    }

    public void setIdGame(Short idGame) {
        this.idGame = idGame;
    }

    public Integer getIdPersonBolt() {
        return idPersonBolt;
    }

    public void setIdPersonBolt(Integer idPersonBolt) {
        this.idPersonBolt = idPersonBolt;
    }

    public Byte getNrBolt() {
        return nrBolt;
    }

    public void setNrBolt(Byte nrBolt) {
        this.nrBolt = nrBolt;
    }

    @NonNull
    public Integer getIdRow() {
        return idRow;
    }

    public void setIdRow(@NonNull Integer idRow) {
        this.idRow = idRow;
    }
}
