package com.ionvaranita.belotenote.database.dao.groups2

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.ionvaranita.belotenote.database.entity.groups2.Scor2GroupsEntity

/**
 * Created by ionvaranita on 09/12/2017.
 */
@Dao
interface Scor2GroupsDao {
    @Insert
    fun insert(scor2GroupsEntity: Scor2GroupsEntity)

    @Query("select *  from Scor2GroupsEntity where idGame = :idGame")
    fun getScore(idGame: Short?): Scor2GroupsEntity

    @Update
    fun update(scor2GroupsEntity: Scor2GroupsEntity): Int

    @Query("delete from Scor2GroupsEntity where idGame = :idGame")
    fun delete(idGame: Short)
}