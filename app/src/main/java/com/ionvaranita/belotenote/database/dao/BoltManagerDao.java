package com.ionvaranita.belotenote.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.ionvaranita.belotenote.database.entity.BoltManagerEntity;

/**
 * Created by ionvaranita on 2019-09-02;
 */
@Dao
public interface BoltManagerDao {
    @Insert
    void insert(BoltManagerEntity boltManagerEntity);

    @Update
    void update(BoltManagerEntity boltManagerEntity);

    @Query("delete from BoltManagerEntity where path = :path and idGame = :idGame")
    void delete(String path,Short idGame);

    @Query("select * from BoltManagerEntity where path = :path and idGame = :idGame and idPerson = :idPerson")
    BoltManagerEntity getIsMeno10(String path,Short idGame, Integer idPerson);
}
