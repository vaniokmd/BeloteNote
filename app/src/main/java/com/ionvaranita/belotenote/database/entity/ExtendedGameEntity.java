package com.ionvaranita.belotenote.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

/**
 * Created by ionvaranita on 2019-08-30;
 */
@Entity
public class ExtendedGameEntity {

    public ExtendedGameEntity(String path) {
        this.path = path;
    }

    private final String path;
    @PrimaryKey
    private Short idGame;
    @NonNull
    private Integer idWinner;
    @NonNull
    private Short maxPoints;

    public Short getIdGame() {
        return idGame;
    }

    public void setIdGame(Short idGame) {
        this.idGame = idGame;
    }

    @NonNull
    public Integer getIdWinner() {
        return idWinner;
    }

    public void setIdWinner(@NonNull Integer idWinner) {
        this.idWinner = idWinner;
    }

    @NonNull
    public Short getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(@NonNull Short maxPoints) {
        this.maxPoints = maxPoints;
    }

    public String getPath() {
        return path;
    }

}
