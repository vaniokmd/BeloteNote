package com.ionvaranita.belotenote.database.dao.players4

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.ionvaranita.belotenote.database.entity.players4.Game4PEntity

/**
 * Created by ionvaranita on 2019-09-11;
 */
@Dao
interface Game4PDao {
    @Insert
    fun insert(game4PEntity: Game4PEntity): Long

    @get:Query("select * from Game4PEntity order by dateGame desc")
    val games: List<Game4PEntity>

    @Query("select * from Game4PEntity where idGame = :idGame")
    fun getGame(idGame: Short): Game4PEntity

    @Query("update Game4PEntity set statusGame = :statusGame, dateGame = :dateGame where idGame = :idGame")
    fun updateStatus(idGame: Short, statusGame: Byte, dateGame: Long): Int

    @Query("update Game4PEntity set statusGame = :statusGame, winnerPoints = :winnerPoints, dateGame = :dateGame where idGame = :idGame")
    fun update(idGame: Short, statusGame: Byte, winnerPoints: Short, dateGame: Long): Int

    @Query("delete from Game4PEntity where idGame = :idGame")
    fun delete(idGame: Short)
}