package com.ionvaranita.belotenote.database.entity.players2;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class Points2PEntity {
    @PrimaryKey(autoGenerate = true)
    private Integer id = null;
    @NonNull
    private Short idGame;
    @NonNull
    private Short pointsMe;
    @NonNull
    private Short pointsYouS;
    @NonNull
    private Short pointsGame;

    public Points2PEntity() {
        this.pointsMe = 0;
        this.pointsYouS = 0;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Short getIdGame() {
        return idGame;
    }

    public void setIdGame(Short idGame) {
        this.idGame = idGame;
    }

    public Short getPointsMe() {
        return pointsMe;
    }

    public void setPointsMe(Short pointsMe) {
        this.pointsMe = pointsMe;
    }

    public Short getPointsYouS() {
        return pointsYouS;
    }

    public void setPointsYouS(Short pointsYouS) {
        this.pointsYouS = pointsYouS;
    }

    public Short getPointsGame() {
        return pointsGame;
    }

    public void setPointsGame(Short pointsGame) {
        this.pointsGame = pointsGame;
    }
}
