package com.ionvaranita.belotenote.database.dao.players4

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.ionvaranita.belotenote.database.entity.players4.Scor4PEntity

/**
 * Created by ionvaranita on 2019-09-11;
 */
@Dao
interface Scor4PDao {
    @Insert
    fun insert(scor4PEntity: Scor4PEntity)

    @Query("select *  from Scor4PEntity where idGame = :idGame")
    fun getScore(idGame: Short): Scor4PEntity

    @Update
    fun update(scor3PEntity: Scor4PEntity): Int

    @Query("delete from Scor4PEntity where idGame = :idGame")
    fun delete(idGame: Short)
}