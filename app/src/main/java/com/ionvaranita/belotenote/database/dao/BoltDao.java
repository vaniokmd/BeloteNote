package com.ionvaranita.belotenote.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.ionvaranita.belotenote.database.entity.BoltEntity;

import java.util.List;

/**
 * Created by ionvaranita on 2019-09-02;
 */
@Dao
public interface BoltDao {
    @Insert
    void insert(BoltEntity boltEntity);

    @Query("select * from BoltEntity b1 where path = :path and idGame = :idGame and b1.idRow = (select max(idRow) from BoltEntity b2 where b2.idPersonBolt = :idPerson)")
    BoltEntity getBolt(String path, Short idGame, Integer idPerson);

    @Query("delete from BoltEntity where path = :path and idGame = :idGame")
    void delete(String path, Short idGame);

    @Query("select * from BoltEntity where path = :path and idGame = :idGioco")
    List<BoltEntity> getBolts(String path, Short idGioco);
}
