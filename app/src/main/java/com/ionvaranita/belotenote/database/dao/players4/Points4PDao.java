package com.ionvaranita.belotenote.database.dao.players4;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.ionvaranita.belotenote.database.entity.players4.Points4PEntity;

import java.util.List;

/**
 * Created by ionvaranita on 2019-09-11;
 */
@Dao
public interface Points4PDao {
    @Insert
    Long insert(Points4PEntity points4PEntity);

    @Query("select * from Points4PEntity where idGame = :idGame")
    List<Points4PEntity> getPoints(Short idGame);

    @Query("select * from Points4PEntity where id = (select max(id) from Points4PEntity where idGame = :idGame)")
    Points4PEntity getLastPoints(Short idGame);

    @Query("delete from Points4PEntity where idGame = :idGame")
    void deletePoints(Short idGame);
}
