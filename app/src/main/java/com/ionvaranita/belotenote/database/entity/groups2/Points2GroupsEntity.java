package com.ionvaranita.belotenote.database.entity.groups2;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

/**
 * Created by ionvaranita on 20/11/17.
 */
@Entity
public class Points2GroupsEntity {
    @PrimaryKey(autoGenerate = true)
    private Integer id = null;
    @NonNull
    private Short idGame;
    @NonNull
    private Short pointsWe;
    @NonNull
    private Short pointsYouP;
    @NonNull
    private Short pointsGame;

    public Points2GroupsEntity() {
        this.pointsWe = 0;
        this.pointsYouP = 0;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Short getIdGame() {
        return idGame;
    }

    public void setIdGame(Short idGame) {
        this.idGame = idGame;
    }

    public Short getPointsWe() {
        return pointsWe;
    }

    public void setPointsWe(Short pointsWe) {
        this.pointsWe = pointsWe;
    }

    public Short getPointsYouP() {
        return pointsYouP;
    }

    public void setPointsYouP(Short pointsYouP) {
        this.pointsYouP = pointsYouP;
    }

    public Short getPointsGame() {
        return pointsGame;
    }

    public void setPointsGame(Short pointsGame) {
        this.pointsGame = pointsGame;
    }

}
