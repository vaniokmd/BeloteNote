package com.ionvaranita.belotenote.database.entity.players2;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

/**
 * Created by ionvaranita on 09/12/2017.
 */
@Entity
public class Scor2PEntity {
    public Scor2PEntity() {
        scorMe = 0;
        scorYouS = 0;
    }
    @PrimaryKey
    private Short idGame;
    @NonNull
    private Short scorMe;
    @NonNull
    private Short scorYouS;
    @NonNull
    public Short getIdGame() {
        return idGame;
    }
    public void setIdGame(Short idGame) {
        this.idGame = idGame;
    }
    @NonNull
    public Short getScorMe() {
        return scorMe;
    }
    public void setScorMe(Short scorMe) {
        this.scorMe = scorMe;
    }
    @NonNull
    public Short getScorYouS() {
        return scorYouS;
    }
    public void setScorYouS(Short scorYouS) {
        this.scorYouS = scorYouS;
    }


}
