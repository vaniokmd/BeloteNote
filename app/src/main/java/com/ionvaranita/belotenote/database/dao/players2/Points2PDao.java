package com.ionvaranita.belotenote.database.dao.players2;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.ionvaranita.belotenote.database.entity.players2.Points2PEntity;

import java.util.List;

/**
 * Created by ionvaranita on 2019-08-26;
 */
@Dao
public interface Points2PDao {
    @Insert
    Long insert(Points2PEntity points2PEntity);

    @Query("select * from Points2PEntity where idGame = :idGame")
    List<Points2PEntity> getPoints(Short idGame);

    @Query("select * from Points2PEntity where id = (select max(id) from Points2PEntity where idGame = :idGame)")
    Points2PEntity getLastPoints(Short idGame);

    @Query("delete from Points2PEntity where idGame = :idGame")
    void delete(Short idGame);
}
