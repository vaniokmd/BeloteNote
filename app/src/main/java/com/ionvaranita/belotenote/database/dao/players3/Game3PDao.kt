package com.ionvaranita.belotenote.database.dao.players3

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.ionvaranita.belotenote.database.entity.players3.Game3PEntity

@Dao
interface Game3PDao {
    @Insert
    fun insert(game3PEntity: Game3PEntity): Long

    @get:Query("select * from Game3PEntity order by dateGame desc")
    val games: List<Game3PEntity>

    @Query("select * from Game3PEntity where idGame = :idGame")
    fun getGame(idGame: Short): Game3PEntity

    @Query("update Game3PEntity set statusGame = :statusGame, dateGame = :dateGame where idGame = :idGame")
    fun updateStatus(idGame: Short, statusGame: Byte, dateGame: Long): Int

    @Query("update Game3PEntity set statusGame = :statusGame, winnerPoints = :winnerPoints, dateGame = :dateGame where idGame = :idGame")
    fun update(idGame: Short, statusGame: Byte, winnerPoints: Short, dateGame: Long): Int

    @Query("delete from Game3PEntity where idGame = :idGame")
    fun delete(idGame: Short)
}