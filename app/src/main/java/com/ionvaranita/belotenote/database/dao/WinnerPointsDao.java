package com.ionvaranita.belotenote.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ionvaranita.belotenote.database.entity.WinnerPointsEntity;

import java.util.List;

@Dao
public interface WinnerPointsDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(WinnerPointsEntity winnerPointsEntity);
    @Query("select puntiVincenti from WinnerPointsEntity order by puntiVincenti desc")
    List<Short> getWinnerPoints();
}
