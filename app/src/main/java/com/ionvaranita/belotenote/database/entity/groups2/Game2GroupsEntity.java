package com.ionvaranita.belotenote.database.entity.groups2;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import com.ionvaranita.belotenote.shared.constants.GameStatus;
import com.ionvaranita.belotenote.shared.utils.ColorUtility;


/**
 * Created by ionvaranita on 09/12/2017.
 */
@Entity
public class Game2GroupsEntity {
    @PrimaryKey(autoGenerate = true)
    private Short idGame;
    @NonNull
    private Long dateGame;
    @NonNull
    private Integer color;
    @NonNull
    private Byte statusGame;
    @NonNull
    private Short winnerPoints;
    @NonNull
    private String name1;
    @NonNull
    private String name2;

    public Game2GroupsEntity() {
        dateGame = new java.util.Date().getTime();
        ColorUtility colorUtility = new ColorUtility();
        color = colorUtility.generateRandomColor();
        statusGame = GameStatus.CONTINUE;
    }

    @NonNull
    public Short getIdGame() {
        return idGame;
    }

    public void setIdGame(@NonNull Short idGame) {
        this.idGame = idGame;
    }

    @NonNull
    public Long getDateGame() {
        return dateGame;
    }

    public void setDateGame(@NonNull Long dateGame) {

        this.dateGame = dateGame;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(@NonNull Integer color) {
        this.color = color;
    }

    public Byte getStatusGame() {
        return statusGame;
    }

    public void setStatusGame(Byte statusGame) {
        this.statusGame = statusGame;
    }

    @NonNull
    public Short getWinnerPoints() {
        return winnerPoints;
    }

    public void setWinnerPoints(@NonNull Short winnerPoints) {
        this.winnerPoints = winnerPoints;
    }

    @NonNull
    public String getName1() {
        return name1;
    }

    public void setName1(@NonNull String name1) {
        this.name1 = name1;
    }

    @NonNull
    public String getName2() {
        return name2;
    }

    public void setName2(@NonNull String name2) {
        this.name2 = name2;
    }
}
