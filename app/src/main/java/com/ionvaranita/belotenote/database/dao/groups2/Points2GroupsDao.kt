package com.ionvaranita.belotenote.database.dao.groups2

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.ionvaranita.belotenote.database.entity.groups2.Points2GroupsEntity

/**
 * Created by ionvaranita on 20/11/17.
 */
@Dao
interface Points2GroupsDao {
    @Insert
    fun insert(points2GroupsEntity: Points2GroupsEntity): Long

    @Query("select * from Points2GroupsEntity where idGame = :idGame")
    fun getPoints(idGame: Short): List<Points2GroupsEntity>

    @Query("select * from Points2GroupsEntity where id = (select max(id) from Points2GroupsEntity where idGame = :idGame)")
    fun getLastPoints(idGame: Short): Points2GroupsEntity?

    @Query("delete from Points2GroupsEntity where idGame = :idGame")
    fun delete(idGame: Short): Int
}