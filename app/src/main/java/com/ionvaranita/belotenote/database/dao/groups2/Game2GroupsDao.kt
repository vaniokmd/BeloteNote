package com.ionvaranita.belotenote.database.dao.groups2

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.ionvaranita.belotenote.database.entity.groups2.Game2GroupsEntity

/**
 * Created by ionvaranita on 11/12/2017.
 */
@Dao
interface Game2GroupsDao {
    @get:Query("select * from Game2GroupsEntity order by dateGame desc")
    val games: List<Game2GroupsEntity>

    @Query("select * from Game2GroupsEntity where idGame = :idGame")
    fun getGame(idGame: Short): Game2GroupsEntity

    @Insert
    fun insert(game2GroupsEntity: Game2GroupsEntity): Long

    @Query("update Game2GroupsEntity set statusGame = :statusGame, winnerPoints = :winnerPoints, dateGame = :dateGame where idGame = :idGame")
    fun update(idGame: Short, statusGame: Byte, winnerPoints: Short, dateGame: Long) : Int

    @Query("update Game2GroupsEntity set statusGame = :statusGame, dateGame = :dateGame where idGame = :idGame")
    fun updateStatus(idGame: Short, statusGame: Byte, dateGame: Long): Int

    @Query("delete from Game2GroupsEntity  where idGame = :idGame")
    fun delete(idGame: Short)
}