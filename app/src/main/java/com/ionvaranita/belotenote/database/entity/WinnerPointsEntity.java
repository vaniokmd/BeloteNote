package com.ionvaranita.belotenote.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class WinnerPointsEntity {
    @PrimaryKey
    private Short puntiVincenti;

    public Short getPuntiVincenti() {
        return puntiVincenti;
    }

    public void setPuntiVincenti(Short puntiVincenti) {
        this.puntiVincenti = puntiVincenti;
    }
}
