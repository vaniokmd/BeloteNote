package com.ionvaranita.belotenote.database.dao.players2

import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.ionvaranita.belotenote.database.entity.players2.Game2PEntity

@Dao
interface Game2PDao {
    @Insert
    fun insert(game2PEntity: Game2PEntity): Long

    @get:Query("select * from Game2PEntity order by dateGame desc")
    val games: List<Game2PEntity>

    @Query("select * from Game2PEntity where idGame = :idGame")
    fun getGame(idGame: Short): Game2PEntity

    @Query("update Game2PEntity set statusGame = :statusGame, dateGame = :dateGame where idGame = :idGame")
    fun updateStatus(idGame: Short, statusGame: Byte, dateGame: Long): Int

    @Query("update Game2PEntity set statusGame = :statusGame, winnerPoints = :winnerPoints, dateGame = :dateGame where idGame = :idGame")
    fun update(idGame: Short, statusGame: Byte, winnerPoints: Short, dateGame: Long): Int

    @Query("delete from Game2PEntity where idGame = :idGame")
    fun delete(idGame: Short)
}