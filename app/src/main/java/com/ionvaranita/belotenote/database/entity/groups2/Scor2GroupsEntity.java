package com.ionvaranita.belotenote.database.entity.groups2;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

/**
 * Created by ionvaranita on 09/12/2017.
 */
@Entity
public class Scor2GroupsEntity {
    @PrimaryKey
    private Short idGame;
    @NonNull
    private Short scorWe;
    @NonNull
    private Short scorYouP;

    public Scor2GroupsEntity() {
        scorWe = 0;
        scorYouP = 0;
    }

    @NonNull
    public Short getIdGame() {
        return idGame;
    }
    public void setIdGame(Short idGame) {
        this.idGame = idGame;
    }
    @NonNull
    public Short getScorWe() {
        return scorWe;
    }
    public void setScorWe(Short scorWe) {
        this.scorWe = scorWe;
    }
    @NonNull
    public Short getScorYouP() {
        return scorYouP;
    }
    public void setScorYouP(Short scorYouP) {
        this.scorYouP = scorYouP;
    }
}
