package com.ionvaranita.belotenote.database.dao.players3;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.ionvaranita.belotenote.database.entity.players3.Points3PEntity;

import java.util.List;

/**
 * Created by ionvaranita on 2019-09-05;
 */
@Dao
public interface Points3PDao {
    @Insert
    Long insert(Points3PEntity Points3PEntity);

    @Query("select * from Points3PEntity where idGame = :idGame")
    List<Points3PEntity> getPoints(Short idGame);

    @Query("select * from Points3PEntity where id = (select max(id) from Points3PEntity where idGame = :idGame)")
    Points3PEntity getLastPoints(Short idGame);

    @Query("delete from Points3PEntity where idGame = :idGame")
    void deletePoints(Short idGame);

}
