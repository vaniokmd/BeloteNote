package com.ionvaranita.belotenote.database.entity.players3;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class Points3PEntity {
    @PrimaryKey(autoGenerate = true)
    private Integer id;
    @NonNull
    private Short idGame;
    @NonNull
    private Short pointsGame;
    @NonNull
    private Short pointsP1;
    @NonNull
    private Short pointsP2;
    @NonNull
    private Short pointsP3;

    public Points3PEntity() {
        this.pointsP1 = 0;
        this.pointsP2 = 0;
        this.pointsP3 = 0;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Short getIdGame() {
        return idGame;
    }

    public void setIdGame(Short idGame) {
        this.idGame = idGame;
    }

    @NonNull
    public Short getPointsGame() {
        return pointsGame;
    }

    public void setPointsGame(@NonNull Short pointsGame) {
        this.pointsGame = pointsGame;
    }

    public Short getPointsP1() {
        return pointsP1;
    }

    public void setPointsP1(Short pointsP1) {
        this.pointsP1 = pointsP1;
    }

    public Short getPointsP2() {
        return pointsP2;
    }

    public void setPointsP2(Short pointsP2) {
        this.pointsP2 = pointsP2;
    }

    public Short getPointsP3() {
        return pointsP3;
    }

    public void setPointsP3(Short pointsP3) {
        this.pointsP3 = pointsP3;
    }
}
