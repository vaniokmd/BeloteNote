package com.ionvaranita.belotenote.database.entity.players4;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

/**
 * Created by ionvaranita on 2019-09-11;
 */
@Entity
public class Scor4PEntity {
    public Scor4PEntity() {
        scorP1 = 0;
        scorP2 = 0;
        scorP3 = 0;
        scorP4 = 0;
    }
    @PrimaryKey
    private Short idGame;
    @NonNull
    private Short scorP1;
    @NonNull
    private Short scorP2;
    @NonNull
    private Short scorP3;
    @NonNull
    private Short scorP4;
    @NonNull
    public Short getIdGame() {
        return idGame;
    }
    public void setIdGame(Short idGame) {
        this.idGame = idGame;
    }
    @NonNull
    public Short getScorP1() {
        return scorP1;
    }
    public void setScorP1(Short scorP1) {
        this.scorP1 = scorP1;
    }
    @NonNull
    public Short getScorP2() {
        return scorP2;
    }
    public void setScorP2(Short scorP2) {
        this.scorP2 = scorP2;
    }

    @NonNull
    public Short getScorP3() {
        return scorP3;
    }

    public void setScorP3(@NonNull Short scorP3) {
        this.scorP3 = scorP3;
    }

    @NonNull
    public Short getScorP4() {
        return scorP4;
    }

    public void setScorP4(@NonNull Short scorP4) {
        this.scorP4 = scorP4;
    }
}
