package com.ionvaranita.belotenote

import android.app.Application
import com.ionvaranita.belotenote.shared.SharePreferencesManager
import com.yariksoffice.lingver.Lingver
import java.util.*


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        languageSelected = Locale.getDefault().language
        Lingver.init(this, languageSelected)
    }

    companion object {
        lateinit var languageSelected: String
    }

}