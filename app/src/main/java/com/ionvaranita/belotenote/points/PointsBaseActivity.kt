package com.ionvaranita.belotenote.points

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.view.ContextMenu.ContextMenuInfo
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.TranslateAnimation
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.ads.*
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.TedPermission
import com.ionvaranita.belotenote.R
import com.ionvaranita.belotenote.main.MainActivity
import com.ionvaranita.belotenote.main.tables.TablesFragment
import com.ionvaranita.belotenote.points.adapter.AdapterPoints
import com.ionvaranita.belotenote.points.viewmodel.PointsViewModel
import com.ionvaranita.belotenote.shared.constants.Constants
import com.ionvaranita.belotenote.shared.constants.GamePath
import com.ionvaranita.belotenote.shared.constants.GameStatus
import com.ionvaranita.belotenote.shared.constants.IdsInputFieldUtils
import com.ionvaranita.belotenote.shared.customview.EditTextStyled
import com.ionvaranita.belotenote.shared.customview.TextViewStyled
import com.ionvaranita.belotenote.shared.dialog.AboutDialogFragment
import com.ionvaranita.belotenote.shared.dialog.InfoGameDialogFramgent
import com.ionvaranita.belotenote.shared.dialog.InsertDialogFragment
import com.ionvaranita.belotenote.shared.dialog.WinnerDialogFragment
import com.ionvaranita.belotenote.shared.generators.InputFieldGenerator
import com.ionvaranita.belotenote.shared.generators.PrintFieldGenerator
import com.ionvaranita.belotenote.shared.generators.ScorFieldGenerator
import com.ionvaranita.belotenote.shared.keyboard.MyKeyboardFragment
import com.ionvaranita.belotenote.shared.result.GameResult
import com.ionvaranita.belotenote.shared.utils.AdMobUtils.getAdSize
import com.ionvaranita.belotenote.shared.utils.AppUtils
import com.ionvaranita.belotenote.shared.utils.DeviceUtils
import com.ionvaranita.belotenote.shared.utils.ResourcesUtils.getImageResByStatus
import com.ionvaranita.belotenote.shared.utils.StatusGameUtils
import com.ionvaranita.belotenote.shared.viewmodel.base.PointsBaseViewModel
import com.ionvaranita.belotenote.shared.viewmodel.dialog.InsertDialogViewModel
import com.ionvaranita.belotenote.shared.viewmodel.factory.PointsViewModelFactory
import com.ionvaranita.belotenote.shared.viewmodel.points.PointsBaseViewModelFactory
import hotchemi.android.rate.AppRate
import kotlinx.android.synthetic.main.activity_points.*
import kotlinx.android.synthetic.main.content_activity_points.*
import java.io.File
import java.io.FileOutputStream
import kotlin.properties.Delegates


/**
 * Created by ionvaranita on 07/09/17.
 */
open class PointsBaseActivity : AppCompatActivity(), MyKeyboardFragment.MyKeyboardListener,
    MyKeyboardFragment.PointsActivityInteraction, WinnerDialogFragment.PointsActivityInteraction,
    MyKeyboardFragment.MyKeyboardEnterClickListener {
    private val mapFields: HashMap<Int, EditTextStyled> = HashMap()
    private var idPersonBolt: Int? = null
    protected open var idGame by Delegates.notNull<Short>()
    private val scorFieldGenerator by lazy { ScorFieldGenerator(this) }
    private var statusGame by Delegates.notNull<Byte>()
    protected lateinit var gamePath: GamePath
    private var winningPoints by Delegates.notNull<Short>()
    private var keyboardFragment: MyKeyboardFragment? = null
    private lateinit var viewModel: PointsBaseViewModel
    private val pointsAdaper by lazy { AdapterPoints(gamePath) }
    private lateinit var viewModelPoints: PointsViewModel
    private lateinit var viewModelInsertDialog: InsertDialogViewModel
    private var scorFile: File? = null
    private lateinit var adView: AdView
    private var mInterstitialAd: InterstitialAd? = null

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.setContentView(R.layout.activity_points)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setCustomView(R.layout.action_bar_custom)
        this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        if (!DeviceUtils.isTablet(this)) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT
        }
        idGame = this.intent.getShortExtra(Constants.ID_GAME, 1)
        viewModel =
            ViewModelProvider(this, PointsBaseViewModelFactory(application, gamePath, idGame)).get(
                PointsBaseViewModel::class.java
            )
        viewModel.observableGameResult.observe(this, Observer { gameResult ->
            statusGame = gameResult.statusGame
            winningPoints = gameResult.winnerPoints
            initBaseUI(gameResult)
            initStatus()
            popolatePrintFields(gameResult.nameMap)
        })
        keyboardFragment =
            supportFragmentManager.findFragmentById(R.id.keyboard_myKeyboard) as MyKeyboardFragment?
        val bacgroundCampo = ContextCompat.getDrawable(this, R.drawable.bckg_shaped_white_bordered)
        bacgroundCampo!!.setColorFilter(Color.WHITE, PorterDuff.Mode.DARKEN)
        pointsRecyclerView.background = bacgroundCampo
        val mLinearLayoutManager = LinearLayoutManager(this)
        pointsRecyclerView.layoutManager = mLinearLayoutManager

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        viewModelPoints =
            ViewModelProvider(this, PointsViewModelFactory(application, idGame, gamePath)).get(
                PointsViewModel::class.java
            )
        viewModelInsertDialog =
            ViewModelProvider(this, PointsBaseViewModelFactory(application, gamePath, idGame)).get(
                InsertDialogViewModel::class.java
            )

        viewModelPoints.getPoints()
        viewModelPoints.getScore()

        viewModelPoints.observablePoints.observe(this, Observer {
            populateRecyclerView(it)
        })

        viewModelInsertDialog.observableInsertMatch.observe(this, Observer {
            updateToExtendMatch(it)
            clearRv()
        })

        viewModelInsertDialog.observableExtendMatch.observe(this, Observer {
            updateToExtendMatch(it)
        })
        viewModelInsertDialog.observableFinishMatch.observe(this, Observer {
            updateStatus(GameStatus.FINISHED)
            updateScor(it)
        })
        viewModelPoints.observableScor.observe(this, Observer {
            scorStatusTableRow.post {
                if (scorStatusTableRow.childCount == 1) {
                    initScorUI(it)
                    populateScore(it)
                }
            }
        })
        viewModelPoints.observablePointsInsert.observe(this, Observer {
            onPointsInserted(it)
        })
        viewModelPoints.observableUpdateStatus.observe(this, Observer {
            updateStatus(it)
        })
        viewModelPoints.observableUpdateScor.observe(this, Observer {
            updateScor(it)
        })

        (supportFragmentManager.findFragmentById(R.id.keyboard_myKeyboard) as MyKeyboardFragment?)?.onEnterClick =
            { onEnterClick() }

        AppRate.with(this).monitor()
        initBanner()
    }

    private fun initBanner() {
        adView = AdView(this)
        adView.adUnitId = getString(R.string.banner_ad_unit_id)
        adView.adSize = getAdSize(this)
        adview_container.post {
            adview_container.addView(adView)
        }
    }

    private fun loadAndShowInterstitialAd() {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(
            this,
            getString(R.string.interstitial_ad_unit_id),
            adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    Log.d(TAG, adError.message)
                    mInterstitialAd = null
                }

                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    Log.d(Companion.TAG, "Ad was loaded.")
                    mInterstitialAd = interstitialAd
                    mInterstitialAd?.fullScreenContentCallback =
                        object : FullScreenContentCallback() {
                            override fun onAdDismissedFullScreenContent() {
                                Log.d(TAG, "Ad was dismissed.")
                            }

                            override fun onAdFailedToShowFullScreenContent(adError: AdError?) {
                                Log.d(TAG, "Ad failed to show.")
                            }

                            override fun onAdShowedFullScreenContent() {
                                Log.d(TAG, "Ad showed fullscreen content.")
                                mInterstitialAd = null
                            }
                        }
                    if (mInterstitialAd != null) {
                        mInterstitialAd?.show(this@PointsBaseActivity)
                    } else {
                        Log.d(TAG, "The interstitial ad wasn't ready yet.")
                    }
                }
            })
    }

    private fun loadBanner() {
        Log.d(TAG,"loadBanner called")
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    private fun popolatePrintFields(personsName: Map<Int, String>) {
        printFieldsTableRow.post {
            if (printFieldsTableRow.childCount == 0) {
                val fieldsPrint = PrintFieldGenerator(gamePath)
                fieldsPrint.popolatePrintPointFields(printFieldsTableRow, personsName)
            }
        }
    }

    private fun initBaseUI(gameResult: GameResult) {
        tv_nameGame.text = getString(R.string.table, idGame.toString())
        wrap_gameBoard.background.current.setColorFilter(gameResult.color, PorterDuff.Mode.DARKEN)
    }

    override fun onResume() {
        super.onResume()
        init()
        wrap_content_points_table.startAnimation(
            AnimationUtils.loadAnimation(
                applicationContext,
                R.anim.bottom_to_top
            )
        )
    }

    private fun init() {
        if (inputCellsTableRow.childCount == 0) {
            popolatePointInputFields()
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun popolatePointInputFields() {
        val inputFieldGenerator = InputFieldGenerator(gamePath)
        inputCellsTableRow?.let {
            keyboardFragment?.let { it1 ->
                inputFieldGenerator.popolatePointsFields(
                    it,
                    it1
                )
            }
        }
        inputCellsTableRow.getChildAt(0).requestFocus()
        for (i in 0 until inputCellsTableRow.childCount) {
            val field = inputCellsTableRow.getChildAt(i) as EditTextStyled
            mapFields[field.id] = field
        }
    }

    private fun isPointsOk(): Boolean {
        if (inputCellsTableRow.animation != null && !inputCellsTableRow.animation.hasEnded()) {
            return false
        }
        val ids = mapFields.keys.iterator()
        var succeed = true
        while (ids.hasNext()) {
            val idField = ids.next()
            val inputField = mapFields[idField]
            val stringValue = inputField!!.text.toString()
            if (stringValue == Constants.STRING_BOLT) {
                idPersonBolt = idField
            } else if (stringValue.isEmpty() && inputField.hint.toString().isEmpty()) {
                inputField.showError()
                succeed = false
            }

        }
        return succeed
    }

    private val upInputFieldsAnim: Animation
        get() {
            val positionX = printFieldsTableRow.x
            val positionY = printFieldsTableRow.y
            val animation = TranslateAnimation(
                0f,
                positionX - inputCellsTableRow.x,
                0f,
                positionY - inputCellsTableRow.y
            )
            animation.repeatMode = 0
            animation.duration = 500
            animation.start()
            return animation
        }

    private val downInputFieldsAnim: Animation
        get() {
            val positionX = printFieldsTableRow.x
            val positionY = printFieldsTableRow.y
            val animation = TranslateAnimation(
                positionX - inputCellsTableRow.x,
                0f,
                positionY - inputCellsTableRow.y,
                0f
            )
            animation.repeatMode = 0
            animation.duration = 500
            animation.start()
            return animation
        }

    private fun clearInputFields(textMap: Map<Int, EditTextStyled>) {
        val ids = textMap.keys.iterator()
        textMap[IdsInputFieldUtils.ID_GAME_POINTS]?.requestFocus()
        while (ids.hasNext()) {
            val idField = ids.next()
            val inputField = textMap[idField]
            inputField?.text?.clear()
            inputField?.hint = ""
        }
        idPersonBolt = null
    }

    private fun updateScor(idWinner: Int) {
        val scorWinner: TextViewStyled? = scorStatusTableRow.findViewById(idWinner)
        val updateScor = scorWinner?.text.toString().toInt().plus(1)
        scorWinner?.text = updateScor.toString()
        WinnerDialogFragment.getInstance(
            viewModel.observableGameResult.value?.nameMap?.get(idWinner)
                ?: ""
        ).show(supportFragmentManager, WinnerDialogFragment.WINNER_DIALOG_TAG)
    }

    private fun initStatus() {
        iv_status.visibility = View.VISIBLE
        val animation = statusAnimation
        iv_status.setImageResource(getImageResByStatus(statusGame))
        when {
            StatusGameUtils.isFinished(statusGame) -> {
                iv_status.clearAnimation()
                setVisibilityFab(true)
                initBannerAdVisibility(true)
            }
            StatusGameUtils.isContinue(statusGame) -> {
                iv_status.startAnimation(animation)
                setVisibilityFab(false)
                initBannerAdVisibility(false)
            }
            StatusGameUtils.isExtendedOrExtendedMandatory(statusGame) -> {
                iv_status.clearAnimation()
                setVisibilityFab(true)
                initBannerAdVisibility(true)
            }
        }

        iv_status.setOnClickListener {
            val infoGameDialogFramgent = InfoGameDialogFramgent.getInstance(
                gamePath,
                tv_nameGame!!.text.toString(),
                winningPoints,
                statusGame
            )
            infoGameDialogFramgent.show(
                supportFragmentManager.beginTransaction(),
                Constants.DIALOG_FRAGMENT_INFO_GAME
            )
        }
    }

    private fun initBannerAdVisibility(isVisible: Boolean) {
        if (!DeviceUtils.isTablet(this)) {
            setBannerVisibility(isVisible)
        }
    }

    private fun setBannerVisibility(isVisible: Boolean) {
        if (isVisible) {
            adview_container.visibility = View.VISIBLE
        } else {
            adview_container.visibility = View.GONE
        }
    }

    private fun updateStatus(status: Byte) {
        statusGame = status
        initStatus()
    }

    private val statusAnimation: Animation
        get() {
            val anim: Animation = AlphaAnimation(0.0f, 1.0f)
            anim.duration = 200 //You can manage the blinking time with this parameter
            anim.startOffset = 200
            anim.repeatMode = Animation.REVERSE
            anim.repeatCount = Animation.INFINITE
            return anim
        }

    fun onFabClick(view: View?) {
        if (StatusGameUtils.isFinished(statusGame)) {
            val insertDialogFragment = InsertDialogFragment.getDialogNewMatch(gamePath, idGame)
            if (!insertDialogFragment.isVisible) {
                insertDialogFragment.show(
                    supportFragmentManager.beginTransaction(),
                    InsertDialogFragment.TAG
                )
            }
        } else if (StatusGameUtils.isExtended(statusGame)) {
            val extendedGame = viewModel.getExtendedGame()
            val winner = viewModel.observableGameResult.value?.nameMap?.get(extendedGame.idWinner)
            val insertDialogFragment =
                winner?.let { InsertDialogFragment.getDialogExtend(gamePath, idGame, it) }
            insertDialogFragment?.show(
                supportFragmentManager.beginTransaction(),
                InsertDialogFragment.TAG
            )
        } else if (StatusGameUtils.isExtendedMandatory(statusGame)) {
            val insertDialogFragment =
                InsertDialogFragment.getFragmentExtendMandatory(gamePath, idGame)
            insertDialogFragment.show(
                supportFragmentManager.beginTransaction(),
                InsertDialogFragment.TAG
            )
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.app_menu, menu)
        if (!DeviceUtils.isTablet(this)) {
            menu.findItem(R.id.main_menu).isVisible = true
        }
        return true
    }

    override fun onCreateContextMenu(
        menu: ContextMenu, v: View,
        menuInfo: ContextMenuInfo
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val inflater = menuInflater
        inflater.inflate(R.menu.app_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.main_menu -> {
                val vaiMainMenu = Intent(this, MainActivity::class.java)
                startActivity(vaiMainMenu)
                true
            }
            R.id.about -> {
                val fragmentManager = supportFragmentManager
                AboutDialogFragment.newInstance()
                    .show(fragmentManager, Constants.DIALOG_FRAGMENT_ABOUT)
                true
            }
            R.id.share -> {
                if (StatusGameUtils.isFinished(statusGame)) {
                    checkSharePermission()
                } else {
                    AppUtils.shareApp(this)
                }
                true
            }
            else -> {
                onBackPressed()
                true
            }
        }
    }

    private fun shareScore() {
        val bitmap = getScreenShot(window.decorView.rootView)
        scorFile = store(bitmap)
        scorFile?.let {
            shareImage(it)
        }
    }

    private fun setVisibilityFab(isShow: Boolean) {
        if (isShow) {
            keyboardFragment?.requireView()?.visibility = View.GONE
            fab_insert_match.visibility = View.VISIBLE
            inputCellsTableRow.visibility = View.GONE
            inputCellsTableRow.isEnabled = false
        } else {
            keyboardFragment?.requireView()?.visibility = View.VISIBLE
            fab_insert_match.visibility = View.GONE
            inputCellsTableRow.visibility = View.VISIBLE
            inputCellsTableRow.isEnabled = true
            mapFields[IdsInputFieldUtils.ID_GAME_POINTS]?.requestFocus()
        }
    }

    override fun clearFiled(idField: Int?) {
        idField?.let {
            mapFields[it]!!.text!!.clear()
        }
    }

    override fun setInputType(idField: Int, inputType: Int) {
        mapFields[idField]?.inputType = inputType
    }

    override fun onOkWinnerDialogClick() {
        AppRate.showRateDialogIfMeetsConditions(this)
        loadBanner()
        loadAndShowInterstitialAd()
    }

    override fun onShareScoreClick() {
        checkSharePermission()
    }

    private fun checkSharePermission() {
        TedPermission.with(this)
            .setPermissionListener(permissionlistener)
            .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
            .setPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .check()
    }

    override fun calcolateRemainingField(idField: Int?) {
        val gamePointsInput = mapFields[IdsInputFieldUtils.ID_GAME_POINTS]
        val gamePoints = gamePointsInput!!.text.toString()
        var sumFields: Short = 0
        var idFieldRemaning: Int? = null
        if (gamePoints.isEmpty()) {
            clearHints()
        } else if (gamePoints.isNotEmpty()) {
            val iteratorFields: Iterator<EditTextStyled> = mapFields.values.iterator()
            var nrEmptyFields: Byte = 0
            while (iteratorFields.hasNext()) {
                val field = iteratorFields.next()
                if (field.id != IdsInputFieldUtils.ID_GAME_POINTS) {
                    if (field.text.toString().isEmpty()) {
                        ++nrEmptyFields
                        idFieldRemaning = if (nrEmptyFields.toInt() == 1) {
                            field.id
                        } else {
                            clearHints()
                            return
                        }
                    } else {
                        var value = getShortInputForDB(field)
                        if (value < 0) {
                            value = 0
                        }
                        sumFields = (sumFields + value).toShort()
                    }
                }
            }
            if (idFieldRemaning != null && nrEmptyFields.toInt() == 1) {
                val gamePointsShort = gamePoints.toShort()
                val difference = (gamePointsShort - sumFields).toShort()
                var stringDifference = ""
                if (difference >= 0) {
                    stringDifference += "" + difference
                }
                mapFields[idFieldRemaning]!!.hint = "" + stringDifference
            }
        }
    }

    private fun clearHints() {
        val iterator: Iterator<EditTextStyled> = mapFields.values.iterator()
        while (iterator.hasNext()) {
            iterator.next().hint = ""
        }
    }

    private fun getShortInputForDB(editText: EditText?): Short {
        val points = if (editText?.text.toString()
                .isEmpty()
        ) editText?.hint.toString() else editText?.text.toString()
        return try {
            points.toShort()
        } catch (e: Exception) {
            0
        }
    }

    private fun startInputAnimation() {
        inputCellsTableRow.animation = upInputFieldsAnim
        Handler(this.mainLooper).postDelayed({
            if (StatusGameUtils.isContinue(statusGame)) {
                inputCellsTableRow.animation = downInputFieldsAnim
            }
            keyboardFragment!!.clearBoltMeno10()
            Handler().postDelayed({ clearInputFields(mapFields) }, 500)
        }, 500)
    }

    private fun updateToExtendMatch(winningPoints: Short) {
        updateStatus(GameStatus.CONTINUE)
        this.winningPoints = winningPoints
    }

    private fun populateScore(scorMap: HashMap<Int, String>) {
        scorStatusTableRow.post {
            scorMap.forEach { elem ->
                val id = elem.key
                val scor = elem.value
                val cell = scorStatusTableRow.findViewById<TextViewStyled>(id)
                cell.text = scor
            }
        }
    }

    private fun initScorUI(map: LinkedHashMap<Int, String>) {
        map.forEach {
            val cell = scorFieldGenerator.getTextViewById(it.key)
            scorStatusTableRow.addView(cell)
        }
    }

    private fun populateRecyclerView(pointsMap: ArrayList<HashMap<Int, String>>) {
        pointsAdaper.items = pointsMap
        pointsRecyclerView.adapter = pointsAdaper
        pointsAdaper.updateAdapter(pointsRecyclerView)
    }

    private fun clearRv() {
        pointsAdaper.clearItems()
    }

    private fun onPointsInserted(pointsMap: HashMap<Int, String>) {
        pointsAdaper.items.add(pointsMap)
        pointsAdaper.updateAdapter(pointsRecyclerView)
        startInputAnimation()
    }

    override fun onEnterClick() {
        if (isPointsOk()) {
            viewModelPoints.insertPoints(mapFields, idPersonBolt, winningPoints)
        }
    }

    private fun getScreenShot(view: View): Bitmap {
        setBannerVisibility(false)
        val returnedBitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(returnedBitmap)
        val bgDrawable = view.background
        if (bgDrawable != null) bgDrawable.draw(canvas)
        else canvas.drawColor(Color.WHITE)
        view.draw(canvas)
        setBannerVisibility(true)
        return returnedBitmap
    }

    var permissionlistener: PermissionListener = object : PermissionListener {
        override fun onPermissionGranted() {
            shareScore()
        }

        override fun onPermissionDenied(deniedPermissions: MutableList<String>?) {
            Toast.makeText(this@PointsBaseActivity, "Permission NOT Granted", Toast.LENGTH_SHORT)
                .show()
        }

    }

    private fun shareImage(file: File) {
        val uri = FileProvider.getUriForFile(
            this,
            "com.ionvaranita.belotenote.provider",
            file
        )
        val intent = Intent()
        intent.action = Intent.ACTION_SEND
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_subject))
        val extraText = "${getString(R.string.share_body)} ${AppUtils.getWebtUrl(this)}"
        intent.putExtra(Intent.EXTRA_TEXT, extraText)
        intent.putExtra(Intent.EXTRA_STREAM, uri)
        try {
            startActivity(Intent.createChooser(intent, getString(R.string.share_score)))
        } catch (e: java.lang.Exception) {
            Toast.makeText(this, "No App Available", Toast.LENGTH_SHORT).show()
        }
    }

    private fun store(bm: Bitmap): File? {
        return try {
            val dir = File(filesDir, "mydir")
            if (!dir.exists()) {
                dir.mkdir()
            }
            val file = File(dir, "ShareScoreBelote.png")

            if (file.exists()) {
                file.delete()
            }
            file.createNewFile()

            val fOut = FileOutputStream(file)
            bm.compress(Bitmap.CompressFormat.PNG, 85, fOut)
            fOut.flush()
            fOut.close()
            bm.recycle()
            file
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            null
        }
    }


    override fun onDestroy() {
        scorFile?.delete()
        super.onDestroy()
    }

    companion object {
        private const val TAG = "PointsBaseActivity"
    }

}