package com.ionvaranita.belotenote.points.activities

import android.os.Bundle
import com.ionvaranita.belotenote.shared.keyboard.MyKeyboardFragment
import com.ionvaranita.belotenote.points.PointsBaseActivity
import com.ionvaranita.belotenote.shared.constants.GamePath


class Points2PActivity : PointsBaseActivity(), MyKeyboardFragment.MyKeyboardEnterClickListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.gamePath = GamePath.GAME_PATH_2_PLAYERS
        super.onCreate(savedInstanceState)
    }

 }