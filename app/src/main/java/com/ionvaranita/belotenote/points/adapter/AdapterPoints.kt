package com.ionvaranita.belotenote.points.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ionvaranita.belotenote.R
import com.ionvaranita.belotenote.shared.constants.GamePath
import com.ionvaranita.belotenote.shared.constants.IdsInputFieldUtils
import kotlinx.android.synthetic.main.item_points_2_groups.view.*
import kotlinx.android.synthetic.main.item_points_2_players.view.*
import kotlinx.android.synthetic.main.item_points_3_players.view.*
import kotlinx.android.synthetic.main.item_points_4_players.view.*

class AdapterPoints(private val gamePath: GamePath) : RecyclerView.Adapter<AdapterPoints.PointsViewHolder>() {
    var items: MutableList<HashMap<Int, String>> = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PointsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (gamePath) {
            GamePath.GAME_PATH_2_PLAYERS -> {
                Points2PViewHolder(inflater.inflate(R.layout.item_points_2_players, parent, false))
            }
            GamePath.GAME_PATH_3_PLAYERS -> {
                Points3PViewHolder(inflater.inflate(R.layout.item_points_3_players, parent, false))

            }
            GamePath.GAME_PATH_4_PLAYERS -> {
                Points4PViewHolder(inflater.inflate(R.layout.item_points_4_players, parent, false))
            }
            GamePath.GAME_PATH_2_GROUPS -> {
                Points2GroupsViewHolder(inflater.inflate(R.layout.item_points_2_groups, parent, false))
            }
        }
    }

    override fun onBindViewHolder(holder: PointsViewHolder, position: Int) {
        val pointsMap = items[position]
        holder.onBind(pointsMap, position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    abstract class PointsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        abstract fun onBind(pointsMap: HashMap<Int, String>, position: Int)
    }

    internal class Points2PViewHolder(itemView: View) : PointsViewHolder(itemView) {

        override fun onBind(pointsMap: HashMap<Int, String>, position: Int) {
            itemView.cell_game_2.text = pointsMap[IdsInputFieldUtils.ID_GAME_POINTS]
            itemView.cell_2_1.text = pointsMap[IdsInputFieldUtils.ID_PERSON_2_1]
            itemView.cell_2_2.text = pointsMap[IdsInputFieldUtils.ID_PERSON_2_2]
            if ((position + 1) % 2 == 0) {
                itemView.wrap_points_2.setBackgroundResource(R.drawable.border_bottom)
            } else {
                itemView.wrap_points_2.setBackgroundColor(Color.WHITE)
            }
        }

    }

    internal class Points3PViewHolder(itemView: View) : PointsViewHolder(itemView) {

        override fun onBind(pointsMap: HashMap<Int, String>, position: Int) {
            itemView.cell_game_3.text = pointsMap[IdsInputFieldUtils.ID_GAME_POINTS]
            itemView.cell_3_1.text = pointsMap[IdsInputFieldUtils.ID_PERSON_3_1]
            itemView.cell_3_2.text = pointsMap[IdsInputFieldUtils.ID_PERSON_3_2]
            itemView.cell_3_3.text = pointsMap[IdsInputFieldUtils.ID_PERSON_3_3]
            if ((position + 1) % 3 == 0) {
                itemView.wrap_points_3.setBackgroundResource(R.drawable.border_bottom)
            } else {
                itemView.wrap_points_3.setBackgroundColor(Color.WHITE)
            }
        }

    }

    internal class Points4PViewHolder(itemView: View) : PointsViewHolder(itemView) {

        override fun onBind(pointsMap: HashMap<Int, String>, position: Int) {
            itemView.cell_game_4.text = pointsMap[IdsInputFieldUtils.ID_GAME_POINTS]
            itemView.cell_4_1.text = pointsMap[IdsInputFieldUtils.ID_PERSON_4_1]
            itemView.cell_4_2.text = pointsMap[IdsInputFieldUtils.ID_PERSON_4_2]
            itemView.cell_4_3.text = pointsMap[IdsInputFieldUtils.ID_PERSON_4_3]
            itemView.cell_4_4.text = pointsMap[IdsInputFieldUtils.ID_PERSON_4_4]

            if ((position + 1) % 4 == 0) {
                itemView.wrap_points_4.setBackgroundResource(R.drawable.border_bottom)
            } else {
                itemView.wrap_points_4.setBackgroundColor(Color.WHITE)
            }
        }
    }

    internal class Points2GroupsViewHolder(itemView: View) : PointsViewHolder(itemView) {

        override fun onBind(pointsMap: HashMap<Int, String>, position: Int) {
            itemView.item_game_2_groups.text = pointsMap[IdsInputFieldUtils.ID_GAME_POINTS]
            itemView.item_we_2_groups.text = pointsMap[IdsInputFieldUtils.ID_TEAM_1]
            itemView.item_you_p_2_groups.text = pointsMap[IdsInputFieldUtils.ID_TEAM_2]
            if ((position + 1) % 2 == 0) {
                itemView.wrap_item_2_groups.setBackgroundResource(R.drawable.border_bottom)
            } else {
                itemView.wrap_item_2_groups.setBackgroundColor(Color.WHITE)
            }
        }

    }

    fun updateAdapter(recyclerView: RecyclerView) {
        notifyDataSetChanged()
        recyclerView.scrollToPosition(itemCount - 1)
    }

    fun clearItems() {
        items.clear()
        notifyDataSetChanged()
    }
}