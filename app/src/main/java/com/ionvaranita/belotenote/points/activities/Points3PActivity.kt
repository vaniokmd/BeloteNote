package com.ionvaranita.belotenote.points.activities

import android.os.Bundle
import com.ionvaranita.belotenote.points.PointsBaseActivity
import com.ionvaranita.belotenote.shared.constants.GamePath
import com.ionvaranita.belotenote.shared.dialog.WinnerDialogFragment
import com.ionvaranita.belotenote.shared.keyboard.MyKeyboardFragment

class Points3PActivity : PointsBaseActivity(), MyKeyboardFragment.MyKeyboardListener, MyKeyboardFragment.MyKeyboardEnterClickListener, WinnerDialogFragment.PointsActivityInteraction, MyKeyboardFragment.PointsActivityInteraction {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.gamePath = GamePath.GAME_PATH_3_PLAYERS
        super.onCreate(savedInstanceState)
    }
}