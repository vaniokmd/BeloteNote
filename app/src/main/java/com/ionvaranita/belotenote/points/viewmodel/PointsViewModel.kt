package com.ionvaranita.belotenote.points.viewmodel

import android.app.Application
import android.widget.EditText
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ionvaranita.belotenote.database.entity.BoltEntity
import com.ionvaranita.belotenote.database.entity.BoltManagerEntity
import com.ionvaranita.belotenote.database.entity.ExtendedGameEntity
import com.ionvaranita.belotenote.database.entity.groups2.Points2GroupsEntity
import com.ionvaranita.belotenote.database.entity.players2.Points2PEntity
import com.ionvaranita.belotenote.database.entity.players3.Points3PEntity
import com.ionvaranita.belotenote.database.entity.players4.Points4PEntity
import com.ionvaranita.belotenote.main.datamodel.PointsModelConverter
import com.ionvaranita.belotenote.shared.constants.Constants
import com.ionvaranita.belotenote.shared.constants.GamePath
import com.ionvaranita.belotenote.shared.constants.GameStatus
import com.ionvaranita.belotenote.shared.constants.IdsInputFieldUtils
import com.ionvaranita.belotenote.shared.utils.StatusGameUtils
import com.ionvaranita.belotenote.shared.viewmodel.base.BaseViewModelPoints
import java.util.*

class PointsViewModel(application: Application, private val idGame: Short, private val gamePath: GamePath) : BaseViewModelPoints(application) {
    private val converter by lazy { PointsModelConverter(application) }

    private val _observablePoints = MutableLiveData<ArrayList<HashMap<Int, String>>>()
    val observablePoints: LiveData<ArrayList<HashMap<Int, String>>> = _observablePoints

    private val _observableScor = MutableLiveData<LinkedHashMap<Int, String>>()
    val observableScor: LiveData<LinkedHashMap<Int, String>> = _observableScor

    private val _observableUpdateScor = MutableLiveData<Int>()
    val observableUpdateScor: LiveData<Int> = _observableUpdateScor

    private val _observableUpdateStatus = MutableLiveData<Byte>()
    val observableUpdateStatus: LiveData<Byte> = _observableUpdateStatus

    private val _observablePointsInsert = MutableLiveData<HashMap<Int, String>>()
    val observablePointsInsert: LiveData<HashMap<Int, String>> = _observablePointsInsert

    fun getPoints() {
        val result = when (gamePath) {
            GamePath.GAME_PATH_2_PLAYERS -> {
                val listPoints = db.points2PDao().getPoints(idGame)
                val listBolts = db.boltDao().getBolts(gamePath.description, idGame)
                converter.convertPoints2P(listPoints, listBolts)
            }
            GamePath.GAME_PATH_3_PLAYERS -> {
                val listPoints = db.points3PDao().getPoints(idGame)
                val listBolts = db.boltDao().getBolts(gamePath.description, idGame)
                converter.convertPoints3P(listPoints, listBolts)
            }
            GamePath.GAME_PATH_4_PLAYERS -> {
                val listPoints = db.points4PDao().getPoints(idGame)
                val listBolts = db.boltDao().getBolts(gamePath.description, idGame)
                converter.convertPoints4P(listPoints, listBolts)
            }
            GamePath.GAME_PATH_2_GROUPS -> {
                val listPoints = db.points2GroupsDao().getPoints(idGame)
                val listBolts = db.boltDao().getBolts(gamePath.description, idGame)
                converter.convertPoints2Groups(listPoints, listBolts)
            }
        }
        _observablePoints.postValue(result)
    }

    fun getScore() {
        val scorConverted = when (gamePath) {
            GamePath.GAME_PATH_2_PLAYERS -> {
                val scor = db.scor2PDao().getScore(idGame)
                converter.convertScor2P(scor)
            }
            GamePath.GAME_PATH_3_PLAYERS -> {
                val scor = db.scor3PDao().getScore(idGame)
                converter.convertScor3P(scor)

            }
            GamePath.GAME_PATH_4_PLAYERS -> {
                val scor = db.scor4PDao().getScore(idGame)
                converter.convertScor4P(scor)

            }
            GamePath.GAME_PATH_2_GROUPS -> {
                val scor = db.scor2GroupsDao().getScore(idGame)
                converter.convertScor2Groups(scor)
            }
        }
        _observableScor.postValue(scorConverted)
    }

    fun insertPoints(pointsMap: Map<Int, EditText?>, idPersonBolt: Int?, winningPoints: Short) {
        val mapPoints = when (gamePath) {
            GamePath.GAME_PATH_2_PLAYERS -> {
                insertPoints2P(pointsMap, idPersonBolt, winningPoints)
            }
            GamePath.GAME_PATH_3_PLAYERS -> {
                insertPoints3P(pointsMap, idPersonBolt, winningPoints)
            }
            GamePath.GAME_PATH_4_PLAYERS -> {
                insertPoints4P(pointsMap, idPersonBolt, winningPoints)
            }
            GamePath.GAME_PATH_2_GROUPS -> {
                insertPoints2Groups(pointsMap, idPersonBolt, winningPoints)
            }
        }
        _observablePointsInsert.postValue(mapPoints)
    }

    private fun convertPointsMapShort(pointsMap: Map<Int, EditText?>): HashMap<Int, Short> {
        return pointsMap.mapValues { key -> getShortInputForDB(key.value) } as HashMap
    }

    private fun convertPointsMapString(pointsMapShort: HashMap<Int, Short>, boltEntity: BoltEntity?, isMeno10: Boolean, idPersonBolt: Int?): HashMap<Int, String> {
        val pointsMapString = pointsMapShort.mapValues { map ->
            map.value.toString()
        } as HashMap
        boltEntity?.let {
            if (!isMeno10) {
                pointsMapString[idPersonBolt!!] = Constants.STRING_BOLT + boltEntity.nrBolt
            }
        }
        return pointsMapString
    }

    private fun insertPoints2P(pointsMap: Map<Int, EditText?>, idPersonBolt: Int?, winningPoints: Short): HashMap<Int, String> {
        val pointsMapShort = convertPointsMapShort(pointsMap)
        var pointsGame = pointsMapShort[IdsInputFieldUtils.ID_GAME_POINTS] ?: 0
        var pointsMe = pointsMapShort[IdsInputFieldUtils.ID_PERSON_2_1] ?: 0
        var pointsYouS = pointsMapShort[IdsInputFieldUtils.ID_PERSON_2_2] ?: 0
        var isMeno10 = false
        if (idPersonBolt != null) {
            val boltManagerEntity = db.boltManagerDao().getIsMeno10(GamePath.GAME_PATH_2_PLAYERS.description, idGame, idPersonBolt)
            if (boltManagerEntity != null) {
                isMeno10 = boltManagerEntity.minus10
            }
            if (boltManagerEntity == null || isMeno10) {
                if (boltManagerEntity == null) {
                    val boltManager2GroupsEntity1 = BoltManagerEntity(GamePath.GAME_PATH_2_PLAYERS.description)
                    boltManager2GroupsEntity1.idGame = idGame
                    boltManager2GroupsEntity1.idPerson = idPersonBolt
                    boltManager2GroupsEntity1.minus10 = false
                    db.boltManagerDao().insert(boltManager2GroupsEntity1)
                }
                if (isMeno10) {
                    boltManagerEntity!!.minus10 = false
                    db.boltManagerDao().update(boltManagerEntity)
                }
            }
            if (isMeno10) {
                if (idPersonBolt == IdsInputFieldUtils.ID_PERSON_2_1) {
                    pointsMe = (-10).toShort()
                } else if (idPersonBolt == IdsInputFieldUtils.ID_PERSON_2_2) {
                    pointsYouS = (-10).toShort()
                }
            }
        }
        var lastRecord2PEntity = db.points2PDao().getLastPoints(idGame)
        if (lastRecord2PEntity == null) {
            lastRecord2PEntity = Points2PEntity()
            lastRecord2PEntity.idGame = idGame
        }
        pointsMe = pointsMe.plus(lastRecord2PEntity.pointsMe).toShort()
        pointsYouS = pointsYouS.plus(lastRecord2PEntity.pointsYouS).toShort()
        val points2PEntity = Points2PEntity()
        points2PEntity.idGame = idGame
        points2PEntity.pointsGame = pointsGame
        points2PEntity.pointsMe = pointsMe
        points2PEntity.pointsYouS = pointsYouS
        val idRowInserted = db.points2PDao()
                .insert(points2PEntity).toInt()
        points2PEntity.id = idRowInserted
        var boltEntity = db.boltDao().getBolt(GamePath.GAME_PATH_2_PLAYERS.description, idGame, idPersonBolt)
        var nrBolt: Byte? = null
        if (idPersonBolt != null && !isMeno10) {
            if (boltEntity != null) {
                nrBolt = boltEntity.nrBolt
            }
            boltEntity = BoltEntity(GamePath.GAME_PATH_2_PLAYERS.description)
            boltEntity.idRow = idRowInserted
            boltEntity.idGame = idGame
            boltEntity.idPersonBolt = idPersonBolt
            boltEntity.nrBolt = nrBolt
            insertBolt(boltEntity)
        }
        pointsMapShort[IdsInputFieldUtils.ID_PERSON_2_1] = pointsMe
        pointsMapShort[IdsInputFieldUtils.ID_PERSON_2_2] = pointsYouS
        val infoWinner = getWinner(pointsMapShort, winningPoints)
        val statusGame = infoWinner.statusGame
        if (!StatusGameUtils.isContinue(statusGame)) {
            if (StatusGameUtils.isFinished(statusGame)) {
                updateScor(infoWinner.winners[0])
            } else if (StatusGameUtils.isExtended(statusGame) || StatusGameUtils.isExtendedMandatory(statusGame)) {
                val extendedGameEntity = ExtendedGameEntity(GamePath.GAME_PATH_2_PLAYERS.description)
                extendedGameEntity.idGame = idGame
                extendedGameEntity.idWinner = infoWinner.maxPersona
                extendedGameEntity.maxPoints = infoWinner.maxPoints
                db.extendedGameDao().merge(extendedGameEntity)
            }
            updateStatus(statusGame)
        }
        return convertPointsMapString(pointsMapShort, boltEntity, isMeno10, idPersonBolt)
    }

    private fun insertPoints3P(pointsMap: Map<Int, EditText?>, idPersonBolt: Int?, winningPoints: Short): HashMap<Int, String> {
        val pointsMapShort = convertPointsMapShort(pointsMap)
        var pointsGame = pointsMapShort[IdsInputFieldUtils.ID_GAME_POINTS] ?: 0
        var pointsP1 = pointsMapShort[IdsInputFieldUtils.ID_PERSON_3_1] ?: 0
        var pointsP2 = pointsMapShort[IdsInputFieldUtils.ID_PERSON_3_2] ?: 0
        var pointsP3 = pointsMapShort[IdsInputFieldUtils.ID_PERSON_3_3] ?: 0
        var isMeno10 = false
        if (idPersonBolt != null) {
            val boltManagerEntity = db.boltManagerDao().getIsMeno10(GamePath.GAME_PATH_3_PLAYERS.description, idGame, idPersonBolt)
            if (boltManagerEntity != null) {
                isMeno10 = boltManagerEntity.minus10
            }
            if (boltManagerEntity == null || isMeno10) {
                if (boltManagerEntity == null) {
                    val boltManager2GroupsEntity1 = BoltManagerEntity(GamePath.GAME_PATH_3_PLAYERS.description)
                    boltManager2GroupsEntity1.idGame = idGame
                    boltManager2GroupsEntity1.idPerson = idPersonBolt
                    boltManager2GroupsEntity1.minus10 = false
                    db.boltManagerDao().insert(boltManager2GroupsEntity1)
                }
                if (isMeno10) {
                    boltManagerEntity!!.minus10 = false
                    db.boltManagerDao().update(boltManagerEntity)
                }
            }
            if (isMeno10) {
                if (idPersonBolt == IdsInputFieldUtils.ID_PERSON_3_1) {
                    pointsP1 = (-10).toShort()
                } else if (idPersonBolt == IdsInputFieldUtils.ID_PERSON_3_2) {
                    pointsP2 = (-10).toShort()
                } else if (idPersonBolt == IdsInputFieldUtils.ID_PERSON_3_3) {
                    pointsP3 = (-10).toShort()
                }
            }
        }
        var lastPoints3PEntity = db.points3PDao().getLastPoints(idGame)
        if (lastPoints3PEntity == null) {
            lastPoints3PEntity = Points3PEntity()
            lastPoints3PEntity.idGame = idGame
        }

        pointsP1 = pointsP1.plus(lastPoints3PEntity.pointsP1).toShort()
        pointsP2 = pointsP2.plus(lastPoints3PEntity.pointsP2).toShort()
        pointsP3 = pointsP3.plus(lastPoints3PEntity.pointsP3).toShort()
        val points3PEntity = Points3PEntity()
        points3PEntity.idGame = idGame
        points3PEntity.pointsGame = pointsGame
        points3PEntity.pointsP1 = pointsP1
        points3PEntity.pointsP2 = pointsP2
        points3PEntity.pointsP3 = pointsP3
        val idRowInserted = db.points3PDao()
                .insert(points3PEntity).toInt()
        points3PEntity.id = idRowInserted
        var boltEntity = db.boltDao().getBolt(GamePath.GAME_PATH_3_PLAYERS.description, idGame, idPersonBolt)
        var nrBolt: Byte? = null
        if (idPersonBolt != null && !isMeno10) {
            if (boltEntity != null) {
                nrBolt = boltEntity.nrBolt
            }
            boltEntity = BoltEntity(GamePath.GAME_PATH_3_PLAYERS.description)
            boltEntity.idRow = idRowInserted
            boltEntity.idGame = idGame
            boltEntity.idPersonBolt = idPersonBolt
            boltEntity.nrBolt = nrBolt
            insertBolt(boltEntity)
        }
        pointsMapShort[IdsInputFieldUtils.ID_PERSON_3_1] = points3PEntity.pointsP1
        pointsMapShort[IdsInputFieldUtils.ID_PERSON_3_2] = points3PEntity.pointsP2
        pointsMapShort[IdsInputFieldUtils.ID_PERSON_3_3] = points3PEntity.pointsP3
        val infoWinner = getWinner(pointsMapShort, winningPoints)
        val statusGame = infoWinner.statusGame
        if (!StatusGameUtils.isContinue(statusGame)) {
            if (StatusGameUtils.isFinished(statusGame)) {
                updateScor(infoWinner.winners[0])
            } else if (StatusGameUtils.isExtended(statusGame) || StatusGameUtils.isExtendedMandatory(statusGame)) {
                val extendedGameEntity = ExtendedGameEntity(GamePath.GAME_PATH_3_PLAYERS.description)
                extendedGameEntity.idGame = idGame
                extendedGameEntity.idWinner = infoWinner.maxPersona
                extendedGameEntity.maxPoints = infoWinner.maxPoints
                db.extendedGameDao().merge(extendedGameEntity)
            }
            updateStatus(statusGame)
        }
        return convertPointsMapString(pointsMapShort, boltEntity, isMeno10, idPersonBolt)
    }

    private fun insertPoints4P(pointsMap: Map<Int, EditText?>, idPersonBolt: Int?, winningPoints: Short): HashMap<Int, String> {
        val pointsMapShort = convertPointsMapShort(pointsMap)
        val pointsGame = pointsMapShort[IdsInputFieldUtils.ID_GAME_POINTS] ?: 0
        var pointsP1 = pointsMapShort[IdsInputFieldUtils.ID_PERSON_4_1] ?: 0
        var pointsP2 = pointsMapShort[IdsInputFieldUtils.ID_PERSON_4_2] ?: 0
        var pointsP3 = pointsMapShort[IdsInputFieldUtils.ID_PERSON_4_3] ?: 0
        var pointsP4 = pointsMapShort[IdsInputFieldUtils.ID_PERSON_4_4] ?: 0
        var isMeno10 = false
        if (idPersonBolt != null) {
            val boltManagerEntity = db.boltManagerDao().getIsMeno10(GamePath.GAME_PATH_4_PLAYERS.description, idGame, idPersonBolt)
            if (boltManagerEntity != null) {
                isMeno10 = boltManagerEntity.minus10
            }
            if (boltManagerEntity == null || isMeno10) {
                if (boltManagerEntity == null) {
                    val boltManager2GroupsEntity1 = BoltManagerEntity(GamePath.GAME_PATH_4_PLAYERS.description)
                    boltManager2GroupsEntity1.idGame = idGame
                    boltManager2GroupsEntity1.idPerson = idPersonBolt
                    boltManager2GroupsEntity1.minus10 = false
                    db.boltManagerDao().insert(boltManager2GroupsEntity1)
                }
                if (isMeno10) {
                    boltManagerEntity!!.minus10 = false
                    db.boltManagerDao().update(boltManagerEntity)
                }
            }
            if (isMeno10) {
                if (idPersonBolt == IdsInputFieldUtils.ID_PERSON_4_1) {
                    pointsP1 = (-10).toShort()
                } else if (idPersonBolt == IdsInputFieldUtils.ID_PERSON_4_2) {
                    pointsP2 = (-10).toShort()
                } else if (idPersonBolt == IdsInputFieldUtils.ID_PERSON_4_3) {
                    pointsP3 = (-10).toShort()
                } else if (idPersonBolt == IdsInputFieldUtils.ID_PERSON_4_4) {
                    pointsP4 = (-10).toShort()
                }
            }
        }
        var lastPoints4PEntity = db.points4PDao().getLastPoints(idGame)
        if (lastPoints4PEntity == null) {
            lastPoints4PEntity = Points4PEntity()
            lastPoints4PEntity.idGame = idGame
        }
        pointsP1 = (pointsP1 + lastPoints4PEntity.pointsP1).toShort()
        pointsP2 = (pointsP2 + lastPoints4PEntity.pointsP2).toShort()
        pointsP3 = (pointsP3 + lastPoints4PEntity.pointsP3).toShort()
        pointsP4 = (pointsP4 + lastPoints4PEntity.pointsP4).toShort()

        val points4PEntity = Points4PEntity()
        points4PEntity.idGame = idGame
        points4PEntity.pointsGame = pointsGame
        points4PEntity.pointsP1 = pointsP1
        points4PEntity.pointsP2 = pointsP2
        points4PEntity.pointsP3 = pointsP3
        points4PEntity.pointsP4 = pointsP4

        val idRowInserted = db.points4PDao()
                .insert(points4PEntity).toInt()
        points4PEntity.id = idRowInserted
        var boltEntity = db.boltDao().getBolt(GamePath.GAME_PATH_4_PLAYERS.description, idGame, idPersonBolt)
        var nrBolt: Byte? = null
        if (idPersonBolt != null && !isMeno10) {
            if (boltEntity != null) {
                nrBolt = boltEntity.nrBolt
            }
            boltEntity = BoltEntity(GamePath.GAME_PATH_4_PLAYERS.description)
            boltEntity.idRow = idRowInserted
            boltEntity.idGame = idGame
            boltEntity.idPersonBolt = idPersonBolt
            boltEntity.nrBolt = nrBolt
            insertBolt(boltEntity)
        }
        pointsMapShort[IdsInputFieldUtils.ID_PERSON_4_1] = points4PEntity.pointsP1
        pointsMapShort[IdsInputFieldUtils.ID_PERSON_4_2] = points4PEntity.pointsP2
        pointsMapShort[IdsInputFieldUtils.ID_PERSON_4_3] = points4PEntity.pointsP3
        pointsMapShort[IdsInputFieldUtils.ID_PERSON_4_4] = points4PEntity.pointsP4
        val infoWinner = getWinner(pointsMapShort, winningPoints)
        val statusGame = infoWinner.statusGame
        if (!StatusGameUtils.isContinue(statusGame)) {
            if (StatusGameUtils.isFinished(statusGame)) {
                updateScor(infoWinner.winners[0])
            } else if (StatusGameUtils.isExtended(statusGame) || StatusGameUtils.isExtendedMandatory(statusGame)) {
                val extendedGameEntity = ExtendedGameEntity(GamePath.GAME_PATH_4_PLAYERS.description)
                extendedGameEntity.idGame = idGame
                extendedGameEntity.idWinner = infoWinner.maxPersona
                extendedGameEntity.maxPoints = infoWinner.maxPoints
                db.extendedGameDao().merge(extendedGameEntity)
            }
            updateStatus(statusGame)
        }
        return convertPointsMapString(pointsMapShort, boltEntity, isMeno10, idPersonBolt)
    }

    private fun insertPoints2Groups(pointsMap: Map<Int, EditText?>, idPersonBolt: Int?, winnerPoints: Short): HashMap<Int, String> {
        val pointsMapShort = convertPointsMapShort(pointsMap)
        var pointsWe = pointsMapShort[IdsInputFieldUtils.ID_TEAM_1] ?: 0
        var pointsYouP = pointsMapShort[IdsInputFieldUtils.ID_TEAM_2] ?: 0
        val pointsGame = pointsMapShort[IdsInputFieldUtils.ID_GAME_POINTS] ?: 0
        var nrBolt: Byte? = null
        var isMeno10 = false
        if (idPersonBolt != null) {
            val boltManager2GroupsEntity = db.boltManagerDao().getIsMeno10(gamePath.description, idGame, idPersonBolt)
            if (boltManager2GroupsEntity != null) {
                isMeno10 = boltManager2GroupsEntity.minus10
            }
            if (boltManager2GroupsEntity == null || isMeno10) {
                if (boltManager2GroupsEntity == null) {
                    val boltManager2GroupsEntity1 = BoltManagerEntity(gamePath.description)
                    boltManager2GroupsEntity1.idGame = idGame
                    boltManager2GroupsEntity1.idPerson = idPersonBolt
                    boltManager2GroupsEntity1.minus10 = false
                    db.boltManagerDao().insert(boltManager2GroupsEntity1)
                }
                if (isMeno10) {
                    boltManager2GroupsEntity!!.minus10 = false
                    db.boltManagerDao().update(boltManager2GroupsEntity)
                }
            }
            if (isMeno10) {
                if (idPersonBolt == IdsInputFieldUtils.ID_TEAM_1) {
                    pointsWe = (-10).toShort()
                } else if (idPersonBolt == IdsInputFieldUtils.ID_TEAM_2) {
                    pointsYouP = (-10).toShort()
                }
            }
        }
        val lastRecord2Gruppi = db.points2GroupsDao()
                .getLastPoints(idGame)
        pointsWe = lastRecord2Gruppi?.pointsWe?.plus(pointsWe)?.toShort() ?: pointsWe
        pointsYouP = lastRecord2Gruppi?.pointsYouP?.plus(pointsYouP)?.toShort() ?: pointsYouP
        val entityToInsert = Points2GroupsEntity()
        entityToInsert.idGame = idGame
        entityToInsert.pointsWe = pointsWe
        entityToInsert.pointsYouP = pointsYouP
        entityToInsert.pointsGame = pointsGame
        val idRowInserted = db.points2GroupsDao()
                .insert(entityToInsert).toInt()
        var boltEntity = db.boltDao().getBolt(gamePath.description, idGame, idPersonBolt)
        if (idPersonBolt != null && !isMeno10) {
            if (boltEntity != null) {
                nrBolt = boltEntity.nrBolt
            }
            boltEntity = BoltEntity(gamePath.description)
            boltEntity.idRow = idRowInserted
            boltEntity.idGame = idGame
            boltEntity.idPersonBolt = idPersonBolt
            boltEntity.nrBolt = nrBolt
            insertBolt(boltEntity)
        }
        pointsMapShort[IdsInputFieldUtils.ID_TEAM_1] = pointsWe
        pointsMapShort[IdsInputFieldUtils.ID_TEAM_2] = pointsYouP
        val infoWinner = getWinner(pointsMapShort, winnerPoints)
        val statusGame = infoWinner.statusGame
        if (!StatusGameUtils.isContinue(statusGame)) {
            if (StatusGameUtils.isFinished(statusGame)) {
                val idWinner = infoWinner.winners[0]
                updateScor(idWinner)
            } else if (StatusGameUtils.isExtended(statusGame) || StatusGameUtils.isExtendedMandatory(statusGame)) {
                val extendedGameEntity = ExtendedGameEntity(gamePath.description)
                extendedGameEntity.idGame = idGame
                extendedGameEntity.idWinner = infoWinner.maxPersona
                extendedGameEntity.maxPoints = infoWinner.maxPoints
                db.extendedGameDao().merge(extendedGameEntity)
            }
            updateStatus(statusGame)
        }
        return convertPointsMapString(pointsMapShort, boltEntity, isMeno10, idPersonBolt)
    }

    private fun updateScor(idWinner: Int) {
        val isUpdated = when (gamePath) {
            GamePath.GAME_PATH_2_PLAYERS -> {
                updateScor2P(idGame, idWinner)
            }
            GamePath.GAME_PATH_3_PLAYERS -> {
                updateScor3P(idGame, idWinner)
            }
            GamePath.GAME_PATH_4_PLAYERS -> {
                updateScor4P(idGame, idWinner)

            }
            GamePath.GAME_PATH_2_GROUPS -> {
                updateScor2Groups(idGame, idWinner)
            }
        }
        if (isUpdated) {
            _observableUpdateScor.postValue(idWinner)
        }
    }

    private fun updateStatus(statusGame: Byte) {
        val isUpdated = when (gamePath) {
            GamePath.GAME_PATH_2_PLAYERS -> {
                updateStatus2P(idGame, statusGame)
            }
            GamePath.GAME_PATH_3_PLAYERS -> {
                updateStatus3P(idGame, statusGame)
            }
            GamePath.GAME_PATH_4_PLAYERS -> {
                updateStatus4P(idGame, statusGame)
            }
            GamePath.GAME_PATH_2_GROUPS -> {
                updateStatus2Groups(idGame, statusGame)
            }
        }
        if (isUpdated) {
            _observableUpdateStatus.postValue(statusGame)
        }
    }

    fun stopGame() {
        val isUpdated = when (gamePath) {
            GamePath.GAME_PATH_2_PLAYERS -> {
                db.game2PDao().updateStatus(idGame, GameStatus.FINISHED, Date().time) == 1
            }
            GamePath.GAME_PATH_3_PLAYERS -> {
                db.game3PDao().updateStatus(idGame, GameStatus.FINISHED, Date().time) == 1

            }
            GamePath.GAME_PATH_4_PLAYERS -> {
                db.game4PDao().updateStatus(idGame, GameStatus.FINISHED, Date().time) == 1

            }
            GamePath.GAME_PATH_2_GROUPS -> {
                db.game2GroupsDao().updateStatus(idGame, GameStatus.FINISHED, Date().time) == 1
            }
        }
        if (isUpdated) {
            _observableUpdateStatus.postValue(GameStatus.FINISHED)
        }
    }

    private fun getShortInputForDB(editText: EditText?): Short {
        val points = if (editText?.text.toString().isEmpty()) editText?.hint.toString() else editText?.text.toString()
        return try {
            points.toShort()
        } catch (e: Exception) {
            0
        }
    }
}