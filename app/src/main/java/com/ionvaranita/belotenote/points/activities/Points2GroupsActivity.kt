package com.ionvaranita.belotenote.points.activities

import android.os.Bundle
import com.ionvaranita.belotenote.points.PointsBaseActivity
import com.ionvaranita.belotenote.shared.constants.GamePath

class Points2GroupsActivity : PointsBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.gamePath = GamePath.GAME_PATH_2_GROUPS
        super.onCreate(savedInstanceState)
    }

}