package com.ionvaranita.belotenote.main


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.ionvaranita.belotenote.R
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class Players2InsertTableTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun players2Test() {
        val appCompatImageButton = onView(
                allOf(withId(R.id.ib_2_players),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.wrap_content),
                                        0),
                                0),
                        isDisplayed()))
        appCompatImageButton.perform(click())

        val floatingActionButton = onView(
                allOf(withId(R.id.fb_insertTable),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.frame_layout_fragment_main),
                                        0),
                                3),
                        isDisplayed()))
        floatingActionButton.perform(click())

        val appCompatButton = onView(
                allOf(withId(R.id.bt_ok), withText("Insert new table"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.LinearLayout")),
                                        4),
                                0),
                        isDisplayed()))
        appCompatButton.perform(click())

        val appCompatButton2 = onView(
                allOf(withId(R.id.button_3), withText("3"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.TableLayout")),
                                        0),
                                2),
                        isDisplayed()))
        appCompatButton2.perform(click())

        val appCompatButton3 = onView(
                allOf(withId(R.id.button_3), withText("3"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.TableLayout")),
                                        0),
                                2),
                        isDisplayed()))
        appCompatButton3.perform(click())

        val appCompatButton4 = onView(
                allOf(withId(R.id.button_2), withText("2"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.TableLayout")),
                                        0),
                                1),
                        isDisplayed()))
        appCompatButton4.perform(click())

        val appCompatButton5 = onView(
                allOf(withId(R.id.button_2), withText("2"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.TableLayout")),
                                        0),
                                1),
                        isDisplayed()))
        appCompatButton5.perform(click())

        val appCompatImageButton2 = onView(
                allOf(withId(R.id.button_enter),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.TableLayout")),
                                        2),
                                2),
                        isDisplayed()))
        appCompatImageButton2.perform(click())
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
