package com.ionvaranita.belotenote.main

import androidx.test.InstrumentationRegistry
import com.ionvaranita.belotenote.database.entity.BoltEntity
import com.ionvaranita.belotenote.database.entity.players2.Points2PEntity
import com.ionvaranita.belotenote.main.datamodel.PointsModelConverter
import com.ionvaranita.belotenote.shared.constants.GamePath
import com.ionvaranita.belotenote.shared.constants.IdsInputFieldUtils
import junit.framework.Assert.assertTrue
import org.junit.Test

class PointsModelConverterTest {
    private val PATH = GamePath.GAME_PATH_2_PLAYERS.description

    @Test
    fun testConvertPoints2P() {
        val context = InstrumentationRegistry.getTargetContext()
        val pointsModelConverter = PointsModelConverter(context)
        val points2PEntity = Points2PEntity()
        points2PEntity.id = 1
        points2PEntity.idGame = 2
        points2PEntity.pointsGame = 66
        points2PEntity.pointsMe = 22
        points2PEntity.pointsYouS = 44

        val listPoints = ArrayList<Points2PEntity>()
        listPoints.add(points2PEntity)

        val boltEntity = BoltEntity(PATH)
        boltEntity.idRow = 1
        boltEntity.idGame = 2
        boltEntity.idPersonBolt = IdsInputFieldUtils.ID_PERSON_2_1
        boltEntity.nrBolt = 1

        val listBolt = ArrayList<BoltEntity>()
        listBolt.add(boltEntity)

        val resultConverted =  pointsModelConverter.convertPoints2P(listPoints, listBolt)

        assertTrue(resultConverted[0].next)

    }

    @Test
    fun testConvertPoints3P() {
    }

    @Test
    fun testConvertPoints4P() {
    }

    @Test
    fun testConvertPoints2Groups() {
    }

    @Test
    fun testConvertScor2P() {
    }

    @Test
    fun testConvertScor3P() {
    }

    @Test
    fun testConvertScor4P() {
    }

    @Test
    fun testConvertScor2Groups() {
    }
}